const { createNativewindDefaultConfig } = require('@q/ui/tw');

// const twConfig = createNativewindConfig({
//   mode: 'web',
//   relContentDirs: [
//     './storybook',
//     './src',
//     './pages',
//     '../../packages/app/src',
//     '../../packages/ui/src',
//     '../../packages/modules/src',
//     '../../packages/stories/src',
//   ],
//   fixRNWOrder: true,
//   darkMode: 'class',
// });

module.exports = createNativewindDefaultConfig({
  mode: 'web',
  baseDir: __dirname,
  type: 'storybook',
});
