process.env.Q_PLATFORM_TARGET = 'web';

// babel.config.js
module.exports = function (api) {
  api.cache.forever();
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      '@babel/plugin-proposal-export-namespace-from',
      'tsconfig-paths-module-resolver',
      ['transform-inline-environment-variables', { include: 'Q_PLATFORM_TARGET' }],
    ],
  };
};
