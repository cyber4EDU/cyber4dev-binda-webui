import * as React from 'react';
import { Preview, StoryContext, StoryFn } from '@storybook/react';
import { Provider } from '@q/app/provider';

import { NativeWindStyleSheet } from 'nativewind';

import { RouterContext } from 'next/dist/shared/lib/router-context';

import '../global.css';
import '../.gen/global.css';
import './fixsb.css';

import { View } from 'react-native';
import { qtwMerge } from '@q/ui';

export const parameters = {
  options: {
    storySort: {
      order: [
        'Design System',
        ['Tokens', 'Text Components'],
        'Elements',
        'Components',
        'Modules',
        'Screens',
        '*',
        'Test',
      ],
    },
  },
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  backgrounds: {
    grid: {
      cellSize: 4,
      opacity: 0.3,
      cellAmount: 5,
      offsetX: 16, // default is 0 if story has 'fullscreen' layout, 16 if layout is 'padded'
      offsetY: 16, // default is 0 if story has 'fullscreen' layout, 16 if layout is 'padded'
    },
    default: '☀️ Light Mode',
    values: [
      {
        name: '☀️ Light Mode',
        value: '#ffffff',
        theme: 'light',
      },
      {
        name: '🌙 Dark Mode',
        value: '#000000',
        theme: 'dark',
      },
    ],
  },
  // backgrounds: {
  //   values: [],
  // },
  nextRouter: {
    Provider: RouterContext.Provider,
  },
};

export const globalTypes = {
  theme: {
    name: 'Theme',
    title: 'Theme',
    description: 'Theme for your components',
    defaultValue: 'bg',
    toolbar: {
      icon: 'paintbrush',
      dynamicTitle: true,
      items: [
        { value: 'auto', left: '🌈', title: 'Auto' },
        { value: 'bg', left: '🌈', title: 'Background' },
        { value: 'light', left: '☀️', title: 'Light Mode' },
        { value: 'dark', left: '🌙', title: 'Dark Mode' },
      ],
    },
  },
};

function getThemeFromBackground(theme?: string, background?: string) {
  if (theme === 'auto') {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      // dark mode
      return 'dark';
    }
    return 'light';
  }
  if (theme === 'bg') {
    return parameters.backgrounds.values.find((v) => v.value === background)?.theme || 'light';
  }
  return theme;
}

const FlexDecorator = (Story: StoryFn, context: StoryContext) => {
  if (context?.parameters?.layout !== 'flex') return <Story />;

  const flexDirection = context?.parameters.flexDirection || 'column';

  return (
    <div
      style={{
        display: 'flex',
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        flexDirection,
      }}
    >
      <Story />
    </div>
  );
};

const ProviderDecorator = (Story: StoryFn, context: StoryContext) => {
  NativeWindStyleSheet.setWebClassNameMergeStrategy(qtwMerge);
  // The theme global we just declared
  const theme = getThemeFromBackground(context.globals.theme, context.globals.backgrounds?.value);
  // const { theme: themeKey } = args.globals;
  // const name = useThemeState((state) => state.name);
  return (
    <>
      <Provider defaultTheme={theme === 'dark' ? 'dark' : 'light'} session={null}>
        <Story />
      </Provider>
    </>
  );
};

export const decorators = [ProviderDecorator, FlexDecorator];

const preview: Preview = {
  globalTypes,
  decorators,
  parameters,
};

export default preview;
