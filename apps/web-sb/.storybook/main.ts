import type { StorybookConfig } from '@storybook/nextjs';
import { cfg } from '@q/stories';

const config: StorybookConfig = {
  stories: cfg.stories('web'),
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
    // 'storybook-react-i18next',
    // 'storybook-addon-next-router',
    {
      name: '@storybook/addon-styling',
      options: {
        postCss: {
          implementation: require('postcss'),
        },
      },
    },
    // 'storybook-addon-next',
    { name: '@storybook/addon-react-native-web', options: {} },
    // {
    //   name: '@storybook/addon-react-native-web',
    //   options: {
    //     modulesToTranspile: [
    //       'solito',
    //       'twrnc',
    //       'nativewind',
    //       'react-native',
    //       // 'react-native-web',
    //       'expo-linking',
    //       'expo-constants',
    //       'expo-modules-core',
    //       // '@my/config',
    //     ],
    //     babelPlugins: [
    //       // "react-native-reanimated/plugin", // this breaks...
    //     ],
    //   },
    // },
    // {
    //   name: '@storybook/addon-styling',
    //   options: {
    //     // Check out https://github.com/storybookjs/addon-styling/blob/main/docs/api.md
    //     // For more details on this addon's options.
    //     postCss: true,
    //   },
    //    },
  ],
  typescript: {
    check: false,
    checkOptions: {},
    reactDocgen: process.env.NODE_ENV === 'development' ? false : 'react-docgen-typescript',
    reactDocgenTypescriptOptions: {
      shouldExtractLiteralValuesFromEnum: true,
      // makes string and boolean types that can be undefined appear as inputs and switches
      shouldRemoveUndefinedFromOptional: true,
      // Filter out third-party props from node_modules except SOME packages
      propFilter: (prop: any) =>
        prop.parent ? !/node_modules\/(?!SOME)/.test(prop.parent.fileName) : true,
    },
  },
  framework: {
    name: '@storybook/nextjs',
    options: {
      fastRefresh: true,
      // nextConfigPath: path.resolve(__dirname, '../next.config.js'),
    },
  },
  docs: {
    autodocs: 'tag',
    defaultName: 'Overview',
  },
  core: {
    disableTelemetry: true,
    enableCrashReports: false,
  },
  env: (config: any) => ({
    ...config,
    Q_PLATFORM_TARGET: 'web',
  }),
  // webpackFinal: async (config: any, { configType }: { configType?: any }) => {
  //   config.resolve = {
  //     ...config.resolve,
  //     fallback: {
  //       ...(config.resolve || {}).fallback,
  //       fs: false,
  //       stream: false,
  //       os: false,
  //       zlib: false,
  //     },
  //   };
  //   // config.cache = {type: 'memory'}
  //   config.module.rules.push({
  //     test: /\.(js|mjs|jsx)$/,
  //     resolve: {
  //       fullySpecified: false,
  //     },
  //   });

  //   // config.resolve.plugins = config.resolve.plugins || []
  //   // config.resolve.plugins.push(
  //   //   new TsconfigPathsPlugin({
  //   //     configFile: path.resolve(__dirname, '../tsconfig.json'),
  //   //   })
  //   // )
  //   return config;
  // },
};
export default config;
