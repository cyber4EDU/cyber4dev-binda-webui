Error.stackTraceLimit = Infinity;

const { withExpo } = require('@expo/next-adapter');

/** @type {import('next').NextConfig} */
const nextConfig = {
  // reanimated (and thus, Moti) doesn't work with strict mode currently...
  // https://github.com/nandorojo/moti/issues/224
  // https://github.com/necolas/react-native-web/pull/2330
  // https://github.com/nandorojo/moti/issues/224
  // once that gets fixed, set this back to true
  reactStrictMode: false,
  transpilePackages: [
    '@q/app',
    '@q/cfg',
    '@q/stories',
    '@q/ui',

    '@qoobz/quant',
    'react-native',
    'react-native-web',
    'solito',
    'moti',
    'react-native-reanimated',
    'nativewind',
    'react-native-gesture-handler',
    'twrnc',
    'expo-linking',
    'expo-constants',
    'expo-modules-core',
  ],
  eslint: {
    ignoreDuringBuilds: true,
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  env: {
    Q_PLATFORM_TARGET: 'web',
  },
};

module.exports = withExpo(nextConfig);
