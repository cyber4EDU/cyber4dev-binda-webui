import { QueryKey, useQuery, UseQueryOptions } from '@tanstack/react-query';
import { useRouter } from 'next/router';
import { Session } from 'next-auth';

export async function fetchSession() {
  const res = await fetch('/api/auth/session');
  const session = await res.json();
  if (Object.keys(session).length) {
    return session;
  }
  return null;
}

export function useSession({
  required,
  redirectTo = '/api/auth/signin?error=SessionExpired',
  queryConfig = {},
}: {
  required?: boolean;
  redirectTo?: string;
  queryConfig?: UseQueryOptions<Session | null>;
} = {}) {
  const router = useRouter();
  const query = useQuery({
    queryKey: ['session'] as QueryKey,
    queryFn: fetchSession,
    ...queryConfig,
    onSettled(data, error) {
      if (queryConfig.onSettled) queryConfig.onSettled(data, error);
      if (data || !required) return;
      router.push(redirectTo);
    },
  });
  return [query.data, query.status === 'loading'];
}
