import { authOptions } from '@q/app/auth/auth-config';
import NextAuth from 'next-auth';

const nextAuth = NextAuth(authOptions);

export { authOptions, nextAuth };
