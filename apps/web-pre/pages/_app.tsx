import 'raf/polyfill';
import getConfig from 'next/config';
import { Root } from '@q/app/features/root';
import { qtwMerge } from '@q/ui/tw';
import { NativeWindStyleSheet } from 'nativewind';
import Head from 'next/head';
import { Session } from 'next-auth';
import '../global.css';
import { setupRuntimeEnv } from '@q/app/env';
import App, { AppContext } from 'next/app';

const { publicRuntimeConfig } = getConfig();

function BindaApp({
  Component,
  pageProps: { session, ...pageProps },
}: any & { pageProps: { session: Session } }) {
  NativeWindStyleSheet.setWebClassNameMergeStrategy(qtwMerge);
  setupRuntimeEnv(publicRuntimeConfig);
  return (
    <>
      <Head>
        <title>QUA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="QUA" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Root session={session} defaultTheme="light">
        <Component {...pageProps} />
      </Root>
    </>
  );
}

export default BindaApp;

BindaApp.getInitialProps = async (appCtx: AppContext) => {
  const ctx = await App.getInitialProps(appCtx);
  return { ...ctx, pageProps: { session: ctx.pageProps.session } };
};
