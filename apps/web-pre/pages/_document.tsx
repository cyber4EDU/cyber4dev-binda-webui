import NextDocument, {
  DocumentContext,
  DocumentInitialProps,
  Head,
  Html,
  Main,
  NextScript,
} from 'next/document';
import React from 'react';
import { AppRegistry } from 'react-native';

export default class Document extends NextDocument {
  static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps> {
    AppRegistry.registerComponent('Main', () => Main);

    // @ts-ignore
    const { getStyleElement } = AppRegistry.getApplication('Main');
    const styles = [getStyleElement()];

    const initialProps = await NextDocument.getInitialProps(ctx);
    return { ...initialProps, styles: React.Children.toArray(styles) };
  }

  render() {
    return (
      <Html lang="de">
        <Head>
          <meta charSet="UTF-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link rel="apple-touch-icon" sizes="57x57" href="/appicon/apple-icon-57x57.png" />
          <link rel="apple-touch-icon" sizes="60x60" href="/appicon/apple-icon-60x60.png" />
          <link rel="apple-touch-icon" sizes="72x72" href="/appicon/apple-icon-72x72.png" />
          <link rel="apple-touch-icon" sizes="76x76" href="/appicon/apple-icon-76x76.png" />
          <link rel="apple-touch-icon" sizes="114x114" href="/appicon/apple-icon-114x114.png" />
          <link rel="apple-touch-icon" sizes="120x120" href="/appicon/apple-icon-120x120.png" />
          <link rel="apple-touch-icon" sizes="144x144" href="/appicon/apple-icon-144x144.png" />
          <link rel="apple-touch-icon" sizes="152x152" href="/appicon/apple-icon-152x152.png" />
          <link rel="apple-touch-icon" sizes="180x180" href="/appicon/apple-icon-180x180.png" />
          <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href="/appicon/android-icon-192x192.png"
          />
          <link rel="icon" type="image/png" sizes="32x32" href="/appicon/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="96x96" href="/appicon/favicon-96x96.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/appicon/favicon-16x16.png" />
          <link rel="manifest" href="/manifest.json" />
          <meta name="msapplication-TileColor" content="#FEF8F8" />
          <meta name="msapplication-TileImage" content="/appicon/ms-icon-144x144.png" />
          <meta name="theme-color" content="#FEF8F8" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
