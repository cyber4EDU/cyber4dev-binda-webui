import { DashboardScreen } from '@q/app/features/dashboard';

export default function Dashboard() {
  return <DashboardScreen />;
}
