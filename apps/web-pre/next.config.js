Error.stackTraceLimit = Infinity;

const { createPublicRuntimeConfig, validateEnvVars } = require('@q/app/src/env/env');
const { withExpo } = require('@expo/next-adapter');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

/**
 * Check env-vars but not during build time
 * @param {import('next').NextConfig} nextConfig
 * @returns {import('next').NextConfig}
 */
const withEnvCheck = (nextConfig) => {
  return (phase /* {defaultConfig} */) => {
    if (phase !== 'phase-production-build') {
      if (typeof document === 'undefined') {
        validateEnvVars();
        console.info('>>> Validated ENV vars => OK.');
      }
    } else {
      console.info('>>> Skipping ENV vars check.');
    }
    return nextConfig;
  };
};

/** @type {import('next').NextConfig} */
const nextConfig = {
  // reanimated (and thus, Moti) doesn't work with strict mode currently...
  // https://github.com/nandorojo/moti/issues/224
  // https://github.com/necolas/react-native-web/pull/2330
  // https://github.com/nandorojo/moti/issues/224
  // once that gets fixed, set this back to true
  reactStrictMode: false,
  transpilePackages: [
    '@q/app',
    '@q/cfg',
    '@q/stories',
    '@q/ui',
    '@qoobz/quant',
    'react-native',
    'react-native-web',
    'solito',
    'moti',
    'react-native-reanimated',
    'nativewind',
    'react-native-svg',
    'react-native-gesture-handler',
    'twrnc',
    'expo-linking',
    'expo-constants',
    'expo-modules-core',
  ],
  eslint: {
    ignoreDuringBuilds: true,
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  publicRuntimeConfig: createPublicRuntimeConfig(),
};

module.exports = withEnvCheck(withBundleAnalyzer(withExpo(nextConfig)));
