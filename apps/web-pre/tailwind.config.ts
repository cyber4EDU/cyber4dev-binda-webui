const { createNativewindDefaultConfig } = require('@q/ui/tw');

module.exports = createNativewindDefaultConfig({
  mode: 'web',
  baseDir: __dirname,
  type: 'next',
});
