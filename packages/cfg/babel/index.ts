import type { ConfigAPI } from '@babel/core';

// const platform = process.env.EAS_BUILD_PLATFORM; // for eas android...

type CretateBabelConfigOptions = {
  useRouter?: boolean;
  mode: 'native' | 'web';
};

export function createBabelConfig({ useRouter = false, mode }: CretateBabelConfigOptions) {
  const web = mode === 'web';
  process.env['Q_PLATFORM_TARGET'] = web ? 'web' : 'native';

  const presets = ['nativewind/babel', ['babel-preset-expo', { jsxRuntime: 'automatic' }]];

  const plugins = [
    '@babel/plugin-proposal-export-namespace-from',
    'tsconfig-paths-module-resolver',
    // if you want reanimated support
    'react-native-reanimated/plugin', // not tested yet on app-router
    // ---
    ['transform-inline-environment-variables', { include: 'Q_PLATFORM_TARGET' }],
    useRouter && require.resolve('expo-router/babel'),
  ].filter(Boolean);
  console.info(
    `>>> Babel config for ${process.env['Q_PLATFORM_TARGET']} – cfg: ${
      useRouter ? 'with router' : ''
    }, nativewind/babel`,
  );

  return function babel(api: ConfigAPI) {
    api.cache.forever();
    return {
      presets,
      plugins,
    };
  };
}
