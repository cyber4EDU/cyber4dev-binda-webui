import { getDefaultConfig } from '@expo/metro-config';
import withNativewind from 'nativewind/dist/metro';
import path from 'path';

type CreateMetroConfigOptions = {
  useWatchman?: boolean;
};

export function createMetroConfig(
  projectRoot: string,
  { useWatchman = true }: CreateMetroConfigOptions = {},
) {
  const config = getDefaultConfig(projectRoot);
  const workspaceRoot = path.resolve(projectRoot, '../..');

  // 1. Watch all files within the monorepo
  config.watchFolders = [workspaceRoot];
  config.resolver.nodeModulesPaths = [
    path.resolve(projectRoot, 'node_modules'),
    path.resolve(workspaceRoot, 'node_modules'),
  ];
  // 3. Force Metro to resolve (sub)dependencies only from the `nodeModulesPaths`
  config.resolver.disableHierarchicalLookup = true;

  config.transformer = { ...config.transformer, unstable_allowRequireContext: true };
  // config.transformer.minifierPath = require.resolve('metro-minify-terser')

  // config.fileMapCacheDirectory = path.resolve(projectRoot, 'node_modules', '.cache', 'metro');
  config.resolver.useWatchman = useWatchman;

  console.info(`>>> Metro config for ${process.env['Q_PLATFORM_TARGET']} – cfg: withNativewind`);

  return withNativewind(config);
}
