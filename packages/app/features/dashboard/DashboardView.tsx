import {
  useMyCurrentDashbaordInfo,
  useMyCurrentDashboardNumbers,
  useMyCurrentHappening,
  useParticipantsListSelectionState,
  useSelectedPupilInfoState,
} from '@q/app/api/api-state';
import { ApiDashboard, ApiGroup, ApiHappeningDashboard, ApiMember } from '@q/app/api/api-types';
import { envFlags } from '@q/app/env';
import { useNow, useSelectedDate } from '@q/app/state/atoms/app-state';
import { useMe } from '@q/app/state/hooks';
import * as du from '@q/app/utils/date-utils';
import { C, CR, Q, T } from '@q/ui';
import { RasterBackgroundContainer } from 'packages/ui/src/ds/elements/RasterBackground';
import { Suspense, useEffect, useState } from 'react';

import { ParticipantsListView } from '../participants-list/ParticipantsList';
import { PupilInfoModal } from '../pupil-info';
import { QRModal } from '../qr-scanner/qr-modal';
import { MenuBar } from '../shared/comps/MenuBar';
import { PanelContainer, PanelPlaceholder, PanelTitle } from '../shared/comps/PanelContainer';

export function DashboardView() {
  const [showQrModal, setShowQrModal] = useState(false);
  const [selectedPupilInfo, setSelectedPupilId] = useSelectedPupilInfoState();
  return (
    <RasterBackgroundContainer>
      <MenuBar />
      <Q.Screen contentCn="min-h-full w-full max-w-bplg mx-auto px-4 py-8 pb-24 pt-24">
        <Suspense fallback={<DVNameHeaderFallback />}>
          <DVNameHeader />
        </Suspense>
        <Suspense fallback={<DashboardSelectionFallback />}>
          <DashboardSelection />
        </Suspense>
        <CR className="fx-1 jc-fs ai-fs flex-wrap gap-3">
          <Suspense
            fallback={<PanelPlaceholder loading message="Infos werden geladen..." />}
            unstable_expectedLoadTime={3000}
          >
            <DashboardPanel />
          </Suspense>
          <ParticipantsListView />
        </CR>
      </Q.Screen>
      <Suspense fallback={<></>}>
        <QRButton onPress={() => setShowQrModal(true)} />
      </Suspense>
      <QRModal visible={showQrModal} onRequestClose={() => setShowQrModal(false)} />
      <PupilInfoModal
        visible={!!selectedPupilInfo}
        onRequestClose={() => setSelectedPupilId(null)}
      />
    </RasterBackgroundContainer>
  );
}

function QRButton({ onPress }: { onPress: () => void }) {
  const myCurrentHappening = useMyCurrentHappening();
  const disabled = !myCurrentHappening?.id;
  return (
    <C className={`fxcc fixed bottom-4 w-full ${disabled ? 'opacity-30' : ''}`}>
      <Q.RoundButton disabled={disabled} iconName="QrCode" label="Check-In" onPress={onPress} />
    </C>
  );
}

function DashboardPanel() {
  const dashboardInfo: ApiDashboard | undefined = useMyCurrentDashbaordInfo()?.data;
  const dashboardNumbers = useMyCurrentDashboardNumbers();
  const [participantsListSelection, setParticipentsListSelection] =
    useParticipantsListSelectionState();

  // TODO ioss: this is not OK, this has to go to the place where dashbaordInfo is set.
  useEffect(() => {
    if (!dashboardInfo && participantsListSelection) {
      setParticipentsListSelection(null);
    }
  }, [dashboardInfo, participantsListSelection]);

  if (!dashboardInfo) {
    return <PanelPlaceholder message="Zur Zeit..., nichts" />;
  }

  const happenings = [dashboardInfo.my_happening, ...(dashboardInfo.related_happenings ?? [])];
  const hapGroups = getHappeningGroups(happenings);
  const nums = dashboardNumbers?.data || {
    absent: 0,
    expected: 0,
    missing: 0,
    happenings: {},
  };
  return (
    <PanelContainer intent={participantsListSelection ? 'dashboard-with-list' : 'dashboard'}>
      <C className="border-b-1 mb-2 gap-3 pb-2">
        <PanelTitle title={hapGroups.map((hg) => hg.name).join(', ')} count={nums.expected ?? 0} />
        <CR className="gap-2">
          <Q.AbsentCardButton
            intent="danger"
            count={nums.missing}
            label="Nicht DA"
            selected={participantsListSelection?.id === 'missing'}
            onPress={() =>
              setParticipentsListSelection({ type: 'group', id: 'missing', groups: hapGroups })
            }
          />
          <Q.AbsentCardButton
            intent="warn"
            count={nums.absent}
            label="Abgemeldet"
            selected={participantsListSelection?.id === 'absent'}
            onPress={() =>
              setParticipentsListSelection({ type: 'group', id: 'absent', groups: hapGroups })
            }
          />
        </CR>
      </C>
      <CourseList>
        {happenings.map((happening, idx) => (
          <DashboardCourseEntry
            key={happening.id}
            count={nums.happenings[happening.id] ?? 0}
            happening={happening}
            own={idx === 0}
          />
        ))}
      </CourseList>
    </PanelContainer>
  );
}

function DashboardCourseEntry({
  happening,
  count,
  own,
}: {
  happening: ApiDashboard['my_happening'];
  count: number;
  own?: boolean;
}) {
  const [selectedList, setSelectedList] = useParticipantsListSelectionState();
  const subLabelItems = envFlags('showResponsiblesOnDashbaord')
    ? [
        comSep(
          happening.groups?.map((g) => g.name),
          2,
        ),
        comSep(happening.responsibles?.map((r) => getMemberFullName(r))),
      ].filter(Boolean)
    : [];

  return (
    <Q.CourseCardButton
      key={happening.id}
      label={happening.name}
      subLabel={subLabelItems.join(' · ')}
      count={count}
      responsible={own}
      selected={happening.id === selectedList?.id}
      onPress={() => setSelectedList({ type: 'happening', name: happening.name, id: happening.id })}
    />
  );
}

const DVNameHeader = () => {
  const me = useMe();
  return <T.PageHeader>Hi {getMemberFullName(me?.data) ?? 'unknown'} </T.PageHeader>;
};

const DVNameHeaderFallback = () => <T.PageHeader className="opacity-50">Hi ...</T.PageHeader>;

export function CourseList({ children }: { children: React.ReactNode }) {
  return <C className="gap-2">{children}</C>;
}

export function DashboardSelection() {
  const selectedDate = useSelectedDate();
  const myCurrentHappening = useMyCurrentHappening();
  const now = useNow();
  const label = myCurrentHappening
    ? du.formatTimeRange(myCurrentHappening.props?.from, myCurrentHappening.props?.to)
    : du.formatDate(now, 'dateTimeShort');
  return (
    <CR className="jc-fs mt-4 gap-2">
      <Q.SelectButton iconName="Calendar" label={du.formatDate(selectedDate, 'dateLong')} />
      {envFlags('showTimeSlotSelection') && <Q.SelectButton iconName="Time" label={label} />}
    </CR>
  );
}

export function DashboardSelectionFallback() {
  return (
    <CR className="jc-fs mt-4 gap-2 opacity-50">
      <Q.SelectButton iconName="Calendar" label="---" />
      <Q.SelectButton iconName="Time" label="---" />
    </CR>
  );
}

function getHappeningGroups(happenings: ApiHappeningDashboard[]) {
  const groups: ApiGroup[] = [];
  happenings.forEach((happening) => {
    happening.groups?.forEach((hg) => {
      if (!groups.some((g) => g.id === hg.id)) {
        groups.push(hg);
      }
    });
  });
  return groups;
}

function comSep(arr: unknown[] | undefined, minLength: number = 0, defaultValue = undefined) {
  if (!arr) return undefined;
  if (arr.length < minLength) return defaultValue;
  return arr.join(', ');
}

function getMemberFullName(member: Partial<ApiMember> | undefined) {
  if (!member || (!member.first_name && !member.last_name)) return undefined;
  return [member.first_name, member.last_name].filter(Boolean).join(' ');
}
