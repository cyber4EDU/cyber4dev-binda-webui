import { MemberShortInfo } from '@q/app/api';
import {
  useMyCurrentHappening,
  useSelectedPupilId,
  useSelectedPupilInfoState,
} from '@q/app/api/api-state';
import { MemberAbsentInfo } from '@q/app/api/api-types';
import { apiClient, getQueryClient } from '@q/app/api/client';
import { xclient } from '@q/app/api/gen_axios/openapi-axios-client';
import { now4query } from '@q/app/state/atoms/app-state';
import { formatDate } from '@q/app/utils/date-utils';
import { C, CR, Q, T } from '@q/ui';
import { IconName } from '@q/ui/ds/elements/Icon';
import { useMemo, useState } from 'react';

interface PupilInfoViewProps {
  onRequestClose: () => void;
}

export function PupilInfoView({ onRequestClose }: PupilInfoViewProps) {
  const [error, setError] = useState<unknown | undefined>(undefined);
  const [lastQueryErrorCount, setLastQueryErrorCount] = useState(0);
  const [selectedPupilInfo, setSelectedPupilInfoId] = useSelectedPupilInfoState();
  const selectedPupilId = useSelectedPupilId();
  const myCurrentHappening = useMyCurrentHappening();
  const absence = selectedPupilInfo?.absent && getAbsenceInfo(selectedPupilInfo.absent);
  let status = undefined;
  if (selectedPupilInfo) {
    if (!selectedPupilInfo.checked_in && !selectedPupilInfo.absent) {
      status = 'missing';
    } else if (selectedPupilInfo.checked_in) {
      status = 'checkedin';
    } else if (selectedPupilInfo.absent) {
      status = 'excused';
    }
  }

  const checkout = () => {
    apiClient
      .post('/actions/happening/{happening_id}/check_out/{member_id}', {
        params: {
          path: {
            member_id: selectedPupilId?.id ?? '',
            happening_id: selectedPupilInfo?.checked_in?.id ?? myCurrentHappening?.id ?? '',
          },
          query: {
            now: now4query(),
          },
        },
      })
      .then(() => setSelectedPupilInfoId(null))
      .catch(() => {});
  };

  const [canCheckIn, handleCheckIn] = useMemo(() => {
    const canCheckIn = selectedPupilInfo?.id && myCurrentHappening?.id;
    const handleCheckIn = canCheckIn
      ? async () => {
          const client = await xclient;
          const result = await client.check_in({
            happening_id: myCurrentHappening.id,
            member_id: selectedPupilInfo.id,
            now: now4query(),
          });
          setSelectedPupilInfoId(null);
          getQueryClient().invalidateQueries(['me-sellist']);
          return result;
        }
      : undefined;
    return [canCheckIn, handleCheckIn];
  }, [selectedPupilInfo?.id, myCurrentHappening?.id]);

  return (
    <C className="aspect-square overflow-hidden rounded-lg">
      <Q.Container bg="white" size="md" className="gap-3 p-6">
        <CR className="">
          <C>
            <T.PupilTitle>{selectedPupilId?.first_name}</T.PupilTitle>
            <T.PupilSubtitle>{selectedPupilId?.last_name}</T.PupilSubtitle>
          </C>
          {status === 'missing' && <Q.PillSmall type="missing" checked label="Nicht DA" />}
          {status === 'checkedin' && (
            <Q.PillSmall
              type="checkedin"
              checked
              label={selectedPupilInfo?.checked_in?.name ?? ''}
            />
          )}
          {absence && (
            <Q.PillSmall label={absence.label} checked rightIcon={absence.icon} type="excused" />
          )}
        </CR>

        <CR className="border-t-1 border-b-1 py-4">
          <C>
            <T.PupilDetailInfo>Demnächst mehr</T.PupilDetailInfo>
            <T.PupilDetailInfo>Infos</T.PupilDetailInfo>
          </C>
          <Q.IconButton label="Kontakt" iconName="Call" intent="info2" />
        </CR>
        <C className="bg-grey rounded-md p-6  opacity-30">
          <T.AbsentCardTitle>Notizen hier</T.AbsentCardTitle>
        </C>
        <CR className="border-t-1 pt-6">
          {status === 'checkedin' && (
            <Q.SmallButton label="Ist nicht da" intent="danger" onPress={checkout} />
          )}
          {status === 'checkedin' && <Q.SmallButton disabled label="Abmelden" intent="warn" />}
          {status === 'missing' && <Q.SmallButton disabled label="Abmelden" intent="warn" />}
          {status === 'missing' && (
            <Q.SmallButton
              disabled={!canCheckIn}
              label="Ist DA"
              intent="safe"
              onPress={handleCheckIn}
            />
          )}
          {status === 'excused' && <Q.SmallButton disabled label="Bearbeiten" intent="warn" />}
          {status === 'excused' && (
            <Q.SmallButton
              disabled={!canCheckIn}
              label="Ist DA"
              intent="safe"
              onPress={handleCheckIn}
            />
          )}
        </CR>
      </Q.Container>
    </C>
  );
}

function getAbsenceInfo(participant: any) {
  if (isMemberAbsentInfo(participant)) {
    return {
      icon: mapReasonToIconName(participant.reason).icon,
      label: `bis ${formatDate(participant.until, 'time')}`,
    };
  }
}

function mapReasonToIconName(reason: MemberAbsentInfo['reason']): {
  icon: IconName;
  label: string;
} {
  switch (reason) {
    case 'Holidays':
      return { icon: 'Apple', label: 'Mittagspause' };
    case 'Ill':
      return { icon: 'Bandage', label: 'Krankmeldung' };
    case 'Late':
      return { icon: 'Time', label: 'Verspätung' };
    case 'Released':
      return { icon: 'School', label: 'Schulbedingt' };
    default:
      return { icon: 'Out', label: 'Entschuldigt' };
  }
}

function isMemberAbsentInfo(participant: MemberShortInfo): participant is MemberAbsentInfo {
  return (participant as MemberAbsentInfo).reason !== undefined;
}
