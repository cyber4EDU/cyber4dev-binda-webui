import { Modal, ModalProps } from '@q/ui/components/modal';

import { PupilInfoView } from './pupil-info-view';

export function PupilInfoModal(props: Omit<ModalProps, 'children'>) {
  return (
    <Modal title="Anwesenheit bearbeiten" closeOnBackgroundPointerUp {...props}>
      <PupilInfoView onRequestClose={props.onRequestClose} />
    </Modal>
  );
}
