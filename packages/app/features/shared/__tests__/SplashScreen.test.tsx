import { render, screen } from '@testing-library/react-native';

import { SplashScreen } from '../../home/SplashScreen';

describe('SplashScreen', () => {
  it('should render with react-native renderer', () => {
    render(<SplashScreen />);

    const logo = screen.getByLabelText('BINDA');
    expect(logo).toBeFound();
  });

  xit('should show the tenant', () => {
    render(<SplashScreen tenant="Testing" />);
    expect(screen.getByText('Testing')).toBeVisible();
  });

  xit('should display an error message', () => {
    render(<SplashScreen tenant="Tester" status="error" error={new Error('Testing Error')} />);
    expect(screen.getByText(`Error:`)).toBeVisible();
    expect(screen.getByText(`Error: Testing`, { exact: false })).toHaveTextContent(
      'Error: Testing Error',
    );
  });

  xit('should not display an error message, if the status is not error', () => {
    render(<SplashScreen status="loaded" error={new Error('X')} />);
    expect(screen.queryByText('Error:')).toBeNull();
  });
});
