import { useParticipantsListSelectionState } from '@q/app/api/api-state';
import { envFlags } from '@q/app/env';
import { WebNowPicker } from '@q/app/state/atoms/app-state';
import { useAppConfig } from '@q/app/state/hooks';
import { HeaderBar, T } from '@q/ui';
import { CH } from '@q/ui/ds';
import { signOut } from 'next-auth/react';
import { useCallback } from 'react';

function getMenuBarTitle(
  selection: ReturnType<typeof useParticipantsListSelectionState>[0] | null,
) {
  if (!selection) return null;
  if (selection.type === 'group') {
    return {
      title: selection.id === 'absent' ? 'Abgemeldet' : 'Nicht DA',
      subtitle: selection.groups?.[0]?.name ?? '',
    };
  }

  return {
    title: selection.name,
    subtitle: '',
  };
}

export function MenuBar() {
  const { tenant } = useAppConfig();
  const [selection, setSelection] = useParticipantsListSelectionState();
  const clearSelection = useCallback(() => {
    setSelection(null);
  }, [setSelection]);

  const { title, subtitle } = getMenuBarTitle(selection) || { title: '', subtitle: '' };
  return (
    <HeaderBar
      tenant={tenant}
      onBack={selection ? clearSelection : undefined}
      onProfilePress={() =>
        signOut({
          callbackUrl: '/',
          redirect: true,
        })
      }
    >
      {selection && (
        <CH hide=">md" className="fxcc gap-0.5">
          <T.HeaderTitle>{title}</T.HeaderTitle>
          {subtitle && <T.HeaderSubtitle>{subtitle}</T.HeaderSubtitle>}
        </CH>
      )}
      <WebNowPicker enabled={envFlags('showTimeSelection')} />
    </HeaderBar>
  );
}
