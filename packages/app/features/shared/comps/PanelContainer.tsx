import { C1, CR, Q, T, styled } from '@q/ui';

export const PanelContainer = styled(Q.Container, 'fx-1 mt-2', {
  variants: {
    loading: {
      true: 'opacity-50',
    },
    intent: {
      dashboard: 'max-w-mimob min-w-full md:min-w-mimobc',
      'dashboard-with-list': 'max-w-mimob min-w-full md:min-w-mimobc hidden md:flex',
      pupillist: '',
      default: 'min-h-60 min-w-80 lg:max-w-1/3 md:max-w-1/2  sm:max-w-full',
    },
  },
  defaultProps: {
    bg: 'rose',
    intent: 'default',
  },
});

export function PanelPlaceholder({ loading, message }: { loading?: boolean; message?: string }) {
  return (
    <PanelContainer loading={loading}>
      <C1 className="fxcc">
        <T.AbsentCardTitle>{message}</T.AbsentCardTitle>
      </C1>
    </PanelContainer>
  );
}

export function PanelTitle({ title, count }: { title?: string; count?: number | '-' }) {
  return (
    <CR>
      <T.ListTitle>{title}</T.ListTitle>
      {count != null ? <Q.Pill count={count} /> : null}
    </CR>
  );
}
