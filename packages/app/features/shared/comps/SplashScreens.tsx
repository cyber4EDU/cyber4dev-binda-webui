import { useAuthStatus } from '@q/app/auth';
import { AuthStatus } from '@q/app/auth/auth-state';
import { CC, Q, SplashView } from '@q/ui';
import { MotiView } from 'moti';
import { signIn, signOut } from 'next-auth/react';
import { ReactElement, ReactNode, Suspense, useEffect, useState } from 'react';

type SplashViewProps = {
  onAuthStatusChange?: (status: AuthStatus) => void;
  children?: ReactNode | ((props: { loading: boolean; authStatus: AuthStatus }) => ReactElement);
};

export function SplashScreenBase({
  loading,
  children,
}: {
  loading?: boolean;
  children?: ReactNode;
}) {
  return (
    <Q.Screen contentCn="min-h-full ai-c jc-c gap-8 p-5 bg-rose-300">
      <SplashView logo={loading ? 'animated' : 'normal'} tenant="ESBZ">
        {children}
      </SplashView>
    </Q.Screen>
  );
}

export function SuspenseSplashScreen({ children }: { children?: ReactNode }) {
  return <Suspense fallback={<SplashScreenBase loading />}>{children}</Suspense>;
}

export function HomeSplashScreen({ onAuthStatusChange, children }: SplashViewProps) {
  const authStatus = useAuthStatus();
  const [waitedABit, setWaitedABit] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setWaitedABit(true);
    }, 1600);
  }, []);
  const loading = !waitedABit || authStatus === 'loading';
  useEffect(() => {
    onAuthStatusChange?.(authStatus);
  }, [authStatus]);

  const Child = typeof children === 'function' ? children : () => <>children</>;
  return (
    <SplashScreenBase loading={loading}>
      <Child loading={loading} authStatus={authStatus} />
    </SplashScreenBase>
  );
}

export function AuthSplashScreen() {
  return (
    <HomeSplashScreen>
      {({ loading }) => <CC className="min-h-24">{!loading && <LoginButton />}</CC>}
    </HomeSplashScreen>
  );
}

function LoginButton() {
  const authStatus = useAuthStatus();
  if (authStatus === 'loading') {
    return null;
  }
  return (
    <MotiView
      from={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 700 }}
    >
      <Q.BigButton
        label={authStatus === 'authenticated' ? 'Abmelden' : 'Anmelden'}
        onPress={() => (authStatus === 'authenticated' ? signOut() : signIn('keycloak'))}
      />
    </MotiView>
  );
}
