import * as NativeRoot from './native';
import type { RootExport } from './root-types';
import * as WebRoot from './web';

const isAssignable = <T extends U, U>(draft: T, expected: U): T => {
  return draft;
};
isAssignable(WebRoot, {} as RootExport);
isAssignable(NativeRoot, {} as RootExport);
