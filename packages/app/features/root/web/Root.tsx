import { useQSession } from '@q/app/auth';
import { envFlags } from '@q/app/env';
import { Provider } from '@q/app/provider';
import { Q } from '@q/ui';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { signOut } from 'next-auth/react';
import {
  QErrorBoundaryDefaultFallback,
  QErrorBoundaryFallback,
} from 'packages/ui/src/ds/base/QErrorBoundary';
import { ReactNode, useEffect } from 'react';

import { AuthSplashScreen, SuspenseSplashScreen } from '../../shared/comps/SplashScreens';
import { RootProps } from '../root-types';

export function Root({ session, defaultTheme, ...props }: RootProps) {
  return (
    // <SessionProvider session={props.session}>
    <Q.ErrorBoundary fallback={RootErrorHandler}>
      <SuspenseSplashScreen>
        <Provider session={session} defaultTheme={defaultTheme}>
          <Q.ErrorBoundary fallback={RootErrorHandler}>
            <SuspenseSplashScreen>
              <RootView {...props} />
            </SuspenseSplashScreen>
          </Q.ErrorBoundary>
          {envFlags('showReactQueryDevTools') && (
            <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
          )}
        </Provider>
      </SuspenseSplashScreen>
    </Q.ErrorBoundary>
    // </SessionProvider>
  );
}

function RootView({ children }: { children?: ReactNode }) {
  const session = useQSession();

  if (session.status !== 'authenticated') {
    return <AuthSplashScreen />;
  }
  return <>{children}</>;
}

function getErrorStatus(err: unknown): number | undefined {
  if (!err || typeof err !== 'object') return undefined;
  if ('code' in err) {
    if (err.code === 'ERR_BAD_REQUEST') {
      return (err as { response?: { status?: number } }).response?.status;
    }
  }
  return undefined;
}

const RootErrorHandler: QErrorBoundaryFallback = (props) => {
  const status = getErrorStatus(props.error);
  const isUnAuthorizedError = status === 401;
  if (!isUnAuthorizedError) {
    console.warn('>>>>> RootErrorBoundary', props.error, JSON.stringify(props.error));
  }
  /* props.error instanceof ApiError && */
  useEffect(() => {
    if (isUnAuthorizedError) {
      signOut();
    }
  }, [isUnAuthorizedError]);
  if (isUnAuthorizedError) {
    return null;
  }
  return <QErrorBoundaryDefaultFallback {...props} />;
};

// function errorStatus(err: unknown) {
//   if (!err) return -1;

//   console.error('>>>>> errorStatus', err);
//   console.dir(err);
//   const statusError = err as { status: number };
//   if ('status' in statusError) {
//     return statusError.status;
//   }
//   return -1;
// }
