import { RootProps } from '../root-types';

export function Root({ children }: RootProps) {
  return <>{children}</>;
}
