import { ComponentProps, ReactNode } from 'react';

import { SessionProvider } from '../../provider/session';

export interface RootProps {
  children: ReactNode;
  defaultTheme: 'light' | 'dark' | (string & object);
  session: ComponentProps<typeof SessionProvider>['session'];
}

export type RootExport = {
  Root: (props: RootProps) => JSX.Element;
};
