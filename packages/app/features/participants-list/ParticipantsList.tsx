import { MemberShortInfo } from '@q/app/api';
import {
  useCurrentParticipantsList,
  useParticipantsListSelection,
  useSetSelectedPupilID,
} from '@q/app/api/api-state';
import { MemberAbsentInfo } from '@q/app/api/api-types';
import { formatDate, formatTimeRange } from '@q/app/utils/date-utils';
import { C, C1, Q, T } from '@q/ui';
import { IconName } from '@q/ui/ds/elements/Icon';
import { Suspense } from 'react';

import { PanelContainer, PanelPlaceholder, PanelTitle } from '../shared/comps/PanelContainer';

export function ParticipantsListView() {
  return (
    <Suspense fallback={<PanelPlaceholder loading message="Schüler*innen werden geladen..." />}>
      <ParticipantsList />
    </Suspense>
  );
}

function ParticipantsList() {
  const listSelection = useParticipantsListSelection();
  const participantsList = useCurrentParticipantsList();
  if (!listSelection) return null;
  const type = listSelection.type === 'happening' ? 'happening' : listSelection.id;

  const title =
    listSelection.id === 'absent'
      ? 'Abgemeldet'
      : listSelection.id === 'missing'
      ? 'Nicht DA'
      : listSelection.type === 'happening'
      ? listSelection.name
      : '???';

  if (!participantsList?.data) {
    return <PanelPlaceholder message="Daten nicht verfügbar!" />;
  }

  const participants = participantsList.data;
  const bg = type === 'absent' ? 'yellow' : type === 'missing' ? 'orange' : 'rose';
  return (
    <PanelContainer bg={bg} intent="pupillist">
      <PanelTitle title={title} count={participants.length} />
      {participants.length ? (
        <C className=" mt-3 gap-2 pb-2">
          {participants.map((participant) => (
            <ParticipantCardItem key={participant.id} participant={participant} />
          ))}
        </C>
      ) : (
        <C1 className="fxcc mt-3">
          <T.AbsentCardTitle>... niemand da.</T.AbsentCardTitle>
        </C1>
      )}
    </PanelContainer>
  );
}

function ParticipantCardItem({
  participant,
}: {
  participant: { id: string; first_name: string; last_name: string };
}) {
  const setSelectedPupilId = useSetSelectedPupilID();
  const absence = getAbsenceInfo(participant);
  return (
    <Q.PupilCardButton
      key={participant.id}
      label={participant.first_name}
      subLabel={participant.last_name}
      onPress={() => {
        setSelectedPupilId(participant);
      }}
    >
      {absence && (
        <Q.PillSmall label={absence.label} checked rightIcon={absence.icon} type="excused" />
      )}
    </Q.PupilCardButton>
  );
}

function getAbsenceInfo(participant: MemberShortInfo) {
  if (isMemberAbsentInfo(participant)) {
    return {
      icon: mapReasonToIconName(participant.reason).icon,
      label: `bis ${formatDate(participant.until, 'time')}`,
    };
  }
}

function mapReasonToIconName(reason: MemberAbsentInfo['reason']): {
  icon: IconName;
  label: string;
} {
  switch (reason) {
    case 'Holidays':
      return { icon: 'Apple', label: 'Mittagspause' };
    case 'Ill':
      return { icon: 'Bandage', label: 'Krankmeldung' };
    case 'Late':
      return { icon: 'Time', label: 'Verspätung' };
    case 'Released':
      return { icon: 'School', label: 'Schulbedingt' };
    default:
      return { icon: 'Out', label: 'Entschuldigt' };
  }
}

function isMemberAbsentInfo(participant: MemberShortInfo): participant is MemberAbsentInfo {
  return (participant as MemberAbsentInfo).reason !== undefined;
}
