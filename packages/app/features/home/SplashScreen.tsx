import { Q, T, SplashView } from '@q/ui';
import { ComponentProps, ReactNode } from 'react';

interface SplashScreenProps {
  status?: 'loading' | 'loaded' | 'error';
  tenant?: string;
  error?: unknown;
  children?: ReactNode;
}

export function SplashScreen({ status, tenant, error, children }: SplashScreenProps) {
  const contentStyle = status === 'error' ? 'jc-fs bg-orng-800' : 'jc-c bg-rose-300';
  const errorMsg = !!error && (error instanceof Error ? error.message : String(error));
  return (
    <Q.Screen contentCn={`min-h-full ai-c  gap-8 p-5 ${contentStyle}`}>
      <SplashView logo={statusToLogoVariant[status ?? 'loaded']} tenant={tenant}>
        {status === 'error' && (
          <>
            <T.PageHeader className="text-white">Error{errorMsg && ':'}</T.PageHeader>
            {errorMsg && <T.HeaderSubtitle className="text-white">{errorMsg}</T.HeaderSubtitle>}
          </>
        )}
        {status !== 'error' && children}
      </SplashView>
    </Q.Screen>
  );
}

const statusToLogoVariant: Record<
  NonNullable<SplashScreenProps['status']>,
  ComponentProps<typeof SplashView>['logo']
> = {
  error: 'rotated',
  loaded: 'normal',
  loading: 'animated',
};
