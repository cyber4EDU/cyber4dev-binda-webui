import { useQSession } from '@q/app/auth';
import { CC, Q, SplashView } from '@q/ui';
import { MotiView } from 'moti';
import { useRouter } from 'next/router';
import { signIn, signOut } from 'next-auth/react';
import { useEffect, useState } from 'react';
export function HomeScreen() {
  const session = useQSession();
  const [waitedABit, setWaitedABit] = useState(false);
  const router = useRouter();

  useEffect(() => {
    setTimeout(() => {
      setWaitedABit(true);
    }, 1000);
  }, []);
  const loading = !waitedABit || session.status === 'loading';
  useEffect(() => {
    if (session.status === 'authenticated') {
      router.push('/dashboard');
    }
  }, [session.status]);
  return (
    <Q.Screen contentCn="min-h-full ai-c jc-c gap-8 p-5">
      <SplashView logo={loading ? 'animated' : 'normal'} tenant="ESBZ">
        <CC className="min-h-24">{!loading && <LoginButton />}</CC>
      </SplashView>
    </Q.Screen>
  );
}

function LoginButton() {
  const session = useQSession();
  if (session.status === 'loading') {
    return null;
  }
  const authed = session.status === 'authenticated';
  return (
    <MotiView
      from={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 700 }}
    >
      <Q.BigButton
        label={authed ? 'Abmelden' : 'Anmelden'}
        onPress={() => (authed ? signOut() : signIn('keycloak'))}
      />
    </MotiView>
  );
}
