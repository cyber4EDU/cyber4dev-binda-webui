import { Modal, ModalProps } from '@q/ui/components/modal';

import { QRCodeView } from './qr-view';

export function QRModal(props: Omit<ModalProps, 'children'>) {
  return (
    <Modal title="QR Code Scannen" closeOnBackgroundPointerUp {...props}>
      <QRCodeView onRequestClose={props.onRequestClose} />
    </Modal>
  );
}
