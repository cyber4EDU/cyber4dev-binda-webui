import { C1, styled, Q } from '@q/ui';
import QRS from 'qr-scanner';

import { useQRScanner } from './qr-scanner-web-hook';
import type { QRScannerProps, QRScannerType } from './types';

function QRScannerComp({ ViewProps, scan, onError, onDetect }: QRScannerProps) {
  const videoRef = useQRScanner({
    scan,
    onError,
    onDetect,
  });

  return (
    <C1 className="bg-grey max-w-mimobc max-h-mimobc overflow-hidden" {...ViewProps}>
      <video className="h-full w-full object-cover" ref={videoRef} />
    </C1>
  );
}

function SafeQRScannerComp(props: QRScannerProps) {
  return (
    <Q.ErrorBoundary>
      <QRScannerComp {...props} />
    </Q.ErrorBoundary>
  );
}

export const qrScannerAvailable = QRS.hasCamera;

export const QRScanner: QRScannerType = styled(SafeQRScannerComp, 'border-mint-300  border-2');
