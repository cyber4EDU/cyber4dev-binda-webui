import { C, styled } from '@q/ui';
import { Camera } from 'expo-camera';

import { QRScannerProps, QRScannerType } from './types';

function QRScannerComp({ ViewProps, ...props }: QRScannerProps) {
  return (
    <C {...props} {...ViewProps}>
      <Camera
        style={{ flex: 1, width: 200, height: 200, borderWidth: 2, borderColor: 'red' }}
        onBarCodeScanned={(e) => {
          alert(JSON.stringify(e));
        }}
      />
    </C>
  );
}

export const QRScanner: QRScannerType = styled(QRScannerComp, 'border-mint-300  border-2');
