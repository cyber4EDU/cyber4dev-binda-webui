import QRS from 'qr-scanner';
import { useCallback, useEffect, useRef } from 'react';

import { QRCodeError } from './QRCodeError';
import { QRScannerProps } from './types';

export function useQRScanner({
  scan,
  onError,
  onDetect,
}: { scan?: boolean } & Pick<QRScannerProps, 'onError' | 'onDetect'>) {
  const scanner = useRef<QRS | null>();
  const videoRef = useRef<HTMLVideoElement>(null);
  const isScanning = useRef(false);
  const callbacks = useRef<Pick<QRScannerProps, 'onError' | 'onDetect'>>({ onError, onDetect });

  useEffect(() => {
    callbacks.current = { onError, onDetect };
  }, [onError, onDetect]);

  const onScanSuccess = useCallback((result: { data?: string }) => {
    if (result?.data) {
      callbacks.current.onDetect?.(result.data);
    } else {
      callbacks.current.onError?.(new QRCodeError('No QR code found', 'NoQRData'));
    }
  }, []);

  const onDecodeError = useCallback((error: Error | string) => {
    const errMsg = error instanceof Error ? error.message : String(error);
    if (errMsg.includes('No QR code found')) {
      return;
    }
    console.log('>>>>>>>>>>>>> Decode Error', errMsg, typeof error, error, String(error));
    if (typeof error === 'string') {
      if (error === 'No QR code found' || error === 'Scanner error: timeout') {
        return;
      }
      callbacks.current.onError?.(new QRCodeError(error, 'Unknown'));
    } else if (error instanceof Error) {
      callbacks.current.onError?.(new QRCodeError(error.message, 'Unknown', error));
    } else {
      callbacks.current.onError?.(new QRCodeError(String(error), 'Unknown', error));
    }
  }, []);

  useEffect(() => {
    try {
      if (videoRef.current) {
        scanner.current = new QRS(videoRef.current, onScanSuccess, {
          highlightCodeOutline: true,
          highlightScanRegion: true,
          onDecodeError,
          calculateScanRegion: (vidElem) => {
            const [cw, ch] = [vidElem.videoWidth, vidElem.videoHeight];
            const ms = Math.max(32, Math.min(cw, ch) - 32);
            const x = (cw - ms) / 2;
            const y = (ch - ms) / 2;

            return {
              x,
              y,
              width: ms,
              height: ms,
            };
          },
          // returnDetailedScanResult: false,
        });

        return () => {
          scanner.current?.destroy();
          scanner.current = null;
        };
      }
    } catch (error) {
      if (error instanceof Error) {
        onDecodeError(error);
      } else {
        onDecodeError(String(error));
      }
    }
  }, [videoRef.current]);

  useEffect(() => {
    try {
      if (scanner.current) {
        if (scan && !isScanning.current) {
          scanner.current.start();
          isScanning.current = true;
        } else if (isScanning.current) {
          scanner.current.stop();
          isScanning.current = false;
        }
      }
    } catch (error) {
      if (error instanceof Error) {
        onDecodeError(error);
      } else {
        onDecodeError(String(error));
      }
    }
  }, [scan, scanner.current]);

  return videoRef;
}
