export type QRCodeErrorType =
  | 'NotFound'
  | 'NoQRData'
  | 'NotSupported'
  | 'Unknown'
  | 'InvalidQRData';

export class QRCodeError extends Error {
  public type: QRCodeErrorType;

  constructor(message: string, type: QRCodeErrorType, cause?: unknown) {
    super(message, { cause });
    this.type = type;
  }
}
