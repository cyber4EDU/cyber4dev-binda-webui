import { ComponentType } from 'react';
import { ViewProps, ViewStyle } from 'react-native';

import { QRCodeError } from './QRCodeError';

export interface QRScannerProps {
  ViewProps?: ViewProps;
  scan?: boolean;
  styles?: ViewStyle;
  onError?: (error: QRCodeError) => void;
  onDetect?: (data: string) => void;
}

export type QRScannerType = ComponentType<QRScannerProps>;
