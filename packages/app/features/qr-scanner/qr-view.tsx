import {
  useMyCurrentDashbaordInfo,
  useSetParticipantsListSelection,
  useSetSelectedPupilID,
} from '@q/app/api/api-state';
import { ApiDashboard, MemberCheckedInInfo } from '@q/app/api/api-types';
import { apiClient } from '@q/app/api/client';
import { xclient } from '@q/app/api/gen_axios/openapi-axios-client';
import { now4query } from '@q/app/state/atoms/app-state';
import { C, CR, Q, T } from '@q/ui';
import { Icon, PupilScanCard, PupilScanCardProps } from '@q/ui/ds/elements';
import { useQuery } from '@tanstack/react-query';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { ActivityIndicator } from 'react-native';

import { QRScanner } from './lib';
import { QRCodeError } from './lib/QRCodeError';
import { PanelTitle } from '../shared/comps/PanelContainer';
import { envFlags } from '@q/app/env';

interface QRCodeViewProps {
  onRequestClose: () => void;
}

export function QRCodeView({ onRequestClose }: QRCodeViewProps) {
  const [error, setError] = useState<unknown | undefined>(undefined);
  const [lastQueryErrorCount, setLastQueryErrorCount] = useState(0);
  const [detected, setDetected] = useState<string | undefined>(undefined);
  const [checkingOut, setCheckingOut] = useState(false);
  const myHappening = useMyCurrentDashbaordInfo()?.data?.my_happening;
  console.info('Rerender QRCodeView');

  useEffect(() => {
    if (!myHappening?.id) {
      onRequestClose?.();
    }
  }, [myHappening?.id]);

  const handleQRError = useCallback((error: QRCodeError) => {
    if (error.type === 'NoQRData') {
      return;
    }
    setError(error);
  }, []);

  const {
    data: pupilInfo,
    isFetching,
    error: queryError,
    errorUpdateCount,
  } = useQuery({
    queryKey: ['pupilInfo', detected],
    queryFn: async () => {
      if (!detected && !myHappening?.id) return;
      try {
        const result = await checkInMutation(detected, myHappening?.id);

        return result;
      } catch (e) {
        setError(e);
        setDetected(undefined);
        return null;
      }
    },
    staleTime: 1000,
    cacheTime: 3000,
    enabled: !!detected,
  });

  // TODO this is strange
  useEffect(() => {
    if (queryError) {
      setLastQueryErrorCount((current) => {
        if (current !== errorUpdateCount) {
          setError(queryError);
        }
        return errorUpdateCount;
      });
    }
  }, [queryError, lastQueryErrorCount]);

  const handleQRDetect = useCallback((data: any) => {
    if (typeof data === 'string') {
      if (data.length !== 32 && !data.includes('-')) {
        handleQRError(new QRCodeError('Invalid QR Code', 'InvalidQRData'));
        return;
      }
      setDetected(data);
    }
  }, []);

  const checkedInId = pupilInfo?.checked_in?.id || pupilInfo?.checked_in_before?.id;

  const canCheckOut = !!(pupilInfo?.id && checkedInId);
  const handleCheckOut = useCallback(() => {
    const sendCheckOut = async () => {
      setCheckingOut(true);
      try {
        if (pupilInfo?.id && checkedInId) {
          await (
            await xclient
          ).check_out({
            member_id: pupilInfo.id,
            happening_id: checkedInId,
            now: now4query(),
          });
          setDetected(undefined);
        }
      } finally {
        setCheckingOut(false);
      }
    };
    sendCheckOut();
  }, [pupilInfo?.id, checkedInId]);

  return (
    <Q.Container bg="white" size="md" className="p-6">
      <CR className="jc-c flex-wrap gap-3">
        <QRPanel onDetect={handleQRDetect} onError={handleQRError} />
        <InfoPanel
          error={error}
          happening={myHappening}
          onRequestClose={onRequestClose}
          pupilInfo={pupilInfo}
          onCheckOut={canCheckOut ? handleCheckOut : undefined}
        />
      </CR>

      {(isFetching || checkingOut) && (
        <C className="fxcc absolute bottom-0 left-0 right-0 top-0 bg-white/50">
          <ActivityIndicator size="large" color="#5B79FF" />
        </C>
      )}
    </Q.Container>
  );
}

function InfoPanel({
  error,
  happening,
  happeningCount,
  onRequestClose,
  onCheckOut,
  pupilInfo,
}: {
  error?: unknown | undefined;
  happening?: ApiDashboard['my_happening'];
  happeningCount?: number;
  onRequestClose: () => void;
  pupilInfo?: any;
  onCheckOut: (() => void) | undefined;
}) {
  const setSelectedList = useSetParticipantsListSelection();
  const handleToListClick = useCallback(() => {
    if (happening) {
      setSelectedList({
        type: 'happening',
        id: happening.id,
        name: happening.name,
      });
    }
    onRequestClose();
  }, [happening, onRequestClose, setSelectedList]);

  return (
    <C className="w-mimobc aspect-square  justify-between overflow-hidden rounded-lg bg-rose-300 p-4 pb-0">
      <PanelTitle title={happening?.name ?? ''} count={happeningCount ?? '-'} />
      <InfoPanelContent
        error={error}
        pupilInfo={pupilInfo}
        onCheckOut={onCheckOut}
        onRequestClose={onRequestClose}
      />

      <Q.TextButton label="Zur Liste" bare iconName="ForwardCircle" onPress={handleToListClick} />
    </C>
  );
}

interface QRViewInfoProps {
  error?: unknown;
  pupilInfo?: MemberCheckedInInfo;
  onCheckOut?: undefined | (() => void);
  onRequestClose: () => void;
}

function InfoPanelContent({ error, pupilInfo, onCheckOut, onRequestClose }: QRViewInfoProps) {
  const setSelectedPupilID = useSetSelectedPupilID();
  const handleSelectPupil = useCallback(() => {
    if (pupilInfo?.id) {
      setSelectedPupilID(pupilInfo);
      onRequestClose();
    }
  }, [pupilInfo]);

  if (error) {
    return (
      <C className="border-1 fxcc fx-1 rounded-md border-dashed border-orange-800">
        <T.PupilCardTitle className="text-center text-orange-800">Fehler</T.PupilCardTitle>
        <T.PupilCardSubtitle className="text-center text-orange-800">
          {error instanceof QRCodeError ? error.message : String(error)}
        </T.PupilCardSubtitle>
      </C>
    );
  }
  if (!pupilInfo) {
    return (
      <C className="border-1 fxcc fx-1 rounded-md border-dashed border-rose-800">
        <T.PupilCardSubtitle className="text-center">
          QR Code scannen, um die Schüler*innen einzuchecken
        </T.PupilCardSubtitle>
      </C>
    );
  }

  if (pupilInfo) {
    const from = pupilInfo.checked_in_before?.name;
    const to = pupilInfo.checked_in?.name;
    let props: PupilScanCardProps = {
      label: pupilInfo.first_name,
      subLabel: pupilInfo.last_name,
      onPress: handleSelectPupil,
    };

    if (from && to) {
      props.children = (
        <CR className="jc-c gap-2">
          <Q.PillSmall label={from} type="checkedin" />
          <Icon name="ArrowForward" size={4} />
          <Q.PillSmall label={to} type="checkedin" checked />
        </CR>
      );
    } else if (to) {
      props = {
        ...props,
        children: <Q.PillSmall label={to} type="checkedin" checked />,
      };
    } else if (from) {
      props = {
        ...props,
        children: (
          <C className="ai-c gap-2">
            <Q.PillSmall label="bereits erfasst" type="missing" rightIcon="Alert" />
            <Q.SelectButton
              disabled={!onCheckOut}
              label="Check-Out"
              intent="danger"
              variant="xs"
              iconName="Out"
              onPress={onCheckOut}
            />
          </C>
        ),
      };
    }

    return <PupilScanCard {...props} />;
  }
  return null;
}

async function checkInMutation(memberId?: string | undefined, happeningId?: string | undefined) {
  if (!memberId || !happeningId)
    throw new Error(`TODO ERROR: missing memberId ${memberId} or happeningId ${happeningId}`);

  const endpoint = envFlags('useKeycloakIds')
    ? '/actions/happening/{happening_id}/check_in/kc/{member_id}'
    : '/actions/happening/{happening_id}/check_in/{member_id}';

  const result = await apiClient.post(endpoint, {
    params: {
      path: {
        member_id: memberId,
        happening_id: happeningId,
      },
      query: {
        now: now4query(),
      },
    },
  });
  await new Promise((resolve) => setTimeout(resolve, 500));

  return result.data;
}

function QRPanel({
  onError,
  onDetect,
}: {
  onError: (error: QRCodeError) => void;
  onDetect: (data: unknown) => void;
}) {
  const onDetectRef = useRef(onDetect);
  const onErrorRef = useRef(onError);
  useEffect(() => {
    onDetectRef.current = onDetect;
    onErrorRef.current = onError;
  }, [onDetect, onError]);

  const handleError = useCallback((error: QRCodeError) => {
    onErrorRef.current(error);
  }, []);
  const handleDetect = useCallback((data: unknown) => {
    onDetectRef.current(data);
  }, []);

  const qrScanner = useMemo(
    () => <QRScanner scan onError={handleError} onDetect={handleDetect} />,
    [],
  );
  return <C className="aspect-square overflow-hidden rounded-lg">{qrScanner}</C>;
}
