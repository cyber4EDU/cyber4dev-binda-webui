import { MemberShortInfo } from '@q/app/api';
import { Q } from '@q/ui';

import { PanelTitle } from '../shared/comps/PanelContainer';

export interface PupilInfoProps {
  happeningName: string;
  happeningCount: number | '-';
  member: MemberShortInfo;
}
export function PupilInfo({ happeningName, happeningCount: count, member }: PupilInfoProps) {
  return (
    <Q.Container size="lg" bg="rose" className=" min-w-mimobc aspect-square gap-6">
      <PanelTitle title={happeningName} count={count} />
      <Q.PupilScanCard
        label={member.first_name}
        subLabel={member.last_name}
        mode="checkedin"
        into="LB NaWi"
      />
      <Q.TextButton label="Zur Liste" bare iconName="ForwardCircle" />
    </Q.Container>
  );
}
