const { createNativewindConfig } = require('@q/ui/tw');

const twConfig = createNativewindConfig({
  relContentDirs: [
    //
    '.',
    '../../packages/ui/src',
  ],
  fixRNWOrder: true,
  darkMode: 'class',
});

module.exports = twConfig;
