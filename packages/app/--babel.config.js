process.env.Q_PLATFORM_TARGET = 'web';

require('ts-node/register');
module.exports = require('@q/cfg/babel').createBabelConfig({
  useRouter: false,
  mode: 'web',
});
