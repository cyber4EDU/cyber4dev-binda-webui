import { Platform } from 'react-native';

import { ClientTarget } from './types';

const qTargetEnv = process?.env?.Q_PLATFORM_TARGET;
const qTargetEnvResolved: ClientTarget = qTargetEnv === 'web' ? 'web' : 'native';
const platformTarget: ClientTarget = Platform.OS === 'web' ? 'web' : 'native';
export const CLIENT_TARGET = qTargetEnv ? qTargetEnvResolved : platformTarget;

if (__DEV__) {
  if (qTargetEnv && qTargetEnv !== qTargetEnvResolved) {
    throw new Error(`Strange Q_PLATFORM_TARGET: ${qTargetEnv}`);
  }
  if (qTargetEnv && qTargetEnvResolved !== platformTarget) {
    throw new Error(`Q_PLATFORM_TARGET does not match Platform ${qTargetEnv} vs. ${Platform.OS}`);
  }
  if (CLIENT_TARGET !== 'native') {
    throw new Error(`ClientTarget in .native file was ${CLIENT_TARGET}?`);
  }
}
