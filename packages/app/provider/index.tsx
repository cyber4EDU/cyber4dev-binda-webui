import { QuantProvider, useAppFonts, useDeviceContext } from '@q/ui';
import { ComponentProps, ReactNode, useEffect } from 'react';
import { useColorScheme } from 'react-native';

import { QueryProvider } from './query';
import { SessionProvider } from './session';
import { SuspenseSplashScreen } from '../features/shared/comps/SplashScreens';

export function Provider({
  children,
  defaultTheme,
  session,
}: {
  children: ReactNode;
  defaultTheme?: 'light' | 'dark' | (string & object);
  session: ComponentProps<typeof SessionProvider>['session'];
}) {
  useDeviceContext();

  const [fontsLoaded] = useAppFonts();

  const scheme = useColorScheme();
  const theme = defaultTheme || (scheme === 'dark' ? 'dark' : 'light');
  useEffect(() => {
    if (typeof document !== 'undefined') {
      const htmlElem = document.querySelector('html');
      htmlElem?.classList.remove('light', 'dark');
      htmlElem?.classList.add(theme);
    }
  }, [theme]);

  if (!fontsLoaded) {
    return <SuspenseSplashScreen />; // TODO fontLoading should be suspensed
  }

  return (
    <SessionProvider session={session}>
      <QueryProvider>
        <QuantProvider>
          {/* <Tester /> */}
          {children}
        </QuantProvider>
      </QueryProvider>
    </SessionProvider>
  );
}
