import dayjs, { ConfigType as DateLike } from 'dayjs';
import durationPlugin from 'dayjs/plugin/duration';
import isBetweenPlugin from 'dayjs/plugin/isBetween';
import isTodayPlugin from 'dayjs/plugin/isToday';

dayjs.extend(isTodayPlugin);
dayjs.extend(durationPlugin);
dayjs.extend(isBetweenPlugin);

const FORMAT_TIME = 'HH:mm';
const FORMAT_DATE_SHORT = 'DD.MM.YY';
const FORMAT_DATE_TIME_SHORT = `${FORMAT_TIME} ${FORMAT_DATE_SHORT}`;

const Formats = {
  time: FORMAT_TIME,
  dateShort: FORMAT_DATE_SHORT,
  dateLong: 'DD.MM.YYYY',
  dateTimeShort: FORMAT_DATE_TIME_SHORT,
  dateTimeWithSecs: `${FORMAT_TIME}:ss ${FORMAT_DATE_SHORT}`,
};

export function formatToQueryDate(dateLike: DateLike) {
  return dayjs(dateLike).toISOString();
}

export function formatToDateTimeInputValue(dateLike: DateLike): string {
  return dayjs(dateLike).format('YYYY-MM-DDTHH:mm:ss');
}

export function formatDate(dateLike: DateLike, mode: keyof typeof Formats) {
  return dayjs(dateLike).format(Formats[mode]);
}

export function startOfDay(dateLike: DateLike) {
  return dayjs(dateLike).startOf('D');
}

export function endOfDay(dateLike: DateLike) {
  return dayjs(dateLike).endOf('D');
}

export function msSinceNow(targetDate: DateLike) {
  return dayjs(targetDate).diff();
}

/**
 * Fromats two dates to a time range
 * @param from {DateLike} start date
 * @param to {DateLike} end date
 * @param mode {'withoutDate' | 'withDateIfDifferent' | 'withDateLast' | 'withDate'}
 *  -
 *  - 'withoutDate' = never show any dates
 *  - 'withDateIfDifferent' (default) = show dates only if the two values are on different days
 *  - 'withDateLast' = show a date after the to date and only if the days are different before from.
 *  - 'withDate' = always show both with dates
 * @returns a formatted string of the timerange
 */
export function formatTimeRange(
  from: DateLike,
  to: DateLike,
  mode:
    | 'withoutDate'
    | 'withDateIfDifferent'
    | 'withDateOnLast'
    | 'withDate' = 'withDateIfDifferent',
) {
  const fromD = dayjs(from);
  const toD = dayjs(to);
  if (mode === 'withDate') {
    return `${fromD.format(FORMAT_DATE_SHORT)} - ${toD.format(FORMAT_DATE_SHORT)}`;
  }

  if (mode === 'withoutDate') {
    return `${fromD.format(FORMAT_TIME)} - ${toD.format(FORMAT_TIME)}`;
  }

  const sameDay = fromD.isSame(toD, 'D');
  if (sameDay) {
    if (mode === 'withDateOnLast') {
      return `${fromD.format(FORMAT_TIME)} - ${toD.format(FORMAT_DATE_TIME_SHORT)}`;
    }
    // if (mode === 'withDateIfDifferent') {
    return `${fromD.format(FORMAT_TIME)} - ${toD.format(FORMAT_TIME)}`;
    // }
  } else {
    return `${fromD.format(FORMAT_DATE_SHORT)} - ${toD.format(FORMAT_DATE_SHORT)}`;
  }
}

export function isBetween(current: DateLike, from: DateLike, to: DateLike) {
  return dayjs(current).isBetween(from, to, 'm', '[]');
}
