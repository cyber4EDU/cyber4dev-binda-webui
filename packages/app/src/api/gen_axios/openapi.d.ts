import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios';

declare namespace Components {
  namespace Schemas {
    export interface Absent {
      canceled: boolean;
      from_date: string; // date-time
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      AbsentId /* uuid */;
      member_id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      reason: AbsentReason;
      until_date: string; // date-time
    }
    /**
     * example:
     * 00000000-0000-0000-0000-000000000000
     */
    export type AbsentId = string; // uuid
    export interface AbsentInfo {
      from: string; // date-time
      reason: AbsentReason;
      until: string; // date-time
    }
    export type AbsentReason = 'Holidays' | 'Ill' | 'Late' | 'Released';
    export interface Dashboard {
      my_happening: HappeningDashboard;
      number_request: NumberRequest;
      related_happenings?: HappeningDashboard[] | null;
    }
    export interface Delay {
      minutes?: null | number; // int32
    }
    export interface Group {
      happenings?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */[] | null;
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */;
      name: /**
       * example:
       * Team A
       */
      GroupName;
      participants?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
      responsibles?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    /**
     * example:
     * 00000000-0000-0000-0000-000000000000
     */
    export type GroupId = string; // uuid
    /**
     * example:
     * Team A
     */
    export type GroupName = string;
    export interface GroupShortInfo {
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */;
      name: /**
       * example:
       * Team A
       */
      GroupName;
    }
    export interface Happening {
      groups?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */[] | null;
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */;
      name: /**
       * example:
       * Sport
       */
      HappeningName;
      participants?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
      props: {
        [key: string]: any;
      };
      responsibles?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    export interface HappeningDashboard {
      groups?: GroupShortInfo[] | null;
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */;
      name: /**
       * example:
       * Sport
       */
      HappeningName;
      props?: {
        from: string; // date-time
        to: string; // date-time
      } | null;
      responsibles?: MemberShortInfo[] | null;
    }
    /**
     * example:
     * 00000000-0000-0000-0000-000000000000
     */
    export type HappeningId = string; // uuid
    /**
     * example:
     * Sport
     */
    export type HappeningName = string;
    export interface HappeningPropsMinimum {
      from: string; // date-time
      to: string; // date-time
    }
    export interface HappeningShortInfo {
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */;
      name: /**
       * example:
       * Sport
       */
      HappeningName;
      props?: {
        from: string; // date-time
        to: string; // date-time
      } | null;
    }
    /**
     * example:
     * 00000000-0000-0000-0000-000000000000
     */
    export type KeycloakId = string; // uuid
    export interface Member {
      first_name: /**
       * example:
       * Erika
       */
      MemberFirstName;
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      keycloak_id?: string | null; // uuid
      last_name: /**
       * example:
       * Musterman
       */
      MemberLastName;
      participant_in?: {
        groups?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        GroupId /* uuid */[] | null;
        happenings?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */[] | null;
      } | null;
      responsible_for?: {
        groups?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        GroupId /* uuid */[] | null;
        happenings?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */[] | null;
        members?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        MemberId /* uuid */[] | null;
      } | null;
      responsibles?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    export interface MemberAbsentInfo {
      first_name: /**
       * example:
       * Erika
       */
      MemberFirstName;
      from: string; // date-time
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      last_name: /**
       * example:
       * Musterman
       */
      MemberLastName;
      reason: AbsentReason;
      until: string; // date-time
    }
    export interface MemberCheckedInInfo {
      checked_in?: {
        id: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */;
        name: /**
         * example:
         * Sport
         */
        HappeningName;
        props?: {
          from: string; // date-time
          to: string; // date-time
        } | null;
      } | null;
      checked_in_before?: {
        id: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */;
        name: /**
         * example:
         * Sport
         */
        HappeningName;
        props?: {
          from: string; // date-time
          to: string; // date-time
        } | null;
      } | null;
      first_name: /**
       * example:
       * Erika
       */
      MemberFirstName;
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      last_name: /**
       * example:
       * Musterman
       */
      MemberLastName;
    }
    /**
     * example:
     * Erika
     */
    export type MemberFirstName = string;
    /**
     * example:
     * 00000000-0000-0000-0000-000000000000
     */
    export type MemberId = string; // uuid
    export interface MemberInfo {
      absent?: {
        from: string; // date-time
        reason: AbsentReason;
        until: string; // date-time
      } | null;
      checked_in?: {
        id: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */;
        name: /**
         * example:
         * Sport
         */
        HappeningName;
        props?: {
          from: string; // date-time
          to: string; // date-time
        } | null;
      } | null;
      first_name: /**
       * example:
       * Erika
       */
      MemberFirstName;
      groups: GroupShortInfo[];
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      last_name: /**
       * example:
       * Musterman
       */
      MemberLastName;
    }
    /**
     * example:
     * Musterman
     */
    export type MemberLastName = string;
    export interface MemberShortInfo {
      first_name: /**
       * example:
       * Erika
       */
      MemberFirstName;
      id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      last_name: /**
       * example:
       * Musterman
       */
      MemberLastName;
    }
    export interface NewAbsent {
      canceled: boolean;
      from_date: string; // date-time
      member_id: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */;
      reason: AbsentReason;
      until_date: string; // date-time
    }
    export interface NewGroup {
      happenings?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */[] | null;
      name: /**
       * example:
       * Team A
       */
      GroupName;
      participants?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
      responsibles?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    export interface NewHappening {
      groups?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */[] | null;
      name: /**
       * example:
       * Sport
       */
      HappeningName;
      participants?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
      props: {
        [key: string]: any;
      };
      responsibles?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    export interface NewMember {
      first_name: /**
       * example:
       * Erika
       */
      MemberFirstName;
      /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      keycloak_id?: string | null; // uuid
      last_name: /**
       * example:
       * Musterman
       */
      MemberLastName;
      participant_in?: {
        groups?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        GroupId /* uuid */[] | null;
        happenings?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */[] | null;
      } | null;
      responsible_for?: {
        groups?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        GroupId /* uuid */[] | null;
        happenings?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        HappeningId /* uuid */[] | null;
        members?: /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        MemberId /* uuid */[] | null;
      } | null;
      responsibles?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    export interface NumberRequest {
      groups: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */[];
      happenings: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */[];
      now?: string | null; // date-time
    }
    export interface NumberResponse {
      absent: number; // int32
      expected: number; // int32
      happenings: {
        [name: string]: number; // int32
      };
      missing: number; // int32
    }
    export interface ParticipantIn {
      groups?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */[] | null;
      happenings?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */[] | null;
    }
    export interface ResponsibleFor {
      groups?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      GroupId /* uuid */[] | null;
      happenings?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      HappeningId /* uuid */[] | null;
      members?: /**
       * example:
       * 00000000-0000-0000-0000-000000000000
       */
      MemberId /* uuid */[] | null;
    }
    export interface TimeData {
      now?: string | null; // date-time
    }
    export interface TimeFrame {
      not_after: string; // date-time
      not_before: string; // date-time
    }
  }
}
declare namespace Paths {
  namespace CheckIn {
    namespace Parameters {
      export type HappeningId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type MemberId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.MemberId /* uuid */;
      export type Minutes = null | number; // int32
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      happening_id: Parameters.HappeningId;
      member_id: Parameters.MemberId;
    }
    export interface QueryParameters {
      minutes?: Parameters.Minutes /* int32 */;
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberCheckedInInfo;
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace CheckInKc {
    namespace Parameters {
      export type HappeningId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type MemberId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.KeycloakId /* uuid */;
      export type Minutes = null | number; // int32
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      happening_id: Parameters.HappeningId;
      member_id: Parameters.MemberId;
    }
    export interface QueryParameters {
      minutes?: Parameters.Minutes /* int32 */;
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberCheckedInInfo;
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace CheckOut {
    namespace Parameters {
      export type HappeningId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type MemberId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.MemberId /* uuid */;
      export type Minutes = null | number; // int32
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      happening_id: Parameters.HappeningId;
      member_id: Parameters.MemberId;
    }
    export interface QueryParameters {
      minutes?: Parameters.Minutes /* int32 */;
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export interface $201 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace CheckOutKc {
    namespace Parameters {
      export type HappeningId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type MemberId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.KeycloakId /* uuid */;
      export type Minutes = null | number; // int32
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      happening_id: Parameters.HappeningId;
      member_id: Parameters.MemberId;
    }
    export interface QueryParameters {
      minutes?: Parameters.Minutes /* int32 */;
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export interface $201 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace Close {
    namespace Parameters {
      export type HappeningId =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type Minutes = null | number; // int32
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      happening_id: Parameters.HappeningId;
    }
    export interface QueryParameters {
      minutes?: Parameters.Minutes /* int32 */;
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export interface $201 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace CreateAbsent {
    export type RequestBody = Components.Schemas.NewAbsent;
    namespace Responses {
      export type $201 = Components.Schemas.Absent;
      export interface $400 {}
      export interface $401 {}
      export interface $409 {}
    }
  }
  namespace CreateGroup {
    export type RequestBody = Components.Schemas.NewGroup;
    namespace Responses {
      export type $201 = Components.Schemas.Group;
      export interface $400 {}
      export interface $401 {}
      export interface $409 {}
    }
  }
  namespace CreateHappening {
    export type RequestBody = Components.Schemas.NewHappening;
    namespace Responses {
      export type $201 = Components.Schemas.Happening;
      export interface $400 {}
      export interface $401 {}
      export interface $409 {}
    }
  }
  namespace CreateMember {
    export type RequestBody = Components.Schemas.NewMember;
    namespace Responses {
      export type $201 = Components.Schemas.Member;
      export interface $400 {}
      export interface $401 {}
      export interface $409 {}
    }
  }
  namespace DeleteAbsent {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.AbsentId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export interface $204 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace DeleteGroup {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.GroupId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export interface $204 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace DeleteHappening {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export interface $204 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace DeleteMember {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.MemberId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export interface $204 {}
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace GetAbsent {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.AbsentId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.Absent;
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace GetAllAbsentGroupMembers {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.GroupId /* uuid */;
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberAbsentInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetAllCheckedInHappeningMembers {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberShortInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetAllGroupMembers {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.GroupId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberShortInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetAllHappeningMembers {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberShortInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetAllMembers {
    namespace Responses {
      export type $200 = Components.Schemas.MemberShortInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetAllMissingGroupMembers {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.GroupId /* uuid */;
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberShortInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetGroup {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.GroupId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.Group;
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace GetHappening {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.Happening;
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace GetMe {
    namespace Responses {
      export type $200 = Components.Schemas.MemberShortInfo;
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetMeDashboard {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.HappeningId /* uuid */;
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.Dashboard;
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetMeHappenings {
    namespace Parameters {
      export type NotAfter = string; // date-time
      export type NotBefore = string; // date-time
    }
    export interface QueryParameters {
      not_before: Parameters.NotBefore /* date-time */;
      not_after: Parameters.NotAfter /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.HappeningShortInfo[];
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetMeNumbers {
    namespace Parameters {
      export type Now = string | null; // date-time
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    export type RequestBody = Components.Schemas.NumberRequest;
    namespace Responses {
      export type $200 = Components.Schemas.NumberResponse;
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetMember {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.MemberId /* uuid */;
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    namespace Responses {
      export type $200 = Components.Schemas.Member;
      export interface $401 {}
      export interface $403 {}
      export interface $404 {}
    }
  }
  namespace GetMemberInfo {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.MemberId /* uuid */;
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberInfo;
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace GetMemberInfoKeycload {
    namespace Parameters {
      export type Id =
        /**
         * example:
         * 00000000-0000-0000-0000-000000000000
         */
        Components.Schemas.KeycloakId /* uuid */;
      export type Now = string | null; // date-time
    }
    export interface PathParameters {
      id: Parameters.Id;
    }
    export interface QueryParameters {
      now?: Parameters.Now /* date-time */;
    }
    namespace Responses {
      export type $200 = Components.Schemas.MemberInfo;
      export interface $401 {}
      export interface $403 {}
    }
  }
  namespace ModifyAbsent {
    export type RequestBody = Components.Schemas.Absent;
    namespace Responses {
      export type $200 = Components.Schemas.Absent;
      export interface $400 {}
      export interface $401 {}
      export interface $404 {}
    }
  }
  namespace ModifyGroup {
    export type RequestBody = Components.Schemas.Group;
    namespace Responses {
      export type $200 = Components.Schemas.Group;
      export interface $400 {}
      export interface $401 {}
      export interface $404 {}
    }
  }
  namespace ModifyHappening {
    export type RequestBody = Components.Schemas.Happening;
    namespace Responses {
      export type $200 = Components.Schemas.Happening;
      export interface $400 {}
      export interface $401 {}
      export interface $404 {}
    }
  }
  namespace ModifyMember {
    export type RequestBody = Components.Schemas.Member;
    namespace Responses {
      export type $200 = Components.Schemas.Member;
      export interface $400 {}
      export interface $401 {}
      export interface $404 {}
    }
  }
  namespace Status {
    namespace Responses {
      export interface $200 {}
    }
  }
  namespace Version {
    namespace Responses {
      export type $200 = string;
    }
  }
}

export interface OperationMethods {
  /**
   * check_in_kc
   */
  'check_in_kc'(
    parameters?: Parameters<
      Paths.CheckInKc.PathParameters & Paths.CheckInKc.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CheckInKc.Responses.$200>;
  /**
   * check_in
   */
  'check_in'(
    parameters?: Parameters<Paths.CheckIn.PathParameters & Paths.CheckIn.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CheckIn.Responses.$200>;
  /**
   * check_out_kc
   */
  'check_out_kc'(
    parameters?: Parameters<
      Paths.CheckOutKc.PathParameters & Paths.CheckOutKc.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CheckOutKc.Responses.$201>;
  /**
   * check_out
   */
  'check_out'(
    parameters?: Parameters<Paths.CheckOut.PathParameters & Paths.CheckOut.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CheckOut.Responses.$201>;
  /**
   * close
   */
  'close'(
    parameters?: Parameters<Paths.Close.PathParameters & Paths.Close.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.Close.Responses.$201>;
  /**
   * modify_absent
   */
  'modify_absent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.ModifyAbsent.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.ModifyAbsent.Responses.$200>;
  /**
   * create_absent
   */
  'create_absent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateAbsent.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CreateAbsent.Responses.$201>;
  /**
   * get_absent
   */
  'get_absent'(
    parameters?: Parameters<Paths.GetAbsent.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAbsent.Responses.$200>;
  /**
   * delete_absent
   */
  'delete_absent'(
    parameters?: Parameters<Paths.DeleteAbsent.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.DeleteAbsent.Responses.$204>;
  /**
   * modify_group
   */
  'modify_group'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.ModifyGroup.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.ModifyGroup.Responses.$200>;
  /**
   * create_group
   */
  'create_group'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateGroup.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CreateGroup.Responses.$201>;
  /**
   * get_group
   */
  'get_group'(
    parameters?: Parameters<Paths.GetGroup.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetGroup.Responses.$200>;
  /**
   * delete_group
   */
  'delete_group'(
    parameters?: Parameters<Paths.DeleteGroup.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.DeleteGroup.Responses.$204>;
  /**
   * modify_happening
   */
  'modify_happening'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.ModifyHappening.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.ModifyHappening.Responses.$200>;
  /**
   * create_happening
   */
  'create_happening'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateHappening.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CreateHappening.Responses.$201>;
  /**
   * get_happening
   */
  'get_happening'(
    parameters?: Parameters<Paths.GetHappening.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetHappening.Responses.$200>;
  /**
   * delete_happening
   */
  'delete_happening'(
    parameters?: Parameters<Paths.DeleteHappening.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.DeleteHappening.Responses.$204>;
  /**
   * modify_member
   */
  'modify_member'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.ModifyMember.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.ModifyMember.Responses.$200>;
  /**
   * create_member
   */
  'create_member'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateMember.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.CreateMember.Responses.$201>;
  /**
   * get_member
   */
  'get_member'(
    parameters?: Parameters<Paths.GetMember.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMember.Responses.$200>;
  /**
   * delete_member
   */
  'delete_member'(
    parameters?: Parameters<Paths.DeleteMember.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.DeleteMember.Responses.$204>;
  /**
   * get_all_members
   */
  'get_all_members'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAllMembers.Responses.$200>;
  /**
   * get_all_group_members
   */
  'get_all_group_members'(
    parameters?: Parameters<Paths.GetAllGroupMembers.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAllGroupMembers.Responses.$200>;
  /**
   * get_all_absent_group_members
   */
  'get_all_absent_group_members'(
    parameters?: Parameters<
      Paths.GetAllAbsentGroupMembers.PathParameters & Paths.GetAllAbsentGroupMembers.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAllAbsentGroupMembers.Responses.$200>;
  /**
   * get_all_missing_group_members
   */
  'get_all_missing_group_members'(
    parameters?: Parameters<
      Paths.GetAllMissingGroupMembers.PathParameters &
        Paths.GetAllMissingGroupMembers.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAllMissingGroupMembers.Responses.$200>;
  /**
   * get_all_happening_members
   */
  'get_all_happening_members'(
    parameters?: Parameters<Paths.GetAllHappeningMembers.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAllHappeningMembers.Responses.$200>;
  /**
   * get_all_checked_in_happening_members
   */
  'get_all_checked_in_happening_members'(
    parameters?: Parameters<
      Paths.GetAllCheckedInHappeningMembers.PathParameters &
        Paths.GetAllCheckedInHappeningMembers.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetAllCheckedInHappeningMembers.Responses.$200>;
  /**
   * get_member_info_keycload
   */
  'get_member_info_keycload'(
    parameters?: Parameters<
      Paths.GetMemberInfoKeycload.PathParameters & Paths.GetMemberInfoKeycload.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMemberInfoKeycload.Responses.$200>;
  /**
   * get_member_info
   */
  'get_member_info'(
    parameters?: Parameters<
      Paths.GetMemberInfo.PathParameters & Paths.GetMemberInfo.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMemberInfo.Responses.$200>;
  /**
   * get_me
   */
  'get_me'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMe.Responses.$200>;
  /**
   * get_me_numbers
   */
  'get_me_numbers'(
    parameters?: Parameters<Paths.GetMeNumbers.QueryParameters> | null,
    data?: Paths.GetMeNumbers.RequestBody,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMeNumbers.Responses.$200>;
  /**
   * get_me_dashboard
   */
  'get_me_dashboard'(
    parameters?: Parameters<
      Paths.GetMeDashboard.PathParameters & Paths.GetMeDashboard.QueryParameters
    > | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMeDashboard.Responses.$200>;
  /**
   * get_me_happenings
   */
  'get_me_happenings'(
    parameters?: Parameters<Paths.GetMeHappenings.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.GetMeHappenings.Responses.$200>;
  /**
   * status
   */
  'status'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.Status.Responses.$200>;
  /**
   * version
   */
  'version'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig,
  ): OperationResponse<Paths.Version.Responses.$200>;
}

export interface PathsDictionary {
  ['/actions/happening/{happening_id}/check_in/kc/{member_id}']: {
    /**
     * check_in_kc
     */
    'post'(
      parameters?: Parameters<
        Paths.CheckInKc.PathParameters & Paths.CheckInKc.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CheckInKc.Responses.$200>;
  };
  ['/actions/happening/{happening_id}/check_in/{member_id}']: {
    /**
     * check_in
     */
    'post'(
      parameters?: Parameters<Paths.CheckIn.PathParameters & Paths.CheckIn.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CheckIn.Responses.$200>;
  };
  ['/actions/happening/{happening_id}/check_out/kc/{member_id}']: {
    /**
     * check_out_kc
     */
    'post'(
      parameters?: Parameters<
        Paths.CheckOutKc.PathParameters & Paths.CheckOutKc.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CheckOutKc.Responses.$201>;
  };
  ['/actions/happening/{happening_id}/check_out/{member_id}']: {
    /**
     * check_out
     */
    'post'(
      parameters?: Parameters<
        Paths.CheckOut.PathParameters & Paths.CheckOut.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CheckOut.Responses.$201>;
  };
  ['/actions/happening/{happening_id}/close']: {
    /**
     * close
     */
    'post'(
      parameters?: Parameters<Paths.Close.PathParameters & Paths.Close.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.Close.Responses.$201>;
  };
  ['/admin/absent']: {
    /**
     * create_absent
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateAbsent.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CreateAbsent.Responses.$201>;
    /**
     * modify_absent
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.ModifyAbsent.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.ModifyAbsent.Responses.$200>;
  };
  ['/admin/absent/{id}']: {
    /**
     * get_absent
     */
    'get'(
      parameters?: Parameters<Paths.GetAbsent.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAbsent.Responses.$200>;
    /**
     * delete_absent
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteAbsent.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.DeleteAbsent.Responses.$204>;
  };
  ['/admin/group']: {
    /**
     * create_group
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateGroup.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CreateGroup.Responses.$201>;
    /**
     * modify_group
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.ModifyGroup.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.ModifyGroup.Responses.$200>;
  };
  ['/admin/group/{id}']: {
    /**
     * get_group
     */
    'get'(
      parameters?: Parameters<Paths.GetGroup.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetGroup.Responses.$200>;
    /**
     * delete_group
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteGroup.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.DeleteGroup.Responses.$204>;
  };
  ['/admin/happening']: {
    /**
     * create_happening
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateHappening.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CreateHappening.Responses.$201>;
    /**
     * modify_happening
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.ModifyHappening.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.ModifyHappening.Responses.$200>;
  };
  ['/admin/happening/{id}']: {
    /**
     * get_happening
     */
    'get'(
      parameters?: Parameters<Paths.GetHappening.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetHappening.Responses.$200>;
    /**
     * delete_happening
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteHappening.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.DeleteHappening.Responses.$204>;
  };
  ['/admin/member']: {
    /**
     * create_member
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateMember.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.CreateMember.Responses.$201>;
    /**
     * modify_member
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.ModifyMember.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.ModifyMember.Responses.$200>;
  };
  ['/admin/member/{id}']: {
    /**
     * get_member
     */
    'get'(
      parameters?: Parameters<Paths.GetMember.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMember.Responses.$200>;
    /**
     * delete_member
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteMember.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.DeleteMember.Responses.$204>;
  };
  ['/admin/members']: {
    /**
     * get_all_members
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAllMembers.Responses.$200>;
  };
  ['/info/group/{id}/participants']: {
    /**
     * get_all_group_members
     */
    'get'(
      parameters?: Parameters<Paths.GetAllGroupMembers.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAllGroupMembers.Responses.$200>;
  };
  ['/info/group/{id}/participants/absent']: {
    /**
     * get_all_absent_group_members
     */
    'get'(
      parameters?: Parameters<
        Paths.GetAllAbsentGroupMembers.PathParameters &
          Paths.GetAllAbsentGroupMembers.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAllAbsentGroupMembers.Responses.$200>;
  };
  ['/info/group/{id}/participants/missing']: {
    /**
     * get_all_missing_group_members
     */
    'get'(
      parameters?: Parameters<
        Paths.GetAllMissingGroupMembers.PathParameters &
          Paths.GetAllMissingGroupMembers.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAllMissingGroupMembers.Responses.$200>;
  };
  ['/info/happening/{id}/participants']: {
    /**
     * get_all_happening_members
     */
    'get'(
      parameters?: Parameters<Paths.GetAllHappeningMembers.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAllHappeningMembers.Responses.$200>;
  };
  ['/info/happening/{id}/participants/checked_in']: {
    /**
     * get_all_checked_in_happening_members
     */
    'get'(
      parameters?: Parameters<
        Paths.GetAllCheckedInHappeningMembers.PathParameters &
          Paths.GetAllCheckedInHappeningMembers.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetAllCheckedInHappeningMembers.Responses.$200>;
  };
  ['/info/member/kc/{id}']: {
    /**
     * get_member_info_keycload
     */
    'get'(
      parameters?: Parameters<
        Paths.GetMemberInfoKeycload.PathParameters & Paths.GetMemberInfoKeycload.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMemberInfoKeycload.Responses.$200>;
  };
  ['/info/member/{id}']: {
    /**
     * get_member_info
     */
    'get'(
      parameters?: Parameters<
        Paths.GetMemberInfo.PathParameters & Paths.GetMemberInfo.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMemberInfo.Responses.$200>;
  };
  ['/me']: {
    /**
     * get_me
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMe.Responses.$200>;
  };
  ['/me/dashboard/numbers']: {
    /**
     * get_me_numbers
     */
    'post'(
      parameters?: Parameters<Paths.GetMeNumbers.QueryParameters> | null,
      data?: Paths.GetMeNumbers.RequestBody,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMeNumbers.Responses.$200>;
  };
  ['/me/dashboard/{id}']: {
    /**
     * get_me_dashboard
     */
    'get'(
      parameters?: Parameters<
        Paths.GetMeDashboard.PathParameters & Paths.GetMeDashboard.QueryParameters
      > | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMeDashboard.Responses.$200>;
  };
  ['/me/happenings']: {
    /**
     * get_me_happenings
     */
    'get'(
      parameters?: Parameters<Paths.GetMeHappenings.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.GetMeHappenings.Responses.$200>;
  };
  ['/status']: {
    /**
     * status
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.Status.Responses.$200>;
  };
  ['/version']: {
    /**
     * version
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig,
    ): OperationResponse<Paths.Version.Responses.$200>;
  };
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>;
