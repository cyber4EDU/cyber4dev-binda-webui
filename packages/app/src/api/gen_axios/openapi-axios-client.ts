import OpenAPIClientAxios from 'openapi-client-axios';

import type { Client as BindaClient } from './openapi.d.ts';
import definition from './openapi_definition.json';
import { getCurrentAuthToken } from '../../auth/auth-state';
import { envTenant } from '../../env/index';

export const axiosClient = new OpenAPIClientAxios({
  definition: definition as any,
});

export const xclient = axiosClient.init<BindaClient>().then((client) => {
  client.interceptors.request.use((config) => {
    config.baseURL = envTenant('apiBaseUrl');
    const authToken = getCurrentAuthToken();
    if (authToken.status === 'authenticated' && authToken.accessToken) {
      config.headers.Authorization = `Bearer ${authToken.accessToken}`;
    }
    // console.info('>>>>>>>>>>>>>>>>>>> sending request config', config, authToken);

    return config;
  });
  return client;
});
