import { components } from './gen/api-schema';

type Schemas = components['schemas'];

export type ApiDashboard = Schemas['Dashboard'];
export type ApiHappening = Schemas['Happening'];
export type ApiMember = Schemas['Member'];
export type ApiMemberShortInfo = Schemas['MemberShortInfo'];
export type ApiHappeningDashboard = Schemas['HappeningDashboard'];
export type ApiGroup = Schemas['Group'];
export type MemberCheckedInInfo = Schemas['MemberCheckedInInfo'];
export type MemberAbsentInfo = Schemas['MemberAbsentInfo'];

export type Identifyiable<T extends { id: string }> = Partial<Omit<T, 'id'>> & { id: string };
