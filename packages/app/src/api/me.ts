import { atom, useAtomValue } from 'jotai';

import { createAtomsWithQuery } from './client';
import { xclient } from './gen_axios/openapi-axios-client';
import { $AuthToken } from '../auth/auth-state';

const [$MeQuery] = createAtomsWithQuery(() => {
  return {
    queryKey: ['me'],
    queryFn: async () => (await xclient).get_me(),
  };
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const $Me = atom(async (get) => {
  const authToken = get($AuthToken);
  if (authToken.status === 'authenticated') {
    return (await xclient).get_me();
    // return apiClient.get('/me', {});
  }
  return null;
});

export function useMe() {
  return useAtomValue($MeQuery);
}
