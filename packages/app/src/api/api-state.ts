import { atom, useAtom, useAtomValue, useSetAtom } from 'jotai';

import { ApiGroup, ApiHappening, Identifyiable } from './api-types';
import { apiClient, createAtomsWithQuery, createAtomsWithQueryAsync } from './client';
import { xclient } from './gen_axios/openapi-axios-client';
import { $Now, $NowRefresh, $SelectedDate, now4query } from '../state/atoms/app-state';
import { formatToQueryDate, isBetween } from '../utils/date-utils';

type DateLike = Date | number | string;
type ID = string;

function toStartOfDay(date: DateLike) {
  const newDate = new Date(date);
  newDate.setHours(0);
  newDate.setMinutes(0);
  newDate.setSeconds(0);
  newDate.setMilliseconds(0);
  return newDate;
}

function toEndOfDate(date: DateLike) {
  const newDate = new Date(date);
  newDate.setHours(23);
  newDate.setMinutes(59);
  newDate.setSeconds(59);
  newDate.setMilliseconds(999);
  return newDate;
}

function toDbDateString(date: DateLike) {
  const dbDate = new Date(date);
  return dbDate.toISOString();
}

async function fetchMeHappeningsByDate(date: DateLike) {
  const startOfDay = toDbDateString(toStartOfDay(date));
  const endOfDay = toDbDateString(toEndOfDate(date));
  const api = await xclient;
  return api.get_me_happenings({
    not_after: endOfDay,
    not_before: startOfDay,
  });
}

// function ofetchMeHappeningsByDate(date: DateLike) {
//   const startOfDay = toDbDateString(toStartOfDay(date));
//   const endOfDay = toDbDateString(toEndOfDate(date));
//   return apiClient
//     .get('/me/happenings', {
//       params: {
//         query: {
//           not_after: endOfDay,
//           not_before: startOfDay,
//         },
//       },
//     })
//     .then((res) => {
//       console.info('>>>>>>>>>>>>>>>>>>> mehappenings res', res);
//       if (!res.response.ok) {
//         const error = { ...res.response, message: res.error };
//         throw error;
//       }

//       return res;
//     })
//     .catch((err) => {
//       console.error('>>>>>>>>>>>>>>>>>>> mehappenings err', err);
//       throw err;
//     });
// }

const [$MyHappeningsQuery] = createAtomsWithQuery((get) => {
  const selectedDate = get($SelectedDate);
  return {
    queryKey: ['me-happenings', formatToQueryDate(selectedDate)] as const,
    queryFn: () => fetchMeHappeningsByDate(selectedDate).then((happenings) => happenings.data),
  };
});

const $MyCurrentHappening = atom(async (get) => {
  const myHappenings = (await get($MyHappeningsQuery)) ?? [];
  const now = get($Now);
  return myHappenings.find((h) => isBetween(now, h.props?.from, h.props?.to));
});

const $SelectedHappening = atom(null as ApiHappening | null);

const [$CurrentDashboardInfoQuery] = createAtomsWithQueryAsync(async (get) => {
  const selectedHappening = await get($MyCurrentHappening);
  return {
    queryKey: ['me-dashboard-info', selectedHappening?.id ?? ''],
    queryFn: async () => {
      if (!selectedHappening) {
        return null;
      }
      return apiClient.get('/me/dashboard/{id}', {
        params: {
          path: {
            id: selectedHappening.id,
          },
          query: {
            now: selectedHappening.props?.from,
          },
        },
      });
    },
  };
});

const [$CurrentDashboardNumbers] = createAtomsWithQueryAsync(async (get) => {
  const currentDashboard = await get($CurrentDashboardInfoQuery);
  const numReq = currentDashboard?.data?.number_request;
  const keys = numReq ? [...numReq.groups, ...numReq.happenings] : [0, 0];
  const now = get($NowRefresh);
  return {
    queryKey: ['me-dashboard-nums', ...keys],
    queryFn: async () => {
      if (!numReq) return null;
      return apiClient.post('/me/dashboard/numbers', {
        body: {
          ...numReq,
        },
        params: {
          query: {
            now: formatToQueryDate(now),
          },
        },
      });
    },
    refetchInterval: 1000,
    cacheTime: 1000,
  };
});

// ================ Lists ================================

type PaticipantsListSelectionProps =
  | {
      type: 'group';
      id: 'absent' | 'missing';
      groups: Identifyiable<ApiGroup>[];
    }
  | {
      type: 'happening';
      name: string;
      id: string & Record<never, never>;
    };

const $PaticipantsListSelection = atom(null as PaticipantsListSelectionProps | null);

const [$CurrentParticipantsList] = createAtomsWithQuery((get) => {
  const selectedList = get($PaticipantsListSelection);
  const group = selectedList?.type === 'group' ? selectedList.groups[0] : null;
  const nowTs = get($NowRefresh);
  const now = formatToQueryDate(nowTs);
  return {
    queryKey: ['me-sellist', selectedList?.type, selectedList?.id, group],
    queryFn: async () => {
      if (!selectedList) return null;
      if (selectedList.id === 'absent' && group) {
        return apiClient.get('/info/group/{id}/participants/absent', {
          params: {
            path: {
              id: group.id,
            },
            query: {
              now,
            },
          },
        });
      }
      if (selectedList.id === 'missing' && group) {
        return apiClient.get('/info/group/{id}/participants/missing', {
          params: {
            path: {
              id: group.id,
            },
            query: {
              now,
            },
          },
        });
      }
      if (selectedList.id) {
        return apiClient.get('/info/happening/{id}/participants/checked_in', {
          params: {
            path: {
              id: selectedList.id,
            },
            query: {
              now,
            },
          },
        });
      }
      return null;
    },
    refetchInterval: 1000,
    cacheTime: 500,
  };
});

// --- selected pupil ----
type MinimalPupilInfo = { id: ID; first_name?: string; last_name?: string };
const $SelectedPupilID = atom(null as MinimalPupilInfo | null);
const $SelectedPupilInfo = atom(
  async (get) => {
    const pupil = get($SelectedPupilID);
    if (!pupil) return null;
    const result = await apiClient.get('/info/member/{id}', {
      params: {
        path: {
          id: pupil.id,
        },
        query: {
          now: now4query(),
        },
      },
    });
    return result.data;
  },
  (_, set, newPupilId: MinimalPupilInfo | null) => {
    set($SelectedPupilID, newPupilId);
  },
);

export function useSelectedPupilInfo() {
  return useAtomValue($SelectedPupilInfo);
}

export function useSelectedPupilId() {
  return useAtomValue($SelectedPupilID);
}

export function useSetSelectedPupilID() {
  return useSetAtom($SelectedPupilInfo);
}

export function useSelectedPupilInfoState() {
  return useAtom($SelectedPupilInfo);
}

// ===========
export function useMyHappeningsToday() {
  return useAtomValue($MyHappeningsQuery);
}

export function useSelectedHappening() {
  return useAtom($SelectedHappening);
}

export function useMyCurrentHappening() {
  return useAtomValue($MyCurrentHappening);
}

export function useMyCurrentDashbaordInfo() {
  return useAtomValue($CurrentDashboardInfoQuery);
}

export function useMyCurrentDashboardNumbers() {
  return useAtomValue($CurrentDashboardNumbers);
}

export function useParticipantsListSelection() {
  return useAtomValue($PaticipantsListSelection);
}

export function useSetParticipantsListSelection() {
  return useSetAtom($PaticipantsListSelection);
}

export function useParticipantsListSelectionState() {
  return useAtom($PaticipantsListSelection);
}

export function useCurrentParticipantsList() {
  return useAtomValue($CurrentParticipantsList);
}
