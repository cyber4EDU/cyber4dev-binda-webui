import { QueryCache, QueryClient } from '@tanstack/react-query';
import { atomsWithMutation, atomsWithQuery, atomsWithQueryAsync } from 'jotai-tanstack-query';
import createClient from 'openapi-fetch';

import { paths } from './gen/api-schema';
import { xclient } from './gen_axios/openapi-axios-client';
import { subscribeToAuthToken } from '../auth/auth-state';
import { envTenant } from '../env';

const state = {
  authToken: undefined as string | undefined,
  client: undefined as ReturnType<typeof createClient<paths>> | undefined,
};

const get: ReturnType<typeof createClient<paths>>['get'] = async (...args) => {
  if (!state.client) {
    console.info('>>>>>>>>>>>>>>>>>>> creating new client', envTenant('apiBaseUrl'));
    state.client = createClient<paths>({
      baseUrl: envTenant('apiBaseUrl'),
      headers: state.authToken
        ? {
            Authorization: `Bearer ${state.authToken}`,
          }
        : {},
    });
  }
  return state.client.get(...args);
};

const post: ReturnType<typeof createClient<paths>>['post'] = async (...args) => {
  if (!state.client) {
    console.info('>>>>>>>>>>>>>>>>>>> creating new client', envTenant('apiBaseUrl'));
    state.client = createClient<paths>({
      baseUrl: envTenant('apiBaseUrl'),
      headers: state.authToken
        ? {
            Authorization: `Bearer ${state.authToken}`,
          }
        : {},
    });
  }
  return state.client.post(...args);
};

export const apiClient = {
  get,
  post,
};

subscribeToAuthToken(async (authToken) => {
  if (authToken.status === 'authenticated' && authToken.accessToken) {
    state.client = undefined;
    state.authToken = authToken.accessToken;
    const axiosClient = await xclient;
    axiosClient.api.axiosConfigDefaults.headers = {
      ...axiosClient.api.axiosConfigDefaults.headers,
      Authorization: `Bearer ${authToken.accessToken}`,
    };
  } else {
    state.client = undefined;
    state.authToken = undefined;
    const axiosClient = await xclient;
    const { Authorization, ...headers } = axiosClient.api.axiosConfigDefaults.headers || {};
    axiosClient.api.axiosConfigDefaults.headers = headers;
  }
});

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 1000 * 60 * 5,
      refetchOnWindowFocus: true,
    },
  },
  queryCache: new QueryCache({
    onError: (err) => {
      console.error('>>>>>>>>>>>>>>>>>>> queryCache err', err);
      console.dir(err);
    },
  }),
});

export const getQueryClient = () => queryClient;
export const createAtomsWithQuery: typeof atomsWithQuery = (getQuery, getClient) =>
  atomsWithQuery(getQuery, getClient ?? getQueryClient);
export const createAtomsWithMutation: typeof atomsWithMutation = (getQuery, getClient) =>
  atomsWithMutation(getQuery, getClient ?? getQueryClient);
export const createAtomsWithQueryAsync: typeof atomsWithQueryAsync = (getQuery, getClient) =>
  atomsWithQueryAsync(getQuery, getClient ?? getQueryClient);
