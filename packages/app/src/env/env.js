// js file, so we can use it in the next.config.js file
const TENANT = {
  name: process.env['NEXT_PUBLIC_TENANT_NAME'] || '',
  type: process.env['NEXT_PUBLIC_TENANT_TYPE'] || 'S',
  apiBaseUrl: process.env['NEXT_PUBLIC_API_BASE_URL'],
};

const APP = {
  testMode: process.env['NEXT_PUBLIC_TEST_MODE'] === 'true',
  platform: process.env['Q_PLATFORM_TARGET'] || process.env['NEXT_PUBLIC_Q_PLATFORM_TARGET'],
  qrType:
    process.env['NEXT_PUBLIC_QRCODE_TYPE'] ||
    (process.env['NEXT_PUBLIC_USE_KEYCLOAK_IDS'] ? 'identity' : 'physical'),
};

const SERVER = {
  nextAuthUrl: process.env['NEXTAUTH_URL'],
  keycloakId: process.env['KEYCLOAK_ID'],
  keycloakSecret: process.env['KEYCLOAK_SECRET'],
  keycloakIssuer: process.env['KEYCLOAK_ISSUER'],
  jwtSecret: process.env['JWT_SECRET'],
};

const FLAGS = {
  // TODO change this, as we now have 3 different types of possible QR codes
  useKeycloakIds: APP.idType === 'identity',
  showTimeSelection: APP.testMode,
  showResponsiblesOnDashbaord: TENANT.type !== 'S',
  showTimeSlotSelection: TENANT.type !== 'S',
  showReactQueryDevTools: APP.testMode,
  refreshEveryMs: APP.testMode ? 1000 : 1000,
};

function dumpVars() {
  const { keycloakId, keycloakIssuer, nextAuthUrl } = SERVER;
  console.info(`Tenant: ${TENANT.name}[${TENANT.type}] @ ${TENANT.apiBaseUrl}`);
  console.info('APP', JSON.stringify(APP, null, 2));
  console.info('FLAGS', JSON.stringify(FLAGS, null, 2));
  console.info(
    'SERVER',
    JSON.stringify(
      {
        keycloakId,
        keycloakIssuer,
        nextAuthUrl,
      },
      null,
      2,
    ),
  );
}

function validateEnvVars() {
  console.info(`
  >>> Validating ENV vars...`);
  dumpVars();

  if (!process.env['NEXT_PUBLIC_QRCODE_TYPE']) {
    console.warn('Environment Warning: NEXT_PUBLIC_QRCODE_TYPE is not set');
  }

  if (!TENANT.name) {
    console.warn('Environment Warning: NEXT_PUBLIC_TENANT_NAME is not set');
  }

  if (APP.platform !== 'web' && APP.platform) {
    throw new Error(
      'Environment Error: Q_PLATFORM_TARGET/NEXT_PUBLIC_Q_PLATFORM_TARGET must be either web or native',
    );
  }

  if (APP.qrType !== 'physical' && APP.qrType !== 'identity' && APP.qrType !== 'member') {
    throw new Error(
      'Environment Error: NEXT_PUBLIC_QRCODE_TYPE must be either physical (default), identity or member',
    );
  }

  if (TENANT.type !== 'S' && TENANT.type !== 'L') {
    throw new Error('Environment Error: NEXT_PUBLIC_TENANT_TYPE must be either S or L');
  }
  if (!TENANT.apiBaseUrl) {
    throw new Error('Environment Error: NEXT_PUBLIC_API_BASE_URL is not set');
  }

  if (typeof document === 'undefined') {
    if (!SERVER.nextAuthUrl) {
      throw new Error('Environment Error: NEXTAUTH_URL is not set');
    }
    if (!SERVER.keycloakId) {
      throw new Error('Environment Error: KEYCLOAK_ID is not set');
    }
    if (!SERVER.keycloakSecret) {
      throw new Error('Environment Error: KEYCLOAK_SECRET is not set');
    }
    if (!SERVER.keycloakIssuer) {
      throw new Error('Environment Error: KEYCLOAK_ISSUER is not set');
    }
    if (!SERVER.jwtSecret) {
      throw new Error('Environment Error: JWT_SECRET is not set');
    }
  }
}

/**
 * @returns {import('./types').RuntimeEnvOptions}
 */
function createPublicRuntimeConfig() {
  return {
    APP: {
      platform: APP.platform,
      testMode: APP.testMode,
      qrType: APP.qrType,
    },
    TENANT: {
      name: TENANT.name,
      type: TENANT.type,
      apiBaseUrl: TENANT.apiBaseUrl,
    },
  };
}

module.exports = {
  TENANT,
  APP,
  FLAGS,
  SERVER,
  createPublicRuntimeConfig,
  validateEnvVars,
};
