import { EnvRef, RuntimeEnvOptions } from './types';

const envRef: EnvRef = {
  TENANT: {
    name: '',
    type: 'S' as 'S' | 'L',
    apiBaseUrl: '',
  },
  APP: {
    testMode: false,
    platform: 'web',
    qrType: 'identity',
  },
  FLAGS: {
    useKeycloakIds: true,
    showTimeSelection: false,
    showResponsiblesOnDashbaord: false,
    showTimeSlotSelection: false,
    showReactQueryDevTools: false,
    refreshEveryMs: 1000,
  },
};

export function setupRuntimeEnv(env: RuntimeEnvOptions) {
  envRef.TENANT = env.TENANT;
  envRef.APP = env.APP;
  envRef.FLAGS = {
    // TODO rename next key or have 3 keys for identity/member/physical
    useKeycloakIds: env.APP.qrType === 'identity',
    showTimeSelection: env.APP.testMode,
    showResponsiblesOnDashbaord: env.TENANT.type !== 'S',
    showTimeSlotSelection: env.TENANT.type !== 'S',
    showReactQueryDevTools: env.APP.testMode,
    refreshEveryMs: env.APP.testMode ? 1000 : 1000,
  };
}

export function envFlags<TKey extends keyof typeof envRef.FLAGS>(
  key: TKey,
): (typeof envRef.FLAGS)[TKey] {
  return envRef.FLAGS[key];
}

export function envApp(key: keyof typeof envRef.APP) {
  return envRef.APP[key];
}

export function envTenant(key: keyof typeof envRef.TENANT) {
  return envRef.TENANT[key];
}

export function envDebug() {
  return envRef;
}
