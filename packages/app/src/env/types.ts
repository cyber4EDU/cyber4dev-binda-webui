export type EnvRef = {
  TENANT: {
    name: string;
    type: 'S' | 'L';
    apiBaseUrl: string;
  };
  APP: {
    testMode: false;
    platform: 'web' | 'native';
    qrType: 'identity' | 'member' | 'physical';
  };
  FLAGS: {
    useKeycloakIds: boolean;
    showTimeSelection: boolean;
    showResponsiblesOnDashbaord: boolean;
    showTimeSlotSelection: boolean;
    showReactQueryDevTools: boolean;
    refreshEveryMs: number;
  };
};

export type RuntimeEnvOptions = {
  TENANT: EnvRef['TENANT'];
  APP: EnvRef['APP'];
};
