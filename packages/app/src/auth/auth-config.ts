import { NextAuthOptions } from 'next-auth';
import { decode } from 'next-auth/jwt';
import KeyCloakProvider from 'next-auth/providers/keycloak';

export const KEYCLOAK_ID = process.env['KEYCLOAK_ID']!;
export const KEYCLOAK_SECRET = process.env['KEYCLOAK_SECRET']!;
export const KEYCLOAK_ISSUER = process.env['KEYCLOAK_ISSUER'];
export const JWT_SECRET = process.env['JWT_SECRET'];

export const authOptions: NextAuthOptions = {
  secret: JWT_SECRET,

  providers: [
    KeyCloakProvider({
      clientId: KEYCLOAK_ID,
      clientSecret: KEYCLOAK_SECRET,
      issuer: KEYCLOAK_ISSUER,
      profile(profile, tokens) {
        // log('authOptions.profile', { profile, tokens });
        const roles = getRoles(tokens.access_token);
        return {
          ...profile,
          id: profile.sub,
          roles,
          // token: tokens.access_token,
        };
      },
    }),
  ],
  callbacks: {
    jwt(jwtProps) {
      const { token, user, account } = jwtProps;
      // log('callbacks.jwt', jwtProps);

      if (account) {
        token.accessToken = account.access_token;
      }
      if (user) {
        // user is profile from before
        token.roles = user.roles;
        token.preferred_username = user.preferred_username;
        token.given_name = user.given_name;
        token.family_name = user.family_name;
      }
      return token; // stored in the cookie and passed to the session callback
    },
    session(sessionProps) {
      const { session, token } = sessionProps;
      // log('callbacks.session', sessionProps);
      if (token.accessToken) {
        session.accessToken = token.accessToken;
      }
      session.user.username = token.preferred_username || token.name;
      session.user.given_name = token.given_name;
      session.user.family_name = token.family_name;

      return session; // accessible in the client
    },
  },
};

async function getRoles(token: string | undefined) {
  if (!token) return [];
  try {
    const tokenValues = await decode({ token, secret: '' }); // no secret here!!! its from KeyCloak
    // const tokenValues = decodeJwt(token) as BindaJWTPayload;
    const roles = tokenValues?.realm_access?.roles ?? ['unknown'];
    return roles;
  } catch {
    return [];
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function log(key: string, value: any) {
  console.info(`\n>>>>>>>>>>>>>>>>>>>> ${key}`);
  console.info(JSON.stringify(value, null, 2));
  console.info(`<<<<<<<<<<<<<<<<<<<< ${key}\n`);
}

declare module 'next-auth/jwt' {
  interface JWT {
    preferred_username?: string;
    given_name?: string;
    family_name?: string;
    accessToken?: string | undefined;
    realm_access?: {
      roles?: string[] | undefined;
    };
  }
}
