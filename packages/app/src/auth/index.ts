import { Session } from 'next-auth';
import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';

import { setAuthToken } from './auth-state';

/**
 * This also sets the Authtoken/status for now, so put this high up into
 * "Root", even if it rerenders to often for now.
 * @returns the current usersession
 */
// TODO imre: refreshToken + connect this into service requests (403)
// or into listeners of signIn/out/...

type QSession = {
  user?: Session['user'];
  status: 'authenticated' | 'loading' | 'unauthenticated' | 'unknown';
  expires?: string;
  token?: string;
  data?: Session | null;
};
export function useQSession() {
  const { data, status } = useSession();
  const [qSession, setQSession] = useState({
    status: 'loading',
  } as QSession);
  useEffect(() => {
    if (status === 'authenticated' && data.accessToken && data.expires) {
      setAuthToken({
        status: 'authenticated',
        accessToken: data.accessToken,
        expiresAt: new Date(data.expires).getTime(),
      });
    } else {
      setAuthToken({
        status: status === 'authenticated' ? 'unknown' : status,
      });
    }
    setQSession({
      user: {
        ...data?.user,
      },
      status,
      expires: data?.expires,
      token: data?.accessToken,
      data,
    });
  }, [data?.accessToken, data?.expires, status]);
  return qSession;
}

export { useAuthStatus, useAuthToken } from './auth-state';
