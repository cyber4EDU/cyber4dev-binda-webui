import { DefaultSession } from 'next-auth';
declare module 'next-auth' {
  interface User {
    roles?: string[] | undefined;
    preferred_username?: string;
    given_name?: string;
    family_name?: string;
  }

  interface Session extends DefaultSession {
    accessToken?: string | undefined;
    user: DefaultSession['user'] & {
      roles?: string[] | undefined;
      username?: string | null;
      given_name?: string;
      family_name?: string;
    };
  }
}
