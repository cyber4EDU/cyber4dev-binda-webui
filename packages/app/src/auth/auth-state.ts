import { atom, getDefaultStore, ExtractAtomValue, useAtomValue } from 'jotai';

type AuthToken =
  | {
      status: 'authenticated';
      accessToken: string;
      refreshToken?: never; // TODO implement refreshToken, but leave it to never for now, so we don't use it yet.
      expiresAt: number;
    }
  | {
      status: 'loading' | 'unauthenticated' | 'unknown';
      accessToken?: never;
      refreshToken?: never;
      expiresAt?: never;
    };
export type AuthStatus = AuthToken['status'];

// TODO for now we use the default store as global store
// change this to session listeners | api-request listeners etc.
const store = getDefaultStore();

export const $AuthToken = atom({ status: 'unknown' } as AuthToken);
export const $AuthStatus = atom((get) => get($AuthToken).status);

export function subscribeToAuthToken(
  callback: (token: ExtractAtomValue<typeof $AuthToken>) => void,
) {
  return store.sub($AuthToken, () => {
    callback(store?.get($AuthToken));
  });
}

export function getCurrentAuthToken() {
  return store.get($AuthToken);
}

export function setAuthToken(token: ExtractAtomValue<typeof $AuthToken>) {
  store.set($AuthToken, token);
}

export function useAuthToken() {
  return useAtomValue($AuthToken);
}

export function useAuthStatus() {
  return useAtomValue($AuthStatus);
}
