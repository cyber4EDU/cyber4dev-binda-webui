import { envTenant, envFlags, envDebug } from '@q/app/env';
import { dateUtils } from '@q/app/utils';
import { formatToDateTimeInputValue, formatToQueryDate, msSinceNow } from '@q/app/utils/date-utils';
import { C, T } from '@q/ui/ds';
import { atom, useAtom, useAtomValue, useSetAtom } from 'jotai';
import { Platform } from 'react-native';

/** Atom for mostly static app configuration */
export const $AppConfig = atom({
  tenant: envTenant('name'),
});

// Allow to time travel
let localNowOffset = 0;

// The initial selected Dateatom
export const $SelectedDate = atom(new Date());

// The following lines create a number of Atoms, that "count" the time.
// So that requests / UI-Updates can be triggered through time.

// Offset of the real time to the desired time
const $$LocalNowOffset = atom(0);

// Slot for saving the current Date.now
const $$NowCache = atom(Date.now());

export function now() {
  return Date.now() + localNowOffset;
}

export function now4query() {
  return formatToQueryDate(now());
}
/**
 * Core of the "Now" hoopla.
 * The $Now atom will be updated about every second and its getter will
 * use the $$LocalNowOffset to allow for testing timetravel
 */
export const $Now = atom(
  (get) => {
    const actualNow = get($$NowCache);
    const nowOffset = get($$LocalNowOffset);
    return actualNow + nowOffset;
  },
  (_, set, _update: never) => {
    set($$NowCache, Date.now());
  },
);
$Now.onMount = (setSelf) => {
  const interval = setInterval(setSelf, 1000);
  return () => clearInterval(interval);
};

/**
 * Now atom, that updates every minute
 */
export const $NowMins = atom((get) => {
  const now = get($Now);
  const partialMins = now % 60000;
  return now - partialMins;
});

const REFRESH_EVERY = envFlags('refreshEveryMs');
/**
 * Now atom, that updates (at the moment every 2 sec).
 * Which is a good fit for polling (but @tanstack query can be used ot do that
 * by it's own, see there)
 * But if standard jotai request or atom reevaluation should be triggered
 * frequently, this is a good candidate
 */
export const $NowRefresh = atom((get) => {
  const now = get($Now);
  const partial2Secs = now % REFRESH_EVERY;
  return now - partial2Secs;
});

// hooks for the above atoms. see tere
export function useAppConfig() {
  return useAtomValue($AppConfig);
}

export function useSelectedDate() {
  return useAtomValue($SelectedDate);
}

export function useSetSelectedDate() {
  return useSetAtom($SelectedDate);
}

/**
 * Attention: This atom updates about once a second!
 *
 * Access to the (potentially timetravel translated) current Date.now() in seconds granularity
 * @returns the current or translated Date.now()
 */
export function useNow() {
  return useAtomValue($Now);
}

/**
 * @returns an updated Date.now value every minute
 */
export function useNowMins() {
  return useAtomValue($NowMins);
}

/**
 * @returns an updated Date.now value about every other second
 */
export function useNowRefresh() {
  return useAtomValue($NowRefresh);
}

// small component to change the TimeTravel value in the browser.
export function WebNowPicker({ enabled }: { enabled: boolean }) {
  const [offset, setAtomOffset] = useAtom($$LocalNowOffset);
  const setOffset = (offset: number) => {
    setAtomOffset(offset);
    localNowOffset = offset;
  };
  if (Platform.OS === 'web' && enabled) {
    return (
      <C>
        <label>
          Virt. Datum:{' '}
          <input
            type="datetime-local"
            value={formatToDateTimeInputValue(Date.now() + offset)}
            onChange={(e) => {
              setOffset(msSinceNow(e.target.value));
            }}
          />
        </label>
        <Now />
      </C>
    );
  }
  return null;
}

export function Now() {
  const now = useNow();
  return <T.ContactInfoLink>{dateUtils.formatDate(now, 'dateTimeWithSecs')}</T.ContactInfoLink>;
}
