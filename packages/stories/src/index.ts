// import path from 'path';
const stories = ['src/**/*.stories.?(ts|tsx|js|jsx)'];
const webStories = ['src/**/*.mdx', ...stories];

function getStories(type: 'web' | 'native') {
  const spath = type === 'web' ? webStories : stories;
  // return spath.map((storyPath) => path.resolve(__dirname, storyPath));
  return spath.map((storyPath) => `../../../packages/stories/${storyPath}`);
}

export const cfg = {
  stories: getStories,
};
