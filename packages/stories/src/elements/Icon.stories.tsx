import { C, Q } from '@q/ui/src';
import { Meta } from '@storybook/react';

const Story: Meta = {
  title: 'Elements/Icons',
  component: Q.Icon,
  decorators: [
    (Story) => (
      <C className="min-h-28 flex flex-1 items-center justify-center">
        <Story />
      </C>
    ),
  ],
  parameters: {
    // layout: 'flex',
  },

  tags: ['autodocs'],
};

export default Story;

export const DefaultIcon = {
  args: {},
};
