import { C, CR, Q } from '@q/ui';
import { ButtonProps } from '@q/ui/ds/elements/Button';
import { Meta } from '@storybook/react';

const Story: Meta = {
  title: 'Elements/Buttons',
  component: Q.SmallButton,
  decorators: [
    (Story) => (
      <C className="min-h-28 flex flex-1 items-center justify-center">
        <Story />
      </C>
    ),
  ],
  parameters: {
    // layout: 'flex',
  },
  args: {
    iconName: 'Edit',
    label: 'Button Text',
  },

  tags: ['autodocs'],
};

export default Story;

export const BaseButton = {
  render: (args: any) => <Q.Button {...args} />,
  args: {
    variant: 'small',
    intent: 'primary',
    shadowless: true,
    label: 'Button small primary without shadows',
  },
};

export const BigButton = {
  render: (args: any) => <Q.BigButton {...args} />,
  args: {
    size: 'big',
    label: 'Big Button Text',
    iconName: null,
  },
};

export const BigButtonWithIcon = {
  render: (args: any) => <Q.BigButton {...args} />,
  args: {
    size: 'big',
    label: 'Big Button Text',
  },
};

export const SmallButton = {
  args: {
    label: 'Small Button Text',
  },
};

export const TextButton = {
  render: (args: any) => <Q.TextButton {...args} />,
  args: {
    label: 'Text Button',
    iconName: 'Down',
  },
};

export const BigButtonWatchInMobileView = {
  render: (args: any) => <Q.BigButton {...args} />,
  args: {
    size: 'big',
    label: 'Big and very long Button Text',
    iconName: 'Apple',
  },
};

export const CheckinButton = {
  render: (args: any) => <Q.BigButton {...args} />,
  args: {
    label: 'Check-In',
    iconName: 'QrCode',
  },
};

export const ActionButtons = () => {
  return (
    <C className="fx ai-fs gap-4">
      <C className="fxr jc-c gap-4">
        <Q.SmallButton label="Abmelden" intent="warn" />
        <Q.SmallButton label="Bearbeiten" intent="warn" />
      </C>

      <C className="fxr jc-c gap-4">
        <Q.SmallButton label="Ist nicht da" intent="danger" />
        <Q.SmallButton label="Ist da" intent="safe" />
      </C>

      <C className="fxr gap-4">
        <Q.SmallButton label="Abbrechen" intent="danger" />
        <Q.SmallButton label="Speichern" intent="safe" />
      </C>
    </C>
  );
};

export const SelectButton = {
  render: (args: any) => <Q.SelectButton {...args} />,
};
export const AbsentCardButton = {
  render: (args: any) => (
    <CR className="min-w-1/2 gap-3">
      <Q.AbsentCardButton {...args} />
      <Q.AbsentCardButton label="Ist irgendwo" intent="warn" count={9} />
    </CR>
  ),
  args: {
    label: 'Ist nicht da',
    count: 15,
  },
};

export const CourseCardButton = {
  render: (args: any) => (
    <C className="min-w-1/2 gap-3">
      <Q.CourseCardButton {...args} />
      <Q.CourseCardButton {...args} label="Das ist ein Kurs" responsible={false} />
    </C>
  ),
  args: {
    label: 'Das ist ein Kurs (verantwortlich)',
    subLabel: 'Das ist der Kurs Untertitel',
    responsible: true,
    count: 12,
  },
};

export const IconButton = {
  render: (args: any) => <Q.IconButton {...args} />,
  args: {
    size: 'small',
    iconName: 'Filter2',
  },
};

export const IconButtons = ({
  iconName = 'Filter2',
  label = 'Sortieren/Filtern',
  ...props
}: ButtonProps) => {
  return (
    <C className="bg-prim-300 flex flex-row gap-4 p-11">
      <Q.IconButton iconName={iconName} label={label} {...props} />
      <Q.IconButton iconName="Call" intent="info" label="Kontaktinfos" />
      <Q.IconButton iconName="Alert" intent="info" label="Vorsicht" />
    </C>
  );
};

export const RoundButton = {
  render: (args: any) => <Q.RoundButton {...args} />,
  args: {
    label: null,
    iconName: 'QrCode',
  },
};
