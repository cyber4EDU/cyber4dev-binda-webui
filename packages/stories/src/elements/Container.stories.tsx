import { C, Q, T } from '@q/ui/src';
import { Meta } from '@storybook/react';

const Story: Meta = {
  title: 'Elements/Container',
  component: Q.Container,
  decorators: [
    (Story) => (
      <C className="min-h-60 container">
        <Story />
      </C>
    ),
  ],
  parameters: {
    // layout: 'flex',
  },
  args: {
    children: (
      <T.NoteBody>
        Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit
        einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer
        Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz!
        Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit
        einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer Notiz! Mit einer
        Notiz!
      </T.NoteBody>
    ),
  },

  argTypes: {
    children: {
      control: {
        type: null,
      },
    },
  },
  tags: ['autodocs'],
};

export default Story;

export const Basic = {};

export const BasicBlue = {
  args: {
    bg: 'blue',
  },
};
