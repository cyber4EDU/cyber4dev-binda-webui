import { Q } from '@q/ui/src';
import { Meta } from '@storybook/react';

const Story: Meta = {
  title: 'Elements/Logos',
  component: Q.Logo,
  parameters: {
    // layout: 'flex',
  },

  tags: ['autodocs'],
};

export default Story;

export const BinDa_Logo_Horizontal = {
  args: {},
};

export const BinDa_Logo_Signet = {
  render: () => <Q.LogoSignet />,
};
