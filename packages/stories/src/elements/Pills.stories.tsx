import { CR, CC, Q } from '@q/ui';
import { Meta, StoryObj } from '@storybook/react';

const Story: Meta = {
  title: 'Elements/Pills',
  component: Q.Pill,
  parameters: {
    // layout: 'flex',
  },

  tags: ['autodocs'],
};

type PillStory = StoryObj<typeof Q.Pill>;

export default Story;

export const Default: PillStory = {
  args: {
    count: 12,
  },
};

export const SmallPill: StoryObj<typeof Q.PillSmall> = {
  render: (args) => <Q.PillSmall {...args} />,
  args: {
    label: 'Small Pill',
    type: 'filter',
    checkedIcon: 'Bandage',
  },
};

export const SmallPills: StoryObj<typeof Q.PillSmall> = {
  render: (args) => (
    <CC className="gap-4">
      <CR className="jc-sb min-w-1/2">
        <Q.PillSmall label="Filter" type="filter" checkedIcon="CheckmarkCircle" initialChecked />
        <Q.PillSmall label="bis 14:30" type="excused" rightIcon="Out" initialChecked />
        <Q.PillSmall label="LB NaWi" type="checkedin" initialChecked />
        <Q.PillSmall label="Nicht DA" type="missing" initialChecked />
      </CR>
      <CR className="jc-sb min-w-1/2">
        <Q.PillSmall label="Filter" type="filter" checkedIcon="CheckmarkCircle" />
        <Q.PillSmall label="bis 14:30" type="excused" rightIcon="Out" />
        <Q.PillSmall label="LB NaWi" type="checkedin" />
        <Q.PillSmall label="Nicht DA" type="missing" rightIcon="Alert" checked={false} />
      </CR>
    </CC>
  ),
};
