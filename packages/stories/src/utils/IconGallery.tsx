import { IconGallery as SBIconGallery, IconItem } from '@storybook/blocks';
import { ComponentType } from 'react';

export function QIconGallery({ icons }: { icons: Record<string, ComponentType<any>> }) {
  return (
    <SBIconGallery>
      {Object.entries(icons).map(([name, Icon]) => {
        return (
          <IconItem key={name} name={name}>
            <Icon />
          </IconItem>
        );
      })}
    </SBIconGallery>
  );
}
