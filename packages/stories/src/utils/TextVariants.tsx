import { styled } from 'nativewind';
import { Text } from 'react-native';

const VText = styled(Text);

export function TextVariants({ variants }: { variants: Record<string, string> }) {
  return (
    <table>
      {Object.entries(variants).map(([key, value]) => (
        <tr key={key}>
          <th style={{ textAlign: 'left' }}>{key}</th>
          <td>
            <VText className={value}>{value}</VText>
          </td>
        </tr>
      ))}
    </table>
  );
}
