import { DashboardScreen } from '@q/app/features/dashboard';
import { Meta } from '@storybook/react';

export default {
  title: 'Screens/Dashboard Screen',
  component: DashboardScreen,
  parameters: {
    layout: 'flex',
  },
  // tags: ['autodocs'],
} satisfies Meta;

export const Default = {
  args: {
    tenant: 'ESBZ',
  },
};
