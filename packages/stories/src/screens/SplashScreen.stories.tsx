import { SplashScreen } from '@q/app/features/shared';
import { Meta } from '@storybook/react';

export default {
  title: 'Screens/Splash Screen',
  component: SplashScreen,
  parameters: {
    layout: 'flex',
  },
  // tags: ['autodocs'],
} satisfies Meta;

export const Default = {
  args: {},
};

export const Loaded = {
  args: {
    tenant: 'ESBZ',
    state: 'loaded',
  },
};

export const Loading = {
  args: {
    tenant: 'ESBZ',
    state: 'loading',
  },
};

export const InErrorBoundary = {
  args: {
    tenant: 'ESBZ',
    state: 'error',
    error: new Error(
      'Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or Something went wrong with a really long message or ',
    ),
  },
};
export const ErrorWithoutMessage = {
  args: {
    tenant: 'ESBZ',
    state: 'error',
  },
};
