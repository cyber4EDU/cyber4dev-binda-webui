import { Q } from '@q/ui';
import { Meta } from '@storybook/react';

export default {
  title: 'Screens/MembersList Screen',
  component: Q.Dummy,
  parameters: {
    layout: 'flex',
  },
  // tags: ['autodocs'],
} satisfies Meta;

export const Default = {
  args: {},
};
