import { HeaderBar } from '@q/ui';
import { Meta } from '@storybook/react';

const Story: Meta = {
  title: 'Components/TopBar',
  component: HeaderBar,
  parameters: {
    layout: 'flex',
  },
  tags: ['autodocs'],
};

export default Story;

export const Anonym = {
  args: {
    name: '',
  },
};

export const ESBZ = {
  args: {
    name: 'ESBZ',
  },
};
