const path = require('path');
const stories = ['**/*.stories.?(ts|tsx|js|jsx)'];
const webStories = ['**/*.mdx', ...stories];

function getStories(type) {
  const spath = type === 'web' ? webStories : stories;
  return spath.map((storyPath) => path.resolve(__dirname, storyPath));
}

module.exports = {
  cfg: {
    stories: getStories,
  },
};
