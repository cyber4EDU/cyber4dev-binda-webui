import { QRScanner } from '@q/app/features/qr-scanner/lib';
import { Meta } from '@storybook/react';

const Story: Meta = {
  title: 'Modules/QR Scanner',
  component: QRScanner,
  tags: ['autodocs'],
};

export default Story;

export const Scanning = {
  args: {
    scan: true,
  },
};
