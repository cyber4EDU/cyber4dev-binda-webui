const { createNativewindConfig } = require('@q/ui/tw');

const twConfig = createNativewindConfig({
  relContentDirs: [
    //
    './src',
    '../../packages/ui/src',
  ],
  fixRNWOrder: true,
  darkMode: 'class',
});

console.info('<<<<<<<<<< TWConfig: @q/stories >>>>>>>>>>');

module.exports = twConfig;
