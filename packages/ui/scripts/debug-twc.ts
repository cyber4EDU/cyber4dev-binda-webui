import { createTailwindConfig } from '../src/tw';

const twConfig = createTailwindConfig({
  relContentDirs: ['./src'],
  fixRNWOrder: false,
});

console.info('>>>>>>>>>>>>>>>>>>>>. TW Config', twConfig);
