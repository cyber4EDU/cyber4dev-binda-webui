# UI Basic Components and Tokens

## Important notes

- Don't use automatic transformation from `nativewind` (and don't add the type enhancements)
- Always use the full `styled`-syntax  
  because nativewind (contrary to `quant`) does not work with a variantdefinition that includes `className`.  
  So we removed that option from `quant: styled`

```ts
const Component1 = styled(AnotherComponent)
const Component2 = styled(AnotherComponent, 'base tailwind classes')
const Component3 = styled(AnotherComponent, 'base tailwind classes', {
    ...cva variants
})
const Component4 = styled(AnotherComponent, undefined, {
    ...cva variants
})
```

because
