// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Defs, Pattern, Path, G, Rect } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgRaster(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} ref={ref} {...props}>
      <Defs>
        <Pattern id="raster_svg__a" width={72} height={72} patternUnits="userSpaceOnUse">
          <Path d="M0 0h72v72H0z" fill={color} />
          <G stroke="#fff" strokeDasharray="2 2" strokeOpacity={0.15}>
            <Path d="M10 0v72M46 0v72M0 46h72M0 10h72" />
          </G>
        </Pattern>
      </Defs>
      <Rect width="100%" height="100%" fill="url(#raster_svg__a)" />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgRaster);
export default ForwardRef;
