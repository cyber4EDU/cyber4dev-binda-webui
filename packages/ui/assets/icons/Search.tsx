// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgSearch(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M10.364 2.25a8.114 8.114 0 1 0 5.182 14.357l4.924 4.923a.75.75 0 1 0 1.06-1.06l-4.924-4.924a8.116 8.116 0 0 0 1.871-5.182 8.114 8.114 0 0 0-8.113-8.114ZM6.689 4.865a6.614 6.614 0 0 1 3.675-1.115l6.613 6.614a6.614 6.614 0 1 1-10.288-5.5Zm8.351.822a6.614 6.614 0 0 0-4.676-1.937l6.613 6.614a6.614 6.614 0 0 0-1.937-4.677Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgSearch);
export default ForwardRef;
