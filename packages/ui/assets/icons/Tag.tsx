// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgTag(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M7.603 21.89a2.106 2.106 0 0 0 2.967-.005L22.083 10.37c.267-.272.412-.628.412-1.008V3.6a2.07 2.07 0 0 0-.604-1.481 2.103 2.103 0 0 0-1.477-.619H14.64c-.38 0-.736.15-1.008.417L2.12 13.44a2.105 2.105 0 0 0 0 2.967l5.484 5.485Zm-4.425-7.396L14.672 3H20.409c.329 0 .591.267.591.6v5.738L9.511 20.827a.609.609 0 0 1-.848 0l-5.485-5.485a.608.608 0 0 1 0-.848ZM16.5 6c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5-1.5.67-1.5 1.5Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgTag);
export default ForwardRef;
