// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgEdit(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M21.316 2.318a.754.754 0 0 1 .244.178.762.762 0 0 1-.04 1.05l-.566.562a.374.374 0 0 1-.531 0l-.532-.53a.375.375 0 0 1 0-.531l.58-.577a.752.752 0 0 1 .845-.152ZM10.257 12.666l8.462-8.447a.422.422 0 0 1 .596 0l.466.469a.422.422 0 0 1 0 .593l-8.447 8.463a.421.421 0 0 1-.184.108l-1.165.391a.184.184 0 0 1-.23-.13.183.183 0 0 1 .002-.097l.392-1.166a.422.422 0 0 1 .108-.184ZM4.08 7.08c.21-.211.497-.33.795-.33h7.85a.75.75 0 0 0 0-1.5h-7.85A2.625 2.625 0 0 0 2.25 7.875v11.25a2.625 2.625 0 0 0 2.625 2.625h11.25a2.625 2.625 0 0 0 2.625-2.625V10.5a.75.75 0 0 0-1.5 0v8.625a1.126 1.126 0 0 1-1.125 1.125H4.875a1.125 1.125 0 0 1-1.125-1.125V7.875c0-.298.119-.585.33-.795Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgEdit);
export default ForwardRef;
