// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgSchool(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M12.372 2.349a.75.75 0 0 0-.744 0l-10.5 6a.75.75 0 0 0 0 1.302L4.5 11.578v5.672a.75.75 0 0 0 .386.656l6.75 3.75a.75.75 0 0 0 .728 0l6.75-3.75a.75.75 0 0 0 .386-.656v-5.672l2.25-1.286v6.958a.75.75 0 0 0 1.5 0V9a.747.747 0 0 0-.39-.658L12.372 2.349ZM18 12.435l-5.25 3v4.29L18 16.81v-4.374Zm-6.75 3-5.25-3v4.374l5.25 2.916v-4.29Zm.75-1.299L3.012 9 12 3.864 20.988 9 12 14.136Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgSchool);
export default ForwardRef;
