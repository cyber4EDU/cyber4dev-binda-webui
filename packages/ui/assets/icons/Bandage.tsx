// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { G, Path, Defs, ClipPath } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgBandage(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <G clipPath="url(#bandage_svg__a)">
        <Path
          fill={color}
          fillRule="evenodd"
          d="M3.235 20.765a3.38 3.38 0 0 1 0-4.781L5.99 13.23c.107.227.255.439.442.626l3.712 3.713c.188.187.4.335.627.442l-2.753 2.754a3.38 3.38 0 0 1-4.781 0Zm14.908-10.127 2.622-2.622a3.38 3.38 0 0 0 0-4.78v-.001a3.38 3.38 0 0 0-4.78 0l-2.623 2.622c.227.107.439.255.626.442l3.713 3.712c.187.188.335.4.442.627ZM2.175 14.923a4.88 4.88 0 1 0 6.903 6.903l12.748-12.75a4.88 4.88 0 0 0-6.903-6.903L2.175 14.924Zm9.692-7.563a.75.75 0 0 1 1.06 0l3.713 3.712a.75.75 0 0 1 0 1.06l-4.375 4.376a.75.75 0 0 1-1.06 0l-3.713-3.712a.75.75 0 0 1 0-1.061l4.375-4.375ZM12 10.5A.75.75 0 1 0 12 9a.75.75 0 0 0 0 1.5Zm3 1.5a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Zm-5.25.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Zm3 1.5a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z"
          clipRule="evenodd"
        />
      </G>
      <Defs>
        <ClipPath id="bandage_svg__a">
          <Path fill="#fff" d="M0 0h24v24H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgBandage);
export default ForwardRef;
