// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgHome(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="m15.75 4.5-2.71-2.595c-.304-.308-.72-.404-1.037-.404-.319 0-.736.094-1.042.404l-9.98 9.553a.75.75 0 0 0 1.038 1.084l.981-.94V21a1.5 1.5 0 0 0 1.5 1.5H9a.75.75 0 0 0 .75-.75v-6.375a.375.375 0 0 1 .375-.375h3.75a.375.375 0 0 1 .375.375v6.375c0 .414.336.75.75.75h4.5A1.5 1.5 0 0 0 21 21v-9.398l.981.94a.75.75 0 0 0 1.038-1.084L19.5 8.09V3a.75.75 0 0 0-.75-.75H16.5a.75.75 0 0 0-.75.75v1.5Zm3.75 5.666-7.485-7.165a.172.172 0 0 0-.03 0L4.5 10.166V21h3.75v-5.625a1.875 1.875 0 0 1 1.875-1.875h3.75a1.875 1.875 0 0 1 1.875 1.875V21h3.75V10.166ZM18 3.75v2.904l-.75-.718V3.75H18Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgHome);
export default ForwardRef;
