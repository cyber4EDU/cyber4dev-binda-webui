// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgChange(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M11.976 2.86A9.14 9.14 0 0 0 4.82 6.3a.75.75 0 1 0 1.172.935 7.64 7.64 0 0 1 5.982-2.876h.001c4.226 0 7.65 3.419 7.65 7.631v.258l-.825-.79a.75.75 0 0 0-1.037 1.084l2.156 2.063a.75.75 0 0 0 1.049-.012l2.062-2.063a.75.75 0 1 0-1.06-1.06l-.845.844v-.324c0-5.043-4.098-9.13-9.15-9.13ZM2.85 11.71l-.82.82a.75.75 0 0 1-1.06-1.06l2.062-2.063a.75.75 0 0 1 1.049-.011l2.156 2.062A.75.75 0 0 1 5.2 12.542l-.85-.813v.279c0 4.217 3.422 7.633 7.65 7.633a7.709 7.709 0 0 0 5.98-2.86.75.75 0 0 1 1.165.945 9.21 9.21 0 0 1-7.144 3.415H12c-5.053 0-9.15-4.085-9.15-9.133v-.297Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgChange);
export default ForwardRef;
