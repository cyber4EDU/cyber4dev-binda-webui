// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgForwardCircle(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M11.782 7.718a.75.75 0 0 1 1.06.004l3.722 3.75a.75.75 0 0 1 0 1.056l-3.721 3.75a.75.75 0 0 1-1.065-1.056l2.452-2.472H7.97a.75.75 0 0 1 0-1.5h6.261l-2.452-2.472a.75.75 0 0 1 .004-1.06Z"
        clipRule="evenodd"
      />
      <Path
        fill={color}
        fillRule="evenodd"
        d="M2.25 12c0-5.383 4.367-9.75 9.75-9.75s9.75 4.367 9.75 9.75-4.367 9.75-9.75 9.75S2.25 17.383 2.25 12ZM12 3.75A8.252 8.252 0 0 0 3.75 12 8.252 8.252 0 0 0 12 20.25 8.252 8.252 0 0 0 20.25 12 8.252 8.252 0 0 0 12 3.75Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgForwardCircle);
export default ForwardRef;
