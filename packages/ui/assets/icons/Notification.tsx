// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgNotification(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M12 2.25c-5.383 0-9.75 4.367-9.75 9.75s4.367 9.75 9.75 9.75 9.75-4.367 9.75-9.75S17.383 2.25 12 2.25ZM3.75 12A8.252 8.252 0 0 1 12 3.75 8.252 8.252 0 0 1 20.25 12 8.252 8.252 0 0 1 12 20.25 8.252 8.252 0 0 1 3.75 12Zm13.171 2.442.198.23c.316.375.026 1.078-.525 1.079H7.406c-.555 0-.842-.704-.525-1.079l.198-.23c.66-.76 1.109-1.279 1.109-3.557 0-2.29 1.207-3.104 2.203-3.5a.532.532 0 0 0 .296-.313C10.861 6.497 11.35 6 12 6c.65 0 1.139.497 1.313 1.071a.535.535 0 0 0 .297.313c.994.396 2.203 1.21 2.203 3.5 0 2.28.449 2.797 1.108 3.558Zm-6.674 2.074a.19.19 0 0 1 .077-.016h3.348a.187.187 0 0 1 .187.21C13.741 17.571 12.97 18 12 18c-.98 0-1.758-.415-1.864-1.293a.186.186 0 0 1 .111-.191Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgNotification);
export default ForwardRef;
