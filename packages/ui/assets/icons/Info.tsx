// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgInfo(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M11.625 2.25a9.376 9.376 0 1 0 .001 18.751 9.376 9.376 0 0 0-.001-18.751ZM3.75 11.625a7.876 7.876 0 1 1 15.751.001 7.876 7.876 0 0 1-15.751-.001Zm7.875-5.531a1.219 1.219 0 1 0 0 2.437 1.219 1.219 0 0 0 0-2.437Zm-1.313 3.468a.75.75 0 1 0 0 1.5h.75v4.126H9.75a.75.75 0 0 0 0 1.5h4.125a.75.75 0 0 0 0-1.5h-1.313v-4.876a.75.75 0 0 0-.75-.75h-1.5Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgInfo);
export default ForwardRef;
