// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgList(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M3.75 5.25a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3ZM7.5 6a.75.75 0 0 0 0 1.5H21A.75.75 0 0 0 21 6H7.5Zm0 5.25a.75.75 0 0 0 0 1.5H21a.75.75 0 0 0 0-1.5H7.5Zm0 5.25a.75.75 0 0 0 0 1.5H21a.75.75 0 0 0 0-1.5H7.5ZM2.25 12a1.5 1.5 0 1 1 3 0 1.5 1.5 0 0 1-3 0Zm1.5 3.75a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgList);
export default ForwardRef;
