// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgFilter2(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M2.278 17.56a.75.75 0 1 0-1.056 1.064l2.75 3.72a.75.75 0 0 0 1.056 0l2.75-3.72a.75.75 0 0 0-1.056-1.065L5.25 19.012V2.75a.75.75 0 0 0-1.5 0v16.262l-1.472-1.453ZM9.645 6C9.289 6 9 6.336 9 6.75s.289.75.645.75h12.96c.356 0 .645-.336.645-.75S22.961 6 22.605 6H9.645Zm0 4c-.356 0-.645.336-.645.75s.289.75.645.75h10.96c.356 0 .645-.336.645-.75s-.289-.75-.645-.75H9.645Zm0 4.25c-.356 0-.645.336-.645.75s.289.75.645.75h7.737c.356 0 .644-.336.644-.75s-.288-.75-.644-.75H9.645Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgFilter2);
export default ForwardRef;
