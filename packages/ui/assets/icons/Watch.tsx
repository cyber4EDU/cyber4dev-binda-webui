// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgWatch(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        d="M19.5 12a7.494 7.494 0 0 0-3.066-6.044l-.584-3.224A1.499 1.499 0 0 0 14.374 1.5H9.626A1.5 1.5 0 0 0 8.15 2.732l-.584 3.224a7.49 7.49 0 0 0 0 12.088l.584 3.224A1.5 1.5 0 0 0 9.626 22.5h4.748a1.499 1.499 0 0 0 1.476-1.232l.584-3.224A7.494 7.494 0 0 0 19.5 12ZM9.626 3h4.748l.366 2.02a7.471 7.471 0 0 0-5.48 0L9.626 3ZM6 12a6 6 0 1 1 6 6 6.007 6.007 0 0 1-6-6Zm8.374 9H9.626l-.366-2.02c1.76.694 3.72.694 5.48 0L14.374 21Zm-3.124-9V8.25a.75.75 0 1 1 1.5 0v3h3a.75.75 0 1 1 0 1.5H12a.75.75 0 0 1-.75-.75Z"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgWatch);
export default ForwardRef;
