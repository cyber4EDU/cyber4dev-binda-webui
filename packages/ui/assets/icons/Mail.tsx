// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgMail(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M3 6.375c0-.621.504-1.125 1.125-1.125h15.75c.621 0 1.125.504 1.125 1.125v11.25c0 .621-.504 1.125-1.125 1.125H4.125A1.125 1.125 0 0 1 3 17.625V6.375ZM4.125 3.75A2.625 2.625 0 0 0 1.5 6.375v11.25a2.625 2.625 0 0 0 2.625 2.625h15.75a2.625 2.625 0 0 0 2.625-2.625V6.375a2.625 2.625 0 0 0-2.625-2.625H4.125ZM5.71 6.908a.75.75 0 0 0-.92 1.184l6.75 5.25c.27.21.65.21.92 0l6.75-5.25a.75.75 0 0 0-.92-1.184L12 11.8 5.71 6.908Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgMail);
export default ForwardRef;
