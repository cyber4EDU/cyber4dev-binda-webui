// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgBackCircle(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M12 2.25c-5.383 0-9.75 4.367-9.75 9.75s4.367 9.75 9.75 9.75 9.75-4.367 9.75-9.75S17.383 2.25 12 2.25ZM3.75 12A8.252 8.252 0 0 1 12 3.75 8.252 8.252 0 0 1 20.25 12 8.252 8.252 0 0 1 12 20.25 8.252 8.252 0 0 1 3.75 12Zm8.472-3.222a.75.75 0 0 0-1.065-1.056l-3.72 3.75a.75.75 0 0 0 0 1.056l3.72 3.75a.75.75 0 0 0 1.065-1.056L9.77 12.75h6.261a.75.75 0 1 0 0-1.5H9.77l2.452-2.472Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgBackCircle);
export default ForwardRef;
