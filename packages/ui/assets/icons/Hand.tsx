// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgHand(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M12.75 0a2.25 2.25 0 0 0-2.25 2.25v.129a2.25 2.25 0 0 0-3 2.121v6.736l-.268-.645a.748.748 0 0 0-.029-.062c-.616-1.171-2.065-1.444-3.11-.85h-.001a2.086 2.086 0 0 0-1.025 1.332c-.133.537-.057 1.102.135 1.623l2.469 6.749.012.032c.508 1.21 1.236 2.378 2.443 3.235 1.21.859 2.822 1.35 5 1.35 2.233 0 4.229-.748 5.66-2.303C20.212 20.148 21 17.886 21 15.003V6.75a2.25 2.25 0 0 0-3-2.121V3.75a2.25 2.25 0 0 0-3.08-2.092A2.25 2.25 0 0 0 12.75 0Zm6.75 15c0 2.619-.712 4.48-1.817 5.68-1.1 1.195-2.667 1.82-4.558 1.82-1.948 0-3.235-.437-4.131-1.073-.895-.636-1.478-1.525-1.92-2.576L4.61 12.117l-.001-.002c-.12-.325-.129-.575-.087-.745a.59.59 0 0 1 .31-.387c.399-.226.85-.087 1.03.221l1.688 4.068a.753.753 0 0 0 .846.464.75.75 0 0 0 .603-.753V4.5a.75.75 0 0 1 1.5 0v6.797a.75.75 0 0 0 1.5 0V2.25a.75.75 0 1 1 1.5 0v9a.75.75 0 0 0 1.5 0v-7.5a.75.75 0 1 1 1.5 0V12a.75.75 0 0 0 1.5 0V6.75a.75.75 0 1 1 1.5 0v8.248"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgHand);
export default ForwardRef;
