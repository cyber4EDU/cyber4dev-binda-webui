// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgTime(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M12 2.25c-5.383 0-9.75 4.367-9.75 9.75s4.367 9.75 9.75 9.75 9.75-4.367 9.75-9.75S17.383 2.25 12 2.25ZM3.75 12A8.252 8.252 0 0 1 12 3.75 8.252 8.252 0 0 1 20.25 12 8.252 8.252 0 0 1 12 20.25 8.252 8.252 0 0 1 3.75 12Zm9-6a.75.75 0 0 0-1.5 0v6.75c0 .414.336.75.75.75h4.5a.75.75 0 0 0 0-1.5h-3.75V6Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgTime);
export default ForwardRef;
