// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgOut(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M4.875 5.25A1.125 1.125 0 0 0 3.75 6.375v11.25a1.125 1.125 0 0 0 1.125 1.125h7.5a1.125 1.125 0 0 0 1.125-1.125V15.75a.75.75 0 0 1 1.5 0v1.875a2.625 2.625 0 0 1-2.625 2.625h-7.5a2.625 2.625 0 0 1-2.625-2.625V6.375A2.625 2.625 0 0 1 4.875 3.75H12c.704 0 1.426.278 1.976.709C14.522 4.887 15 5.552 15 6.375V8.25a.75.75 0 0 1-1.5 0V6.375c0-.213-.13-.485-.45-.735-.316-.249-.719-.39-1.05-.39H4.875ZM16.72 7.72a.75.75 0 0 1 1.06 0l3.75 3.75a.75.75 0 0 1 0 1.06l-3.75 3.75a.75.75 0 1 1-1.06-1.06l2.47-2.47H8.25a.75.75 0 0 1 0-1.5h10.94l-2.47-2.47a.75.75 0 0 1 0-1.06Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgOut);
export default ForwardRef;
