// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
import * as React from 'react';
import { Ref, forwardRef } from 'react';
import Svg, { Path } from 'react-native-svg';
import type { SvgProps } from 'react-native-svg';
function SvgUser(props: SvgProps, ref: Ref<Svg>) {
  const { color = '#000' } = props;
  return (
    <Svg width={24} height={24} fill="none" viewBox="0 0 24 24" ref={ref} {...props}>
      <Path
        fill={color}
        fillRule="evenodd"
        d="M2.25 12.136c-.074-5.484 4.402-9.96 9.886-9.886 5.259.073 9.54 4.355 9.613 9.615.075 5.483-4.4 9.96-9.885 9.885-5.259-.073-9.54-4.355-9.614-9.614Zm15.745 5.5a.187.187 0 0 0 .067-.046A8.218 8.218 0 0 0 20.25 12c0-4.556-3.707-8.261-8.27-8.25-4.564.011-8.306 3.83-8.23 8.39a8.218 8.218 0 0 0 2.188 5.45.188.188 0 0 0 .288-.016 5.825 5.825 0 0 1 1.516-1.386C8.936 15.422 10.448 15 12 15s3.064.422 4.258 1.188a5.826 5.826 0 0 1 1.516 1.387.187.187 0 0 0 .22.061Zm-8.35-9.91c.595-.63 1.43-.976 2.355-.976.918 0 1.753.349 2.349.982.604.64.898 1.503.829 2.429C15.038 12 13.616 13.5 12 13.5c-1.616 0-3.042-1.5-3.178-3.34-.067-.934.23-1.805.824-2.434Z"
        clipRule="evenodd"
      />
    </Svg>
  );
}
const ForwardRef = forwardRef(SvgUser);
export default ForwardRef;
