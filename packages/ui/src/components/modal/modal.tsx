import { C } from '@q/ui/ds/base';
import { ReactNode, useCallback } from 'react';
import { ModalBaseProps, PointerEvent, Pressable, Modal as RNModal, ViewProps } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { C1, CR, Q, T } from '../../ds';
import { IconButton } from '../../ds/elements';

export interface ModalProps extends Exclude<ModalBaseProps, 'onRequestClose'>, ViewProps {
  onRequestClose: () => void;
  closeOnBackgroundPointerUp?: boolean;
  title?: string;
  children: ReactNode;
}
export function Modal({
  title,
  children,
  onRequestClose,
  closeOnBackgroundPointerUp,
  ...rnModalProps
}: ModalProps) {
  const handleBGTouch = useCallback(() => {
    if (closeOnBackgroundPointerUp) {
      onRequestClose?.();
    }
  }, [onRequestClose, closeOnBackgroundPointerUp]);

  return (
    <RNModal {...rnModalProps} onRequestClose={onRequestClose}>
      <Q.StyledPressable className="bg-prim/50 fx-1 fxcc cursor-default" onPress={handleBGTouch}>
        <C className="fx-1 fxcc p-2">
          <Q.StyledPressable className="cursor-default gap-2" onPress={() => {}}>
            {title && (
              <C className="fxcc flex-row rounded bg-white py-3 pl-14 pr-5">
                <C className="fx-1 fxcc">
                  <T.PupilSubtitle className="">{title}</T.PupilSubtitle>
                </C>
                <IconButton
                  label="Schliessen"
                  bare
                  iconName="CloseCircleFull"
                  onPress={onRequestClose}
                />
              </C>
            )}
            {children}
          </Q.StyledPressable>
        </C>
      </Q.StyledPressable>
    </RNModal>
  );
}

Modal.defaultProps = {
  animationType: 'fade',
  transparent: true,
};

const noop = () => {};
