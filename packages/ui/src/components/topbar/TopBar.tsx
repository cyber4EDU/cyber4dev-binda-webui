import { ReactNode } from 'react';
import { Platform } from 'react-native';

import { C, CH, CR, Q, T } from '../../ds';

interface HeaderBarProps {
  tenant?: string;
  onBack?: () => void;
  onProfilePress?: () => void;
  children: ReactNode;
}

export function HeaderBar({ children, tenant, onProfilePress, onBack }: HeaderBarProps) {
  const mayHideLogo = !!onBack;
  const webCn = Platform.OS === 'web' ? 'fixed z-50 w-full' : '';
  return (
    <C className={`fxcc border-sec shdw-md border-b-2 bg-rose-300 py-2 ${webCn}`}>
      <C className="fxr jc-sb ai-c max-w-bplg w-full px-4">
        <CH hide={mayHideLogo && '<md'}>
          <LogoWithName name={tenant} />
        </CH>
        {mayHideLogo && (
          <CH hide=">md">
            <Q.IconButton iconName="BackCircle" label="Zurück" bare onPress={onBack} />
          </CH>
        )}
        <CR className="gap-8">{children}</CR>
        <Q.IconButton label="Profil" iconName="User" intent="info" bare onPress={onProfilePress} />
      </C>
    </C>
  );
}

export function LogoWithName({ name }: { name?: string }) {
  return (
    <CR className="ai-fe">
      <Q.Logo />
      {name && (
        <>
          <T.LogoText className="ml-1">x </T.LogoText>
          <T.LogoTextColored>{name}</T.LogoTextColored>
        </>
      )}
    </CR>
  );
}
