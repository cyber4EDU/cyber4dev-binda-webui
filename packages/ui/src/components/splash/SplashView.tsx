import { AnimatePresence, MotiView } from 'moti';
import { ReactNode } from 'react';
import { Easing } from 'react-native-reanimated';

import { C, CC, Q, T } from '../../ds';

function SplashLogo({ variant = 'normal' }: { variant?: 'rotated' | 'animated' | 'normal' }) {
  return (
    <AnimatePresence exitBeforeEnter>
      {variant === 'animated' && (
        <MotiView
          key="animLogo"
          from={{ opacity: 0 }}
          exit={{ opacity: 1 }}
          animate={{
            opacity: [0.5, 0.2],
          }}
          transition={{
            type: 'timing',
            loop: true,
            // repeatReverse: false,
            easing: Easing.linear,
            duration: 1000,
          }}
        >
          <Q.LogoSignet />
        </MotiView>
      )}
      {variant !== 'animated' && (
        <C key="staticLogo" className={variant === 'rotated' ? 'rotate-45' : undefined}>
          <Q.LogoSignet />
        </C>
      )}
    </AnimatePresence>
  );
}

export function SplashView({
  tenant,
  logo,
  children,
  // className,
  ...props
}: {
  logo?: 'rotated' | 'animated' | 'normal';
  tenant?: string;
  // className?: string;
  children?: ReactNode;
}) {
  return (
    <CC className="gap-3" {...props}>
      <SplashLogo variant={logo} />
      {tenant && <T.LogoTextColored tt="extra">{tenant}</T.LogoTextColored>}
      {children}
    </CC>
  );
}
