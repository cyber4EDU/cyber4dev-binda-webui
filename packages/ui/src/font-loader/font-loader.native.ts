import { Barrio_400Regular } from '@expo-google-fonts/barrio';
import { useFonts, Lexend_400Regular, Lexend_600SemiBold } from '@expo-google-fonts/lexend';

export function useAppFonts() {
  return useFonts({
    // no spaces allowed in fonts!
    Lexend_400Regular,
    Lexend_600SemiBold,
    Barrio_400Regular,
  });
}
