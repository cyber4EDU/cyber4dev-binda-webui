import '@fontsource/lexend/400.css';
import '@fontsource/lexend/600.css';
import '@fontsource/barrio/400.css';

export function useAppFonts(): [boolean, Error | null] {
  return [true, null];
}
