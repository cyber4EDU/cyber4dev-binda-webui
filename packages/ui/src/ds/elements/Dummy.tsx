import { ReactNode } from 'react';
import { Text, ViewProps } from 'react-native';

import { Container } from './Container';
import { Slot, SlotProps } from '../base/Slot';
import { styled } from '../base/base';

function DummyCmp({ text = 'Dummy Component', ...props }: { text: string } & ViewProps) {
  return (
    <Container {...props}>
      <Text>{text}</Text>
    </Container>
  );
}

export const Dummy = styled(DummyCmp, 'bg-orange-300 p-11');

export const DummySlot = styled(
  ({
    asChild,
    children,
    ...props
  }: SlotProps<any> & { asChild?: boolean; children?: ReactNode }) => {
    const Comp = asChild ? Slot : Container;
    return (
      <Comp {...props}>
        <Text>{children}</Text>
      </Comp>
    );
  },
);
