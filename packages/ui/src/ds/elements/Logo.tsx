import { C, T } from './_';
import { LogoHBw, LogoSignetFull } from '../../../assets/logos';

export function Logo() {
  return <LogoHBw width={98} height={30} title="BINDA" />;
}

export function LogoSignet() {
  return (
    <LogoSignetFull
      width={160}
      height={160}
      title="BINDA"
      aria-label="BINDA"
      accessibilityLabel="BINDA"
      role="img"
    />
  );
}

export function LogoWithName({ name }: { name?: string }) {
  return (
    <C className="fxr ai-fe">
      <Logo />
      {name && (
        <>
          <T.LogoText className="ml-1">x </T.LogoText>
          <T.LogoTextColored>{name}</T.LogoTextColored>
        </>
      )}
    </C>
  );
}
