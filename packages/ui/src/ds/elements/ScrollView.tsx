import { ScrollView as RNScrollView, ScrollViewProps, ViewStyle } from 'react-native';

import { styled } from './_';

const StyledOuterScrollView = styled(RNScrollView, 'fx-1');

// TODO forwardRef
function StyleableInnerScrollView({
  children,
  style,
  outerCn,
  outerStyle,
}: Omit<ScrollViewProps, 'contentContainerStyle'> & { outerCn?: string; outerStyle?: ViewStyle }) {
  return (
    <StyledOuterScrollView className={outerCn} style={outerStyle} contentContainerStyle={style}>
      {children}
    </StyledOuterScrollView>
  );
}
/**
 * This ScrollView turns the props "around"
 * style / className => contentContainerStyle
 * outerStyle / outerCn => style
 */
export const ScrollView = styled(StyleableInnerScrollView, 'grow-1');
