import { ComponentProps, ReactNode } from 'react';
import { Pressable } from 'react-native';

import { T } from './_';
import { Q } from '..';
import { C, CR } from '../base';

export function CourseCard({
  count,
  label,
  subLabel,
}: {
  label?: string;
  subLabel?: string;
  count?: number;
}) {
  return (
    <CR className="fx-1 rounded-md bg-white">
      <CR className="gap-1" pointerEvents="none">
        <T.CourseCardTitle>{label}</T.CourseCardTitle>
        <T.CourseCardSubtitle>{subLabel}</T.CourseCardSubtitle>
      </CR>
      <T.PupilCount>{count}</T.PupilCount>
    </CR>
  );
}

export function PupilCard({
  label,
  subLabel,
  children,
}: {
  label?: string;
  subLabel?: string;
  children?: ReactNode;
}) {
  return (
    <CR className="fx-1 rounded-md bg-white">
      <C className="gap-1">
        <T.PupilCardTitle>{label}</T.PupilCardTitle>
        <T.PupilCardSubtitle>{subLabel}</T.PupilCardSubtitle>
      </C>
      <C>{children}</C>
    </CR>
  );
}

type PillType = NonNullable<ComponentProps<typeof Q.PillSmall>['type']>;

export type PupilScanCardProps =
  | {
      label?: string;
      subLabel?: string;
      children?: never;
      mode: PillType | `${PillType}-`;
      into: string;
      onPress?: () => void;
    }
  | {
      label?: string;
      subLabel?: string;
      mode?: never;
      into?: never;
      children?: ReactNode;
      onPress?: () => void;
    };

export function PupilScanCard({
  label,
  subLabel,
  children,
  into,
  mode,
  onPress,
}: PupilScanCardProps) {
  const type = (mode?.endsWith('-') ? mode.slice(0, -1) : mode) as PillType | undefined;
  const checked = !mode?.endsWith('-');
  return (
    <Pressable onPress={onPress}>
      <C className="fx shdw-sm flex-none gap-4 rounded-md border-2 border-white bg-white p-4">
        <C className="ai-c jc-c gap-1">
          <T.PupilTitle>{label}</T.PupilTitle>
          <T.PupilSubtitle>{subLabel}</T.PupilSubtitle>
        </C>
        <T.AbsentCardTitle>{mode}</T.AbsentCardTitle>

        <C>
          {mode && <Q.PillSmall label={into ?? 'Ist Da'} checked={checked} type={type} />}
          {!mode && children}
        </C>
      </C>
    </Pressable>
  );
}
