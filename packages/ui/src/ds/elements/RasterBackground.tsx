import { ViewProps } from 'react-native';

import Raster from '../../../assets/bg/Raster';
import { C, styled } from '../base';

export function AbsoluteFillRaster() {
  return (
    <C className="bg-prim fixed bottom-0 left-0 right-0 top-0">
      <Raster width="100%" height="100%" color="#5b79ff" />
    </C>
  );
}

function RasterBGComp({ children, ...props }: ViewProps) {
  return (
    <C {...props}>
      <AbsoluteFillRaster />
      {children}
    </C>
  );
}

export const RasterBackgroundContainer = styled(RasterBGComp, 'fx-1');
