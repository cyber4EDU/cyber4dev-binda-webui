import { ComponentProps } from 'react';

import * as Icons from '../../../assets/icons';

export type IconName = keyof typeof Icons;
export type IconProps = ComponentProps<(typeof Icons)[IconName]>;
export function Icon({ size = 6, name, ...props }: { size?: number; name?: IconName } & IconProps) {
  const w = props.width ?? size * 4;
  const h = props.height ?? size * 4;
  if (!name) return null;
  const IconComp = Icons[name];
  return <IconComp width={w} height={h} {...props} />;
}
