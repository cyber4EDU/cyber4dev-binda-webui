import { Platform, ScrollViewProps, ViewStyle } from 'react-native';

import { ScrollView } from './ScrollView';
import { C } from './_';

export function Screen({
  children,
  contentCn,
  contentStyle,
  scrollerCn,
  scrollerStyle,
  ScrollViewProps,
}: {
  children: React.ReactNode;
  contentCn?: string;
  contentStyle?: ViewStyle;
  scrollerCn?: string;
  scrollerStyle?: ViewStyle;
  ScrollViewProps?: ScrollViewProps;
}) {
  if (Platform.OS === 'web') {
    return (
      <C className={contentCn} style={contentStyle}>
        {children}
      </C>
    );
  }
  return (
    <ScrollView
      className={contentCn}
      outerCn={scrollerCn}
      outerStyle={scrollerStyle}
      style={contentStyle}
      {...ScrollViewProps}
    >
      {children}
    </ScrollView>
  );
}
