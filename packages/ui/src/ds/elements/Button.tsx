import { ComponentProps, ComponentType, ReactNode } from 'react';
import { Pressable, PressableProps } from 'react-native';

import { Icon, IconName } from './Icon';
import { C, CR, T, styled } from './_';

export const StyledPressable = styled(Pressable);

const StyledButton = styled(Pressable, 'rounded-sm fxr border-2 border-transparent', {
  variants: {
    variant: {
      big: 'px-3 py-4 min-w-60 gap-4 fxcc',
      small: 'px-4 py-2.5 gap-4 fxcc',
      iconOnly: 'p-2.5 min-w-11 min-h-11 fxcc',
      text: 'px-2 py-3 gap-1 fxcc',
      select: 'py-1.5 px-3 gap-2 fxcc',
      card: 'px-3 py-2 gap-4 ai-c jc-sb fx-1',
      course: 'px-3 py-3 ai-c jc-sb fx-1',
      round: 'p-4 rounded-full',
      xs: 'px-2 py-1 gap-2 fxcc',
    },
    intent: {
      primary:
        'bg-prim-300  hover:bg-prim-500 active:bg-prim-300 active:border-2 active:border-prim-500',
      secondary: 'bg-sec hover:bg-sec-800',
      warn: 'bg-yllw-300 hover:bg-yllw-500',
      danger: 'bg-orng-300 hover:bg-orng-500',
      safe: 'bg-mint-300 hover:bg-mint-500',
      info: 'bg-white hover:bg-rose-300 active:bg-rose-300 active:border-2 active:border-prim-500',
      info2:
        'bg-rose-300 hover:bg-rose-500 active:bg-rose-500 active:border-2 active:border-rose-800',
      none: 'bg-none ',
    },
    selected: {
      true: 'border-2 border-prim-500',
    },
    disabled: {
      true: 'opacity-40',
    },
    bare: {
      true: 'bg-transparent hover:bg-rose-500',
    },
    shadowless: {
      false: 'shdw-sm',
      force: 'shdw-none',
    },
  },
  compoundVariants: [
    {
      variant: 'text',
      className: 'bg-none',
    },
    {
      bare: true,
      shadowless: false,
      className: 'shdw-none',
    },
  ],
  defaultProps: {
    mode: 'small',
    intent: 'secondary',
    shadowless: false,
    role: 'button',
  },
});
type StyledButtonProps = ComponentProps<typeof StyledButton> & { title?: string };
const ExtStyledButton: ComponentType<StyledButtonProps> = StyledButton;

export interface ButtonProps {
  variant?: StyledButtonProps['variant'];
  intent?: StyledButtonProps['intent'];
  bare?: StyledButtonProps['bare'];
  selected?: StyledButtonProps['selected'];
  label: string;
  iconName?: IconName;
  onPress?: PressableProps['onPress'];
  disabled?: PressableProps['disabled'];
  PressableProps?: PressableProps;
}

export function Button({ variant, label, iconName, PressableProps, ...props }: ButtonProps) {
  const finalVariant = !label && iconName ? 'iconOnly' : variant;
  const ButtonText = variant === 'text' ? T.ButtonTextSmall : T.ButtonText;
  return (
    <StyledButton {...props} variant={finalVariant} {...PressableProps}>
      {label && (
        <ButtonText className={variant === 'small' || variant === 'big' ? 'uppercase' : ''}>
          {label}
        </ButtonText>
      )}
      <Icon name={iconName} />
    </StyledButton>
  );
}

export function BigButton({
  label,
  iconName,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'>) {
  return (
    <StyledButton {...props} variant="big" {...PressableProps}>
      <T.ButtonText className="uppercase">{label}</T.ButtonText>
      <Icon name={iconName} />
    </StyledButton>
  );
}

export function RoundButton({
  label,
  iconName,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'>) {
  return (
    <StyledButton
      className="shdw-md"
      {...props}
      variant="round"
      {...PressableProps}
      aria-label={label}
    >
      <Icon name={iconName} />
    </StyledButton>
  );
}

export function SmallButton({
  label,
  iconName,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'>) {
  return (
    <StyledButton {...props} variant="small" {...PressableProps}>
      <T.ButtonText className="uppercase">{label}</T.ButtonText>
      <Icon name={iconName} />
    </StyledButton>
  );
}

export function IconButton({
  label,
  iconName,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'>) {
  return (
    <ExtStyledButton
      {...props}
      variant="iconOnly"
      {...PressableProps}
      title={label}
      aria-label={label}
    >
      <Icon name={iconName} />
    </ExtStyledButton>
  );
}

export function TextButton({
  label,
  iconName,
  intent = 'none',
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'>) {
  return (
    <StyledButton {...props} variant="text" intent={intent} {...PressableProps}>
      <T.ButtonTextSmall>{label}</T.ButtonTextSmall>
      <Icon name={iconName} />
    </StyledButton>
  );
}

export function SelectButton({
  label,
  iconName,
  intent = 'info',
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'>) {
  return (
    <StyledButton
      {...props}
      variant="select"
      intent={intent}
      className="flex-none"
      {...PressableProps}
    >
      <Icon name={iconName} size={3.5} />
      <T.ButtonTextSmall>{label}</T.ButtonTextSmall>
    </StyledButton>
  );
}

export function AbsentCardButton({
  label,
  count,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'> & { count: number }) {
  return (
    <StyledButton {...props} variant="card" className="flex-1" {...PressableProps}>
      <T.AbsentCardTitle>{label}</T.AbsentCardTitle>
      <T.PupilCount>{count}</T.PupilCount>
    </StyledButton>
  );
}

export function CourseCardButton({
  count,
  label,
  responsible,
  subLabel,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'> & { subLabel?: string; count?: number; responsible?: boolean }) {
  return (
    <StyledButton
      intent={responsible ? 'primary' : 'info'}
      {...props}
      variant="course"
      {...PressableProps}
      pointerEvents="box-only"
      className="pointer-events-none"
    >
      <C className="gap-1" pointerEvents="none">
        <T.CourseCardTitle>{label}</T.CourseCardTitle>
        <T.CourseCardSubtitle>{subLabel}</T.CourseCardSubtitle>
      </C>
      <T.PupilCount>{count}</T.PupilCount>
    </StyledButton>
  );
}

export function PupilCardButton({
  label,
  subLabel,
  children,
  PressableProps,
  ...props
}: Omit<ButtonProps, 'size'> & { subLabel?: string; children?: ReactNode }) {
  return (
    <StyledButton
      intent="info"
      {...props}
      variant="course"
      {...PressableProps}
      pointerEvents="box-only"
      className="pointer-events-none"
    >
      <CR className="fx-1">
        <C className="gap-1" pointerEvents="none">
          <T.PupilCardTitle>{label}</T.PupilCardTitle>
          <T.PupilCardSubtitle>{subLabel}</T.PupilCardSubtitle>
        </C>
        <C>{children}</C>
      </CR>
    </StyledButton>
  );
}
