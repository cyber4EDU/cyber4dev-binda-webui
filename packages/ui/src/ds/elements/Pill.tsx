import { $$ } from '@qoobz/quant';
import { useState } from 'react';

import { Icon, IconName } from './Icon';
import { C, CR, T } from './_';
import { colors } from '../../tw/theme/colors';

export function Pill({ count }: { count: number | '-' }) {
  return (
    <C className="fxcc">
      <C className="rounded-lg bg-black px-3 py-1">
        <T.PillTitleBig className="text-white">{count}</T.PillTitleBig>
      </C>
    </C>
  );
}

const PillSmallVariantMap = $$({
  filter: ['bg-sec-500 border-sec-500', 'bg-sec-300 border-sec-300'],
  excused: ['bg-yllw-300 border-yllw-300', 'bg-white border-yllw-500 border-dashed'],
  checkedin: ['bg-mint-300 border-mint-300', 'bg-white border-mint-500 border-dashed'],
  missing: [
    'bg-orng-300 border-orange-300',
    'bg-white border-orng-500 border-dashed',
    'text-orng-800',
    colors.orange[800],
  ],
  default: ['bg-white border-yllw-500 border-dashed', 'bg-yllw-300 border-yllw-300'],
});

export function PillSmall({
  label,
  leftIcon,
  checkedIcon,
  rightIcon,
  type = 'default',
  checked,
  initialChecked,
  onPress,
}: {
  label: string;
  leftIcon?: IconName;
  checkedIcon?: IconName;
  rightIcon?: IconName;
  checked?: boolean;
  initialChecked?: boolean;
  onPress?: (checked: boolean) => void;
  type?: keyof typeof PillSmallVariantMap;
}) {
  const [uncChecked, setUncChecked] = useState(!!initialChecked);
  const uncontrolled = checked === undefined;
  const finalChecked = uncontrolled ? uncChecked : checked;
  const variantCn = PillSmallVariantMap[type][finalChecked ? 0 : 1];
  const textVariantCn = (!finalChecked && PillSmallVariantMap[type][2]) || undefined;
  const iconColor = (!finalChecked && PillSmallVariantMap[type][3]) || 'black';
  const iconLeft: IconName | undefined = checkedIcon && finalChecked ? checkedIcon : leftIcon;

  const handlePress = () => {
    if (uncontrolled) {
      setUncChecked(!finalChecked);
      onPress?.(!finalChecked);
    } else {
      onPress?.(!finalChecked);
    }
  };

  return (
    <C className="fxcc">
      {/* // TODO ioss: aria-role ?? checkbox? */}
      <CR
        className={`border-1 gap-1 rounded-full px-3 py-1 ${variantCn}`}
        role="button"
        onPointerUp={handlePress}
      >
        {iconLeft && <Icon name={iconLeft} size={4} color={iconColor} />}
        <T.PillTitleSmall className={textVariantCn}>{label}</T.PillTitleSmall>
        {rightIcon && <Icon name={rightIcon} size={4} color={iconColor} />}
      </CR>
    </C>
  );
}
