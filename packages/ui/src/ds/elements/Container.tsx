import { View } from 'react-native';

import { styled } from './_';

export const Container = styled(View, 'p-4', {
  variants: {
    bg: {
      blue: 'bg-blue-300',
      rose: 'bg-rose-300',
      orange: 'bg-orng-300',
      yellow: 'bg-yllw-300',
      mint: 'bg-mint-300',
      pink: 'bg-pink-300',
      white: 'bg-white',
      black: 'bg-black',
      grey: 'bg-grey',
    },
    size: {
      sm: 'rounded-sm',
      md: 'rounded-md',
      lg: 'rounded-lg',
    },
  },
  defaultProps: {
    bg: 'rose',
    size: 'lg',
  },
});
