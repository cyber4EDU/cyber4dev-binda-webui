import React, { ComponentProps } from 'react';
import { Text } from 'react-native';

import { StyledText } from './text-utils';
import { styled } from '../base';

export const LogoText = styled(Text, '', {
  variants: {
    tt: {
      base: 'font-fancy text-base',
      big: 'font-fancy text-3xl',
      extra: 'font-fancy text-huge',
    },
  },
  defaultProps: {
    tt: 'base',
  },
});

const logoTextColorMap = [
  'text-blue-800',
  'text-orng-800',
  'text-yllw-800',
  'text-mint-800',
  'text-pink-800',
];
const logoTextColorMapExtra = [
  'text-blue-500',
  'text-orng-500',
  'text-yllw-500',
  'text-mint-500',
  'text-pink-500',
];

export function LogoTextColored({
  children,
  tt = 'big',
  ...props
}: ComponentProps<typeof LogoText>) {
  const processedChildren = React.useMemo(() => {
    let cnt = 0;
    const colorMap = tt === 'extra' ? logoTextColorMapExtra : logoTextColorMap;
    return React.Children.toArray(children)
      .map((child) => {
        if (typeof child === 'string') {
          return child.split('').map((char, i) => {
            if (char !== ' ') {
              return (
                <StyledText key={i} className={colorMap[cnt++ % 5]}>
                  {char}
                </StyledText>
              );
            } else {
              return <Text> </Text>;
            }
          });
        } else {
          return child;
        }
      })
      .flat(1);
  }, [children]);
  return (
    <LogoText tt={tt} {...props}>
      {processedChildren}
    </LogoText>
  );
}
