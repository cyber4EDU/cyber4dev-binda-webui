import { $$ } from '@qoobz/quant';

import { createHeader, createText } from './text-utils';

const BodySmallRegular = createText('BodySmallRegular', 'body_sm');

export const HeaderTitle = createHeader('HeaderTitle', { level: 1, tt: 'body_sb' });
export const HeaderSubtitle = BodySmallRegular;

export const AbsentCardTitle = createHeader('AbsentCardLabel', { level: 2, tt: 'body_sm' });

export const PupilCount = createText('PupilCount', 'display');

export const CourseCardTitle = createHeader('CourseCardTitle', { level: 2, tt: 'body_sb' });
export const CourseCardSubtitle = BodySmallRegular;

export const PupilCardTitle = CourseCardTitle;
export const PupilCardSubtitle = BodySmallRegular;

export const PupilTitle = createHeader('PupilTitle', { level: 2, tt: 'title_sm' });
export const PupilSubtitle = createText('PupilSubtitle', 'title_sm');

export const PupilDetailInfo = BodySmallRegular;

export const ContactName = createText('ContactName', 'body_lg_sb');
export const ContactRole = BodySmallRegular;
export const ContactInfoType = BodySmallRegular;
export const ContactInfoLink = createText('ContactInfoLink', 'body'); // TODO underline

export const NoteHeader = createText('NoteHeader', 'body_sm_sb');
export const NoteBody = BodySmallRegular;

export const AddOptionsTitle = createText('AddOptionsTitle', 'body_sb');
export const AddOptionsSubtitle = BodySmallRegular;

export const ListTitle = createHeader('ListTitle', { level: 2, tt: 'title' });
export const PageHeader = createHeader('PageHeader', { level: 1, tt: 'pageheader' });
export const PageHeaderDark = createHeader('PageHeader', { level: 1, tt: 'headline' });

export const ButtonText = createText('ButtonText', 'body_lg_sb', $$('text-center'));
export const ButtonTextSmall = createText('ButtonTextSmall', 'body_sm', $$('text-center'));
export const SearchInput = BodySmallRegular;
export const SelectInfo = createText('SelectInfo', 'body');
export const PillTitleBig = createText('PillTitleBig', 'body');
export const PillTitleSmall = createText('PillTitleSmall', 'body_sm');
