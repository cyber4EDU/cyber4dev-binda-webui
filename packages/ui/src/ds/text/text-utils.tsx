import { ComponentProps, ComponentRef, forwardRef } from 'react';
import { Text, TextProps } from 'react-native';

import { textVariants } from '../../tw/theme/docs';
import { styled } from '../base';

export const StyledText = styled(Text);

const QText = styled(Text, '', {
  variants: {
    tt: textVariants,
  },
  defaultProps: {
    tt: 'body',
  },
});

export type QTextProps = ComponentProps<typeof QText>;
export type QTextRef = ComponentRef<typeof QText>;

export interface QHeadingProps extends ComponentProps<typeof QText> {
  children: TextProps['children'];
  style?: TextProps['style'];
  level?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
}

function heading2BTextProps({
  level = 1,
  tt,
  ...props
}: QHeadingProps): ComponentProps<typeof QText> & { 'aria-level': number } {
  const ftt = tt || 'headline';

  return {
    ...props,
    role: 'heading',
    'aria-level': level,
    tt: ftt,
  };
}

export function createText(displayName: string, tt: QTextProps['tt'], className?: string) {
  const Component = forwardRef<QTextRef, QTextProps>(function TextComp(props, ref) {
    return <QText ref={ref} {...props} className={`${className} ${props.className}`} />;
  });
  Component.defaultProps = {
    tt,
  };
  Component.displayName = displayName;
  return Component;
}

export function createHeader(
  displayName: string,
  {
    level,
    tt,
  }: {
    level: QHeadingProps['level'];
    tt: QTextProps['tt'];
  },
) {
  const Component = forwardRef<QTextRef, QHeadingProps>(function HeaderComp(props, ref) {
    const btextProps = heading2BTextProps(props);
    return <QText ref={ref} {...btextProps} />;
  });
  Component.defaultProps = {
    level,
    tt,
  };
  Component.displayName = displayName;
  return Component;
}
