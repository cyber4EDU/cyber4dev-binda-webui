import { Component, ComponentType, ReactNode } from 'react';
import { Pressable, Text, View } from 'react-native';

import { C, CC } from './Container';

export type QErrorBoundaryFallback = ComponentType<{ error?: unknown; onReset: () => void }>;

interface ErrorBoundaryProps {
  children: ReactNode;
  fallback?: QErrorBoundaryFallback;
}

export function QErrorBoundaryDefaultFallback({
  error,
  onReset,
}: {
  error?: unknown;
  onReset: () => void;
}) {
  const err = errorProps(error, true);
  const cause = errorProps(err.cause);
  return (
    <View
      style={{
        backgroundColor: '#FFA295',
        padding: 40,
        justifyContent: 'center',
        alignItems: 'center',
        gap: 10,
        flex: 1,
      }}
    >
      <C className="mb-11">
        <CC className="mb-8 gap-4">
          <Text>Fehler [{err.type}]</Text>
          <Text>{err.name}</Text>
        </CC>
        <Text style={{ color: 'white' }}>{err.message}</Text>
      </C>

      <Pressable onPress={onReset}>
        <C className="rounded-sm bg-white p-3">
          <Text style={{ color: '#FFA295', fontSize: 20 }}>RESET</Text>
        </C>
      </Pressable>

      {__DEV__ && (
        <C className="max-w-80 md:max-w-1/3 mt-11">
          <C className="border-t-2 border-white">
            <Text style={{ color: 'white' }}>{err.stack}</Text>
          </C>

          {cause && (
            <C className="border-t-2 border-white">
              <Text style={{ color: 'white' }}>Cause:</Text>
              <Text style={{ color: 'white' }}>{JSON.stringify(cause, null, 2)}</Text>
            </C>
          )}

          <C className="mt-8 border-t-2 border-white">
            <Text style={{ color: 'white' }}>Error:</Text>
            <Text style={{ color: 'white' }}>{JSON.stringify(err, null, 2)}</Text>
          </C>

          {!!err.cause && (
            <C className="border-t-2 border-white">
              <Text style={{ color: 'white' }}>Cause:</Text>
              <Text style={{ color: 'white' }}>{JSON.stringify(err.cause, null, 2)}</Text>
            </C>
          )}
        </C>
      )}
    </View>
  );
}

export class QErrorBoundary extends Component<
  ErrorBoundaryProps,
  { hasError: boolean; error?: unknown }
> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  onReset = () => {
    this.setState({ hasError: false, error: undefined });
  };

  static getDerivedStateFromError(error: unknown) {
    return { hasError: true, error };
  }

  render() {
    if (this.state.hasError) {
      const Fallback = this.props.fallback || QErrorBoundaryDefaultFallback;
      return <Fallback error={this.state.error} onReset={this.onReset} />;
    }
    return this.props.children;
  }
}

interface ErrorProps {
  type: 'ERROR' | 'UNKNOWN' | 'BE';
  name: string;
  message: string;
  cause: unknown;
  stack: string | undefined;
}

function errorProps<TProcEmpty extends boolean>(
  error: unknown,
  processEmpty?: TProcEmpty,
): TProcEmpty extends true ? ErrorProps : ErrorProps | undefined {
  const err = toErrorProps(error, processEmpty);
  if (err?.message === 'Failed to fetch') {
    return {
      type: 'BE',
      name: 'Keine Verbindung zum Backend',
      message: 'Zur Zeit ist keine Verbindung zum Backend möglich.',
      cause: err,
      stack: undefined,
    };
  }
  return err;
}

function toErrorProps<TProcEmpty extends boolean>(
  error: unknown,
  processEmpty?: TProcEmpty,
): TProcEmpty extends true ? ErrorProps : ErrorProps | undefined {
  // @ts-ignore
  if (!error && !processEmpty) return undefined;
  const isError = error instanceof Error;
  return {
    type: isError ? 'ERROR' : 'UNKNOWN',
    name: isError ? error.name : 'Unknown Error',
    message: error instanceof Error ? error.message : error ? String(error) : '',
    cause: isError ? error.cause : undefined,
    stack: isError ? error.stack : undefined,
  };
}
