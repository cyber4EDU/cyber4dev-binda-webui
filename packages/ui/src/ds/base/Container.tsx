import { View } from 'react-native';

import { styled } from './base';

export const C = styled(View);
export const C1 = styled(View, 'fx fx-1');
export const CC = styled(View, 'fxcc');
export const CR = styled(View, 'fxr jc-sb ai-c');
export const CH = styled(View, '', {
  variants: {
    hide: {
      false: '',
      '<lg': 'hidden lg:flex',
      '<md': 'hidden md:flex',
      '>md': 'flex md:hidden',
    },
  },
});
