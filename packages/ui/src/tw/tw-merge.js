const { extendTailwindMerge } = require('tailwind-merge');

const customTailwindMerge = extendTailwindMerge({
  classGroups: {
    shadow: ['shadow', { shdw: ['none', 'sm', 'md', 'lg', 'xl'] }],
    flexJustify: ['justify', { jc: ['fs', 'fe', 'c', 'sb'] }],
  },
});

module.exports = {
  twMerge: customTailwindMerge,
};
