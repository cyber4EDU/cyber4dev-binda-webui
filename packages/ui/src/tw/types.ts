import type { Config as TailwindConfig } from 'tailwindcss';

export type { Config as TailwindConfig } from 'tailwindcss';
export type PluginsConfig = TailwindConfig['plugins'];
export type ThemeConfig = TailwindConfig['theme'];

export type ContentConfig = {
  relative: true;
  files: string[];
};

export type CreateTwConfig = Omit<TailwindConfig, 'content'> & {
  relContentDirs?: string[];
  content?: ContentConfig;
  fixRNWOrder: boolean;
};

export type CreateNwConfig = CreateTwConfig & {
  mode: 'native' | 'web';
};

export type CreateDefaultTwConfig = {
  baseDir: string;
  mode: 'native' | 'web';
  type?: 'storybook' | 'next' | 'expo';
  theme?: ThemeConfig;
  plugins?: PluginsConfig;
};

export type FunctionWrapper<T, U> = (fn: (...args: T[]) => U) => (...args: T[]) => U;
