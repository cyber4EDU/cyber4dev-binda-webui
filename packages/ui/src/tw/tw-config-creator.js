// @ts-ignore
const nativewind = require('nativewind/tailwind');
const path = require('path');
/**
 * Creates a tailwind config creator with provided theme and plugins
 *
 * @param {import('./types').ThemeConfig} theme
 * @param {import('./types').PluginsConfig} plugins
 */
function twConfigCreator(theme, plugins) {
  /**
   * Creates a tailwind config with preset plugins and theme
   * @param {import('./types').CreateTwConfig} externalConfig
   * @returns {import('./types').TailwindConfig}
   */
  return function (externalConfig) {
    const { relContentDirs, content, fixRNWOrder, ...rest } = externalConfig;

    const cfgFiles = relContentDirs?.map((dir) => mergePath(dir, '**/*.{js,jsx,ts,tsx}')) ?? [];

    /** @type {import('./types').ContentConfig} */
    const mergedContent = {
      relative: true,
      files: [...(content?.files ?? []), ...cfgFiles],
    };

    return {
      important: fixRNWOrder ? 'html' : undefined,
      darkMode: 'class',
      ...rest,
      content: mergedContent,
      theme: mergeTheme(theme, rest.theme),
      plugins: [...toArray(plugins), ...(rest.plugins ?? [])],
    };
  };
}

/**
 * Creates a nativewind config creator with provided theme and plugins
 *
 * @param {import('./types').ThemeConfig} theme
 * @param {import('./types').PluginsConfig} plugins
 */
function nativewindConfigCreator(theme, plugins) {
  const createTwConfig = twConfigCreator(theme, plugins);
  /**
   * Creates a tailwind config with preset plugins and theme
   * and a nativewind preset
   * @param {import('./types').CreateNwConfig} externalConfig
   * @returns {import('./types').TailwindConfig}
   */
  return function (externalConfig) {
    // const isNative = externalConfig.mode !== 'web';
    const config = {
      presets: [nativewind],
      ...externalConfig,
    };
    const twConfig = createTwConfig(config);
    return twConfig;
  };
}

/**
 * Creates a nativewind config creator with provided theme and plugins
 *
 * @param {import('./types').ThemeConfig} theme
 * @param {import('./types').PluginsConfig} plugins
 */
function nativewindDefaultConfigCreator(theme, plugins) {
  /**
   * Creates a tailwind config with preset plugins and theme
   * and a nativewind preset
   * @param {import('./types').CreateDefaultTwConfig} externalConfig
   * @returns {import('./types').TailwindConfig}
   */
  return function ({ mode, baseDir, type, ...rest }) {
    const isNative = mode !== 'web';
    const rootDir = path.resolve(__dirname, '../../../..');
    const content = [
      path.resolve(rootDir, 'packages', 'ui', 'src', '**/*.{js,jsx,ts,tsx}'),
      path.resolve(rootDir, 'packages', 'app', '*', '**/*.{js,jsx,ts,tsx}'),
      path.resolve(rootDir, 'packages', 'modules', 'src', '**/*.{js,jsx,ts,tsx}'),
      path.resolve(baseDir, 'src', '**/*.{js,jsx,ts,tsx}'),
    ];
    if (type === 'storybook') {
      content.push(path.resolve(rootDir, 'packages', 'stories', 'src', '**/*.{js,jsx,ts,tsx}'));
      content.push(path.resolve(baseDir, '.storybook', '**/*.{js,jsx,ts,tsx}'));
      content.push(path.resolve(baseDir, 'stories', '**/*.{js,jsx,ts,tsx}'));
    }
    if (type === 'next') {
      content.push(path.resolve(baseDir, 'pages', '**/*.{js,jsx,ts,tsx}'));
    }
    console.info(`<<<< TW ${baseDir} >>>>`, JSON.stringify(content, null, 2));
    return {
      important: !isNative ? 'html' : undefined,
      darkMode: 'class',
      content,
      theme: mergeTheme(theme, rest.theme),
      plugins: [...toArray(plugins), ...(rest.plugins ?? [])],
    };
  };
}

/**
 * @template T
 * @param {T | T[]} value
 * return {T[]}
 */
function toArray(value) {
  return Array.isArray(value) ? value : [value];
}

/**
 *
 * @param {import('./types').ThemeConfig} baseTheme
 * @param {import('./types').ThemeConfig} addTheme
 * @returns {import('./types').ThemeConfig}
 */
function mergeTheme(baseTheme, addTheme) {
  return {
    ...baseTheme,
    ...addTheme,
    extends: {
      ...baseTheme?.extends,
      ...addTheme?.extends,
    },
  };
}

/**
 * Merge two pathes, so that there is only one slash between them
 * @param {string} a
 * @param {string} b
 * @returns string
 */
function mergePath(a, b) {
  if (a.endsWith('/') && b.startsWith('/')) {
    return `${a}${b.slice(1)}`;
  }

  if (a.endsWith('/') || b.startsWith('/')) {
    return `${a}${b}`;
  }

  return `${a}/${b}`;
}

module.exports = {
  twConfigCreator,
  nativewindConfigCreator,
  nativewindDefaultConfigCreator,
};
