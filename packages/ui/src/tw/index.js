const docs = require('./theme/docs.js');
const plugins = require('./theme/plugins.js');
const theme = require('./theme/theme.js');
// const { textVariants } = require('./theme/typography.js');
const {
  twConfigCreator,
  nativewindConfigCreator,
  nativewindDefaultConfigCreator,
} = require('./tw-config-creator.js');
const { twMerge } = require('./tw-merge.js');

module.exports = {
  docs,
  theme,
  qtwMerge: twMerge,
  createTailwindConfig: twConfigCreator(theme, plugins),
  createNativewindConfig: nativewindConfigCreator(theme, plugins),
  createNativewindDefaultConfig: nativewindDefaultConfigCreator(theme, plugins),
};
