const White = '#FFFFFF';
const Grey = '#EDEDED';
const Black = '#000000';
const bittersweetorange = { DEFAULT: '#FF6953', 300: '#FFA295', 500: '#FF6953', 800: '#D63A24' };
const cottoncandy = { DEFAULT: '#FEB0E2', 300: '#FFD9F1', 500: '#FEB0E2', 800: '#E753B1' };
const darkmint = { DEFAULT: '#01C97B', 300: '#7FEBC1', 500: '#01C97B', 800: '#0D945F' };
const dawnpink = { DEFAULT: '#F6E5E5', 300: '#FEF8F8', 500: '#F6E5E5', 800: '#DDC5C5' };
const luckymustard = { DEFAULT: '#FBCC58', 300: '#FFEBB9', 500: '#FBCC58', 800: '#E9B535' };
const moodycornflower = { DEFAULT: '#5B79FF', 300: '#A7B7FF', 500: '#5B79FF', 800: '#2A48D1' };

const colors = {
  white: White,
  grey: Grey,
  black: Black,
  // c$orange: bittersweetorange,
  // c$candy: cottoncandy,
  // c$mint: darkmint,
  // c$pink: dawnpink,
  // c$mustard: luckymustard,
  // c$cornflower: moodycornflower,
  sec: cottoncandy,
  prim: moodycornflower,
  pink: cottoncandy,
  blue: moodycornflower,
  mint: darkmint,
  orange: bittersweetorange,
  orng: bittersweetorange,
  yellow: luckymustard,
  yllw: luckymustard,
  rose: dawnpink,
  bg: cottoncandy[300],
  transparent: 'transparent',
};

/**
 *
 * @param {typeof bittersweetorange} color
 */
function removeDefault(color) {
  const { DEFAULT, ...rest } = color;
  return rest;
}

const colorDocs = [
  {
    title: 'Black & White',
    colors: { White, Grey, Black },
  },
  {
    title: 'prim / blue',
    subtitle: 'Moody Cornflower',
    colors: removeDefault(colors.prim),
  },
  {
    title: 'sec / pink',
    subtitle: 'Cotton Candy',
    colors: removeDefault(colors.sec),
  },
  {
    title: 'orange / (orng)',
    subtitle: 'Bittersweet Orange',
    colors: removeDefault(colors.orange),
  },
  {
    title: 'mint',
    subtitle: 'Dark Mint',
    colors: removeDefault(colors.mint),
  },
  {
    title: 'yellow / (yllw)',
    subtitle: 'Lucky Mustard',
    colors: removeDefault(colors.yellow),
  },
  {
    title: 'rose',
    subtitle: 'Dawn Pink',
    colors: removeDefault(colors.rose),
  },
];

module.exports = {
  colors,
  colorDocs,
};
