const REM_VALUE = 16;
const GRID_SIZE = 4;

/** @typedef {'gr' | 'rem' | 'px'} Unit */
/** @typedef {{value: number, unit: Unit}} UnitValue */

/**
 * @param {number} value in pixels
 * @param {Unit} fromUnit unit to convert from
 * @param {Unit} toUnit unit to convert to
 * @returns {number} the value in the given unit
 */
function convertValue(value, fromUnit, toUnit) {
  if (fromUnit === toUnit) {
    return value;
  }
  let pxValue;
  switch (fromUnit) {
    case 'gr':
      pxValue = value * GRID_SIZE;
      break;
    case 'rem':
      pxValue = value * REM_VALUE;
      break;
    case 'px':
      pxValue = value;
      break;
    default:
      throw new Error(`Unknown unit ${fromUnit}`);
  }
  switch (toUnit) {
    case 'gr':
      return pxValue / GRID_SIZE;
    case 'rem':
      return pxValue / REM_VALUE;
    case 'px':
      return pxValue;
    default:
      throw new Error(`Unknown unit ${toUnit}`);
  }
}

/**
 * Convert from one UnitValue to another
 * @param {UnitValue} unitValue
 * @param {Unit} toUnit
 * @returns {UnitValue}
 */
// eslint-disable-next-line no-unused-vars
function convertUnitValue(unitValue, toUnit) {
  if (unitValue.unit === toUnit) {
    return unitValue;
  }
  return {
    value: convertValue(unitValue.value, unitValue.unit, toUnit),
    unit: toUnit,
  };
}

/**
 *
 * @param {number | string} value the value to convert to a unit, if the value has no unit, the default is 'gr' (grid size)
 * @param {Unit} unit the unit to convert to, if no unit is given, the default is 'gr' (grid size)
 * @param {{value: number, unit: Unit}} unit
 */
function toUnitValue(value, unit = 'gr') {
  if (typeof value === 'string') {
    const nv = value.trim();
    if (nv.endsWith('px')) {
      return {
        value: convertValue(Number(nv.substring(0, nv.length - 2)), 'px', unit),
        unit,
      };
    }
    if (nv.endsWith('rem')) {
      return {
        value: convertValue(Number(nv.replaceAll('rem', '')), 'rem', unit),
        unit,
      };
    }
    // maybe handle other units here

    // now it is a size value (gr)
    return {
      value: convertValue(Number(nv), 'gr', unit),
      unit,
    };
  }
  return {
    value: convertValue(Number(value), 'gr', unit),
    unit,
  };
}

/**
 * Convert a size or a unitValue to pixels
 * @param {number | string | UnitValue} space
 * @return {number} pixels
 */
function px(space) {
  if (typeof space === 'number') {
    return space * GRID_SIZE;
  }
  if (typeof space === 'string') {
    return toUnitValue(space, 'px').value;
  }
  return convertValue(space.value, space.unit, 'px');
}

/**
 * Convert a size or a unitValue to pixels
 * @param {number | string | UnitValue} spaceValue
 * @return {string} with rem unit
 */
function sRem(spaceValue) {
  const value = space(spaceValue);
  return `${value / REM_VALUE}rem`;
}

/**
 * Convert a size or a unitValue to pixels
 * @param {number | string | UnitValue} spaceOrUnitValue
 * @return {string} with px unit
 */
function sPx(spaceOrUnitValue) {
  const value = px(spaceOrUnitValue);
  return `${value}px`;
}

/**
 * @param {number | string | UnitValue} value
 * @returns {number} the value in grid size
 */
function space(value) {
  if (typeof value === 'number') {
    return value;
  }
  if (typeof value === 'string') {
    return toUnitValue(value, 'gr').value;
  }
  return convertValue(value.value, value.unit, 'gr');
}

module.exports = {
  px,
  sPx,
  space,
  sRem,
};
