const screens = {
  // keep the defaults for now
  sm: '640px',
  md: '768px',
  lg: '1024px',
  xl: '1280px',
  '2xl': '1536px',
  bpxs: '320px',
  bpsm: '480px',
  bpmd: '640px',
  bptp: '768px',
  bptl: '1024px',
};

module.exports = { screens };
