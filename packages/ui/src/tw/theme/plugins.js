// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Maybe don't use plugins?
// Support varies between frameworks
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/** @typedef {import('tailwindcss/types/config').CSSRuleObject} CSSRuleObject */

/** @type {import('tailwindcss/plugin')} */
const plugin = require('tailwindcss/plugin');

const { utilities, webUtilities } = require('./utilities');

/** @type {CSSRuleObject} */
// @ts-ignore
const utilitiesTWRNC = { ...utilities, ...webUtilities };
const utilitiesCN = Object.fromEntries(
  Object.entries(utilitiesTWRNC).map(([key, value]) => [`.${key}`, value]),
);

// console.info('<<<<<<<<<< Utilities: @q/ui >>>>>>>>>>', Object.keys(utilitiesCN));

const plugins = [
  plugin(({ addUtilities }) => {
    addUtilities({
      ...utilitiesTWRNC,
      ...utilitiesCN,
    });
  }),
];

module.exports = plugins;
