const { colors } = require('./colors');
// const { boxShadow } = require('./shadow');
// const { screens } = require('./screens');
const {
  borderRadius,
  borderWidth,
  spacing,
  height,
  maxHeight,
  minHeight,
  width,
  maxWidth,
  minWidth,
} = require('./sizes');
const { fontFamily, fontSize, fontWeight } = require('./typography');
// const { themeVariables, themeDarkVariables } = require('./vars');

/** @type {import('../types').ThemeConfig} */
const theme = {
  // variables: themeVariables,
  // darkVariables: themeDarkVariables,
  borderWidth,
  borderRadius,
  spacing,
  height,
  maxHeight,
  minHeight,
  width,
  maxWidth,
  minWidth,
  colors,
  // boxShadow,
  // @ts-ignore
  fontFamily,
  fontSize,
  fontWeight,
  // screens,
  extends: {},
};

module.exports = theme;
