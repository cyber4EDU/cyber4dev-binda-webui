const { px } = require('./base');
/**
 * Prefixes the keys of an object with a given string.
 * @param {string} key the prefix
 * @param {Record<string, any>} obj
 * @returns the object with prefixed keys
 */
// eslint-disable-next-line no-unused-vars
function j(key, obj) {
  return Object.fromEntries(
    Object.entries(obj).map(
      /**
       * @param {[string, any]} kv
       */
      ([k, v]) => [`${key}-${k}`, v],
    ),
  );
}

const spaceScale = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const flexShorthands = {
  fx: {
    display: 'flex',
  },
  'fx-1': {
    flex: 1,
  },
  fxcc: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  'ai-c': {
    alignItems: 'center',
  },
  'jc-c': {
    justifyContent: 'center',
  },
  'jc-sb': {
    justifyContent: 'space-between',
  },
  'jc-sa': {
    justifyContent: 'space-around',
  },
  'jc-fe': {
    justifyContent: 'flex-end',
  },
  'jc-fs': {
    justifyContent: 'flex-start',
  },
  'ai-fs': {
    alignItems: 'flex-start',
  },
  'ai-fe': {
    alignItems: 'flex-end',
  },
  'ai-sb': {
    alignItems: 'space-between',
  },
  'ai-sa': {
    alignItems: 'space-around',
  },
  fxr: {
    display: 'flex',
    flexDirection: 'row',
  },
  fxc: {
    display: 'flex',
    flexDirection: 'column',
  },
};

/**
 * @type {Record<string, any>}
 */
const utilities = {
  ...flexShorthands,
  fixed: {
    position: 'fixed',
  },
  btn: {
    borderRadius: px(2),
  },
  'shdw-none': {
    boxShadow: 'none',
  },
  'shdw-sm': {
    boxShadow: '0px 1px 1.4px rgba(0, 0, 0, 0.5)',
  },
  'shdw-md': {
    boxShadow: '1px 2px 4px rgba(0, 0, 0, 0.5)',
  },
};

const webUtilities = {
  'web--block': {
    display: 'block',
  },
  'web--inline-block': {
    display: 'inline-block',
  },
};

spaceScale.forEach((s) => {
  utilities[`gap-${s}`] = {
    gap: px(s),
  };
  utilities[`gap-x-${s}`] = {
    columnGap: px(s),
  };
  utilities[`gap-y-${s}`] = {
    rowGap: px(s),
  };
});

module.exports = {
  utilities,
  webUtilities,
};
