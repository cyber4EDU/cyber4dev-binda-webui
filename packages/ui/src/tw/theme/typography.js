/**
 * The used font family settings
 *
 * 1) Tailwind should set the "sans" font on the html-element. So it might be a good
 *    idea to use "sans" as the default font.
 *
 * 2) React-Native does not do font/weight resolution like web does. So:
 *    Use the font names from useAppFonts() first (the native ones)
 *    and check the fontname from @fontsource/* and use it as the second one in
 *    both sans & sans_bold
 */
const qPlatformTarget =
  process.env.Q_ENV_PLATFORM || process.env.NEXT_PUBLIC_Q_PLATFORM_TARGET || 'web';

const fontFamily = {
  body: [qPlatformTarget === 'native' && 'Lexend_400Regular', 'Lexend', 'sans-serif'].filter(
    Boolean,
  ),
  bodybold: [qPlatformTarget === 'native' && 'Lexend_600SemiBold', 'Lexend', 'sans-serif'].filter(
    Boolean,
  ),
  fancy: [qPlatformTarget === 'native' && 'Barrio_400Regular', 'Barrio', 'mono'].filter(Boolean),
};

// we use rem = 16px, so recalculate the sizes
/** @type {Record<string, [string, { lineHeight: string }]>} */
const fontSize = {
  xs: ['0.75rem', { lineHeight: '0.875rem' }], // repeat sm
  sm: ['0.75rem', { lineHeight: '1rem' }], // body small
  base: ['0.875rem', { lineHeight: '1.25rem' }], // body
  lg: ['1rem', { lineHeight: '1.25rem' }], // body large
  xl: ['1.125rem', { lineHeight: '1.5rem' }], // title-small
  '2xl': ['1.25rem', { lineHeight: '1.5rem' }], // title
  '3xl': ['1.5rem', { lineHeight: '1.75rem' }], // headline
  '4xl': ['2rem', { lineHeight: '2rem' }], // display
  huge: ['2.75rem', { lineHeight: '3.25rem' }], // splash screen fancy
};

const fontWeight = {
  // thin: '100',
  // extralight: '200',
  // xlight: '200',
  // light: '300',
  normal: '400',
  // medium: '500',
  semibold: '600',
  sb: '600',
  // bold: '600',
  // extrabold: '800',
  // xb: '800',
  // black: '900',
};

const textVariants = {
  body_sm: 'font-body text-sm',
  body_sm_sb: 'font-bodybold text-sm font-sb',
  body: 'font-body text-base',
  body_sb: 'font-bodybold text-base font-sb',
  body_lg: 'font-body text-lg',
  body_lg_sb: 'font-bodybold text-lg font-sb',
  title_sm: 'font-body text-xl',
  title: 'font-bodybold text-2xl font-sb',
  headline: 'font-bodybold text-3xl font-sb',
  pageheader: 'font-bodybold text-3xl font-sb text-white',
  display: 'font-bodybold text-4xl font-sb',
  logo: 'font-fancy text-3xl',
  logo_lg: 'font-fancy text-huge',
  logo_sm: 'font-fancy text-base',
};

module.exports = {
  fontFamily,
  fontSize,
  fontWeight,
  textVariants,
};
