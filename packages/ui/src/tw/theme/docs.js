module.exports = {
  colorDocs: require('./colors').colorDocs,
  textVariants: require('./typography').textVariants,
};
