import { createQuant } from '@qoobz/quant';

import { createTailwindConfig, qtwMerge } from './tw';

export { $$, createQuant, styled, useQuant } from '@qoobz/quant';

const createdQuant = createQuant({
  ...createTailwindConfig({
    fixRNWOrder: process.env['Q_PLATFORM_TARGET'] !== 'native',
  }),
  options: {
    mode: process.env['Q_PLATFORM_TARGET'] === 'native' ? 'native' : 'web',
    twMerge: qtwMerge,
  },
});

export const QuantProvider = createdQuant.QuantProvider;
export const quant = createdQuant.quant;
export const useDeviceContext = createdQuant.useDeviceContext;
