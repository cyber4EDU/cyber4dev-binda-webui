module.exports = {
  extends: [
    'universe/native',
    'universe/web',
    //  'universe/shared/typescript-analysis'
  ],
  // overrides: [
  //   {
  //     files: ['*.ts', '*.tsx', '*.d.ts'],
  //     parserOptions: {
  //       tsconfigRootDir: __dirname,
  //       project: [
  //         './packages/*/tsconfig.json',
  //         './apps/*/tsconfig.json',
  //         './@qoobz/packages/*/tsconfig.json',
  //       ],
  //     },
  //   },
  // ],
  overrides: [
    // Only uses Testing Library lint rules in test files
    {
      files: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'],
      extends: ['plugin:testing-library/react'],
    },
  ],
  plugins: ['testing-library'],
  env: {
    node: true,
    commonjs: true,
  },
  root: true,
};
