const ALL = {
  dependencies: ['**'],
  packages: ['**'],
};

const FIXED_DEPENDENCIES = [
  //
  'burnt',
  'expo',
  'expo-constants',
  'expo-linking',
  'expo-router',
  'metro',
  'metro-resolver',
  'react',
  'react-dom',
  'react-native',
  'react-native-web',
  'react-native-safe-area-context',
  'react-native-svg',
];
const DEV_TOOLS = [
  //
  '@types/react-native',
  '@types/react',
  'eslint',
  'prettier',
  'react-refresh',
  'tailwindcss',
  'ts-node',
  'typescript',
  'tailwindcss',
];

const DEPTYPE_ALL = [
  'dev', //	devDependencies
  'overrides', //	overrides
  'peer', //	peerDependencies
  'pnpmOverrides', //	pnpm.overrides
  'prod', //	dependencies
  'resolutions', //	resolutions
  'workspace', //	version
  'optional', //	optionalDependencies?
];

const DEPTYPE_ALL_BUT_WS = [
  'dev', //	devDependencies
  'overrides', //	overrides
  'peer', //	peerDependencies
  'pnpmOverrides', //	pnpm.overrides
  'prod', //	dependencies
  'resolutions', //	resolutions
  'optional', //	optionalDependencies?
];

const DEPTYPE_ALL_BUT_PEERS = [
  'dev', //	devDependencies
  'overrides', //	overrides
  'pnpmOverrides', //	pnpm.overrides
  'prod', //	dependencies
  'resolutions', //	resolutions
  'optional', //	optionalDependencies?
];

const DEPTYPE_LIB = [
  'dev', //	devDependencies
  'prod', //	dependencies
];

module.exports = {
  customTypes: {
    engines: {
      path: 'engines',
      strategy: 'versionsByName',
    },
    packageManager: {
      path: 'packageManager',
      strategy: 'name@version',
    },
  },
  sortFirst: [
    'name',
    'version',
    'private',
    'description',
    'scripts',
    'author',
    'exports',
    'main',
    'types',
    'react-native',
    'files',
  ],
  versionGroups: [
    {
      dependencies: ['**'],
      packages: ['@qoobz/**'],
      dependencyTypes: DEPTYPE_LIB,
      label: 'group @qoobz',
    },
    {
      dependencies: ['**'],
      packages: ['@qoobz/**'],
      dependencyTypes: DEPTYPE_ALL,
      label: 'group @qoobz peer',
    },
    {
      dependencies: ['@qoobz/**', '@q/*'],
      packages: ['**'],
      dependencyTypes: ['prod', 'dev', 'peer', 'optional'],
      pinVersion: '*',
    },
    {
      dependencies: ['**'],
      packages: ['**'],
      dependencyTypes: ['peer'],

      preferVersion: 'highestSemver',
    },

    {
      preferVersion: 'highestSemver',
      ...ALL,
      packages: ['!@qoobz/**'],
    },
  ],
  semverGroups: [
    {
      range: '',
      dependencies: FIXED_DEPENDENCIES,
      packages: ['**'],
      dependencyTypes: DEPTYPE_ALL_BUT_PEERS,
    },
    {
      range: '^',
      dependencies: DEV_TOOLS,
      packages: ['**'],
      dependencyTypes: DEPTYPE_ALL_BUT_PEERS,
    },
    // qoobz
    {
      range: '>=',
      dependencies: ['**'],
      packages: ['@qoobz/**'],
      dependencyTypes: ['peers'],
    },
    // -- normal
    {
      range: '^',
      dependencies: ['**'],
      packages: ['-qua-monorepo'],
      dependencyTypes: ['prod', 'dev', 'optional'],
    },

    {
      range: '^',
      dependencyTypes: ['dev'],
      ...ALL,
    },
    {
      range: '^',
      dependencyTypes: ['peer'],
      ...ALL,
    },
    {
      range: '',
      dependencyTypes: ['dependencies', 'overrides', 'pnpmOverrides', 'workspace', 'prod'],
      ...ALL,
    },
  ],
};
