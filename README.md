# cyber4dev-binda-webui

## Description

Frontends for BinDa

## Installation

After checking the projects out `yarn install`

## Usage

### Demo Deployment

For the web-preview

```bash
cd apps/web-pre
yarn build
yarn start
```

### Development

For the web-preview:

```bash
cd apps/web-pre
yarn dev
```

## Relevant Environment Variables

**The platform this client is made for**

> Q_PLATFORM_TARGET=web

\*\*To disable next telemetry https://nextjs.org/telemetry

> NEXT_TELEMETRY_DISABLED=1

**canonical URL of your site https://next-auth.js.org/configuration/options**

> NEXTAUTH_URL="http://localhost:3000"

**The base url of the Api-Server (without trailing "/")**

> NEXT_PUBLIC_API_BASE_URL="http://localhost:8000/api/v1"

**KeyCloak**

> KEYCLOAK_ID="binda-ui_access"
> KEYCLOAK_SECRET="<<<keycloak client secret>>>"
> KEYCLOAK_ISSUER="http://localhost:8282/realms/esbz"

**Encodes the session cookie between next and the browser**

> JWT_SECRET="<<<some funny secret>>>"

**!!! DEPRECATED !!!**

> NEXT_PUBLIC_USE_KEYCLOAK_IDS=true

**Which endpoint to use for the QRCode ID**
With physical(default)=associated print, identity=keycloak or member=databaseid

> NEXT_PUBLIC_QRCODE_TYPE=identity

**Enable testing tools in the page**

> NEXT_PUBLIC_TEST_MODE=false

**Duplicate for now from above**

> NEXT_PUBLIC_Q_PLATFORM_TARGET=web

**School name**

> NEXT_PUBLIC_TENANT_NAME='Eine Schule mit Namen'

**S for Schule, L for Lehrnbuero**

> NEXT_PUBLIC_TENANT_TYPE=S
