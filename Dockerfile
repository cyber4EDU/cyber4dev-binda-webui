# FIXME: this needs to be cleaned up
FROM node:18-alpine

COPY . /app

WORKDIR /app

RUN --mount=type=cache,target=/app/.yarn/cache \
    yarn && \
    yarn run build:web-app

EXPOSE 3000

CMD yarn run start:web-app
