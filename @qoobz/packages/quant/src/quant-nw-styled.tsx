import { styled as nwStyled, variants } from 'nativewind';
import { ComponentType, forwardRef } from 'react';

import { QuantWrapperProps } from './types';
import { AnyStyledOptions, ClassProp } from './v/nw-types';
import { CompoundVariant, ConfigSchema, ConfigVariants } from './v/nw-variants';
import { Styled } from './v/types';

export const styledNW: Styled = (
  Component: ComponentType,
  classValueOrOptions?: string | AnyStyledOptions,
  maybeOptions?: AnyStyledOptions,
) => {
  const { defaultProps, props, ...cvaOptions } =
    (typeof classValueOrOptions === 'string' ? maybeOptions : classValueOrOptions) ?? {};
  const baseClassValue = typeof classValueOrOptions === 'string' ? classValueOrOptions : '';
  const cvaConfig = { ...cvaOptions };

  const variantStyles =
    baseClassValue || Object.keys(cvaConfig).length
      ? variants(baseClassValue, cvaConfig)
      : (props: { className?: string }) => props.className;

  const StyledComponent = nwStyled(Component);

  const QWrapped = forwardRef<unknown, any>((props: QuantWrapperProps, ref: unknown) => {
    const { className, _, style, ...rest } = props;
    const cvaClassName = variantStyles({ ...rest, style, className: className || _ } as any);
    return <StyledComponent className={cvaClassName} {...(rest as any)} ref={ref} />;
  });
  const componentName =
    (typeof Component === 'string' ? Component : Component.displayName || Component.name) ??
    'Component';
  QWrapped.displayName = `Qvantified(${componentName})`;
  QWrapped.defaultProps = defaultProps;
  return QWrapped;
};

export type CVariantsConfig<T extends ConfigSchema> = ClassProp & {
  variants?: T;
  defaultProps?: ConfigVariants<T>;
  compoundVariants?: CompoundVariant<T>[];
};

// export function $$(x: string): string;
// export function $$<T extends ConfigSchema>(x: CVariantsConfig<T>): CVariantsConfig<T>;
export function $$<T>(x: T): T {
  return x;
}
