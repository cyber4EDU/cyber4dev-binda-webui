import { useShallowMemo } from '@qoobz/qutils/hooks/useShallowMemo';
import { cva } from 'class-variance-authority';
import { useMemo, useRef } from 'react';

import { QuantCVAConfig, QuantVariantProps } from './types';
import { CVAConfigSchema } from './v/types';
export const qva = cva;

export function useVariants<T extends CVAConfigSchema>(
  config: QuantCVAConfig<T>,
  props: QuantVariantProps<T>,
): string {
  const initialConfig = useRef(config);
  if (initialConfig.current !== config) {
    if (__DEV__) {
      console.warn('useCVA config must not change');
    } else {
      initialConfig.current = config;
    }
  }
  const qvaFn = useMemo(() => {
    let { className, ...cvaConfig } = initialConfig.current;
    const { defaultProps } = cvaConfig;
    if (defaultProps) {
      cvaConfig = {
        ...cvaConfig,
        defaultVariants: defaultProps,
      };
    }
    return qva(className || '', cvaConfig);
  }, [initialConfig.current]);

  const result = useShallowMemo((props: QuantVariantProps<T>) => qvaFn(props), [props], [qvaFn]);
  return result;
}

export function useVariantsFn<T extends CVAConfigSchema>(
  qvaFn: ReturnType<typeof qva<T>>,
  props: QuantVariantProps<T>,
): string {
  const initialFn = useRef(qvaFn);
  if (initialFn.current !== qvaFn) {
    if (__DEV__) {
      console.warn('useVariantsFn qvaFn must not change');
    } else {
      initialFn.current = qvaFn;
    }
  }
  const result = useShallowMemo(
    (props: QuantVariantProps<T>) => qvaFn(props as any), // TODO ioss: fix types
    [props],
    [initialFn.current],
  );
  return result;
}
