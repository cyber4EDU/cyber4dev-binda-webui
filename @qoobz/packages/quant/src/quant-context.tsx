import { createContext, useCallback, useContext, useRef } from 'react';

import { QuantContextValue, QuantOptions, QuantStyleFn, QuantStyleProps } from './types';

const QuantContext = createContext<QuantContextValue | null>(null);

export function createQuantProvider(defaultQuantValue: QuantContextValue, options?: QuantOptions) {
  return function QuantProvider({
    children,
    quant,
  }: {
    children: React.ReactNode;
    quant?: QuantContextValue;
  }) {
    return (
      <QuantContext.Provider value={quant || defaultQuantValue}>{children}</QuantContext.Provider>
    );
  };
}

export function useQuant() {
  const quant = useContext(QuantContext);
  if (!quant) {
    throw new Error('No QuantProvider found as ancestor of this component.');
  }
  return quant;
}

/**
 * Same as useQuant().style, but memoized.
 */
export function useQuantStyle(): QuantStyleFn {
  const quant = useQuant();

  const memoized = useRef<{ result: QuantStyleProps; dependencies: any[] }>({
    result: {},
    dependencies: [],
  });
  return useCallback(
    (style: any, ...inputs: any[]) => {
      if (!depsChanged(memoized.current.dependencies, style, inputs)) {
        return memoized.current.result;
      }
      const newResult = quant.style(style, ...inputs);
      memoized.current = {
        result: newResult,
        dependencies: [style, ...inputs],
      };
      return newResult;
    },
    [quant],
  );
}

function depsChanged(deps: any[], style: any, inputs: any[]) {
  if (deps.length !== inputs.length + 1) {
    return true;
  }
  if (deps[0] !== style) {
    return true;
  }
  for (let i = 0; i < inputs.length; i++) {
    if (deps[i + 1] !== inputs[i]) {
      return true;
    }
  }
  return false;
}
