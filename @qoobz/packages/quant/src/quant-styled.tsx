import { cx } from 'class-variance-authority';
import { variants } from 'nativewind';
import { ComponentType, forwardRef } from 'react';
import { StyleSheet } from 'react-native';
import { twMerge as origTwMerge } from 'tailwind-merge';
import { create as twrncCreate, useDeviceContext } from 'twrnc';

import { createQuantProvider, useQuantStyle } from './quant-context';
import { QuantConfig, QuantOptions, QuantStyleFn, QuantWrapperProps } from './types';
import { AnyStyledOptions, ClassProp } from './v/nw-types';
import { CompoundVariant, ConfigSchema, ConfigVariants } from './v/nw-variants';
import { Styled, TWRNCStyle } from './v/types';

const QUANTIFY_MODE = process.env['Q_PLATFORM_TARGET'] === 'web' ? 'web' : 'native'; // TODO get from env

export function createQuant(config: QuantConfig) {
  const { options = {}, ...twConfig } = config;
  const tw = twrncCreate(twConfig as any);
  const styleMap = new WeakMap<TWRNCStyle, TWRNCStyle>();
  const finalOptions: QuantOptions = {
    twMerge: origTwMerge,
    ...options,
    mode: options?.mode || QUANTIFY_MODE || 'native',
  };

  const styleFn: QuantStyleFn = (style, ...inputs) => {
    if (finalOptions.mode === 'native') {
      if (!inputs.length) {
        return { style };
      }
      const twStyle = tw.style(...(inputs as any));
      let cached = styleMap.get(twStyle);
      if (!cached) {
        cached = StyleSheet.create({ style: twStyle }).style;
        styleMap.set(twStyle, cached);
      }

      return {
        style: style ? [cached, style] : cached,
      };
    } else {
      if (!inputs.length) {
        return { style };
      }

      return {
        style,
        className: finalOptions.twMerge(cx(...inputs)),
      };
    }
  };

  const quantValue = {
    style: styleFn,
    options: finalOptions,
  };

  return {
    quant: quantValue,
    QuantProvider: createQuantProvider(quantValue, finalOptions),
    useDeviceContext: () => useDeviceContext(tw),
  };
}

export const styled: Styled = (
  Component: ComponentType,
  classValueOrOptions?: string | AnyStyledOptions,
  maybeOptions?: AnyStyledOptions,
) => {
  const { defaultProps, props, ...cvaOptions } =
    (typeof classValueOrOptions === 'string' ? maybeOptions : classValueOrOptions) ?? {};
  const baseClassValue = typeof classValueOrOptions === 'string' ? classValueOrOptions : '';
  const cvaConfig = { ...cvaOptions };

  const variantStyles =
    baseClassValue || Object.keys(cvaConfig).length
      ? variants(baseClassValue, cvaConfig)
      : (props: { className?: string }) => props.className;

  const QWrapped = forwardRef<unknown, any>((props: QuantWrapperProps, ref: unknown) => {
    const styleFn = useQuantStyle();
    const { className, _, style, ...rest } = props;

    const cvaClassName = variantStyles({ ...rest, style, className: className || _ } as any);
    const quantProps = styleFn(style, cvaClassName);
    const styles = quantProps.className ? [{ $$css: true }, quantProps.style] : quantProps.style;
    return <Component style={styles} dataSet={{ stype: 'qstyled' }} {...(rest as any)} ref={ref} />;
  });
  const componentName =
    (typeof Component === 'string' ? Component : Component.displayName || Component.name) ??
    'Component';
  QWrapped.displayName = `Qvantified(${componentName})`;
  QWrapped.defaultProps = defaultProps;
  return QWrapped;
};

export type CVariantsConfig<T extends ConfigSchema> = ClassProp & {
  variants?: T;
  defaultProps?: ConfigVariants<T>;
  compoundVariants?: CompoundVariant<T>[];
};

// export function $$(x: string): string;
// export function $$<T extends ConfigSchema>(x: CVariantsConfig<T>): CVariantsConfig<T>;
export function $$<T>(x: T): T {
  return x;
}
