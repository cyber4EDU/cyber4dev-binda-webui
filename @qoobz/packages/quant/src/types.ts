import { VariantProps, cva } from 'class-variance-authority';
import { ComponentType } from 'react';
import { twMerge } from 'tailwind-merge';

import { Style } from './v/nw-types';
import {
  CVAClassValue,
  CVAConfig,
  CVAConfigSchema,
  CVAConfigVariants,
  TWRNCStyle,
  TailwindConfig,
} from './v/types';

type Simplify<T> = { [KeyType in keyof T]: T[KeyType] } & object;

export type QuantStyle = Style;
export type QuantClassInput = CVAClassValue;

export interface QuantOptions {
  mode: 'web' | 'native';
  twMerge: typeof twMerge;
}

export interface QuantConfig extends TailwindConfig {
  options?: Partial<QuantOptions>;
}

export type QuantStyleProps = {
  className?: string;
  style?: QuantStyle | QuantStyle[];
};

export type QuantStyleFn = (
  style?: QuantStyle,
  inputs?: QuantClassInput | QuantClassInput[],
) => QuantStyleProps;

export interface QuantContextValue {
  options: QuantOptions;
  style: QuantStyleFn;
}

type ßCVAConfig<T> = Simplify<CVAConfig<T>>;

export type QuantStyledCVAConfig<T extends CVAConfigSchema> = ßCVAConfig<T> & {
  defaultProps?: CVAConfigVariants<T>;
};

export type QuantCVAConfig<T extends CVAConfigSchema> = {
  className?: CVAClassValue;
} & QuantStyledCVAConfig<T>;

export type QuantVariantProps<T extends QuantCVAConfig<any>> = T extends QuantCVAConfig<any>
  ? VariantProps<ReturnType<typeof cva<T['variants']>>>
  : never;

export type QuantifiableComponent<TProps = any> = ComponentType<
  TProps & { style: TWRNCStyle; className?: string }
>;

export type QuantWrapperProps =
  | {
      style?: Style;
      className?: string;
      _?: never;
    }
  | {
      style?: Style;
      className?: never;
      _?: string;
    };
