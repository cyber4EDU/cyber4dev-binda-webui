import type { cva } from 'class-variance-authority';
import { ClassValue, StringToBoolean } from 'class-variance-authority/dist/types';
import type { TailwindFn } from 'twrnc';

import type { Styled as NWStyled } from './nw-types';

export type { Config as TailwindConfig } from 'tailwindcss';
export type { TwConfig as TWRNCConfig } from 'twrnc';
export type TWRNCClassInput = Parameters<TailwindFn['style']>[0];
export type TWRNCStyle = ReturnType<TailwindFn>;
export type TWRNCStyleFn = TailwindFn['style'];

export type { VariantProps as CVAVariantProps } from 'class-variance-authority';
export type CVAClassValue = Parameters<typeof cva>[0];
export type CVAConfig<T> = NonNullable<Parameters<typeof cva<T>>[1]>;
export type CVAFn<T> = ReturnType<typeof cva<T>>;
export type CVAConfigSchema = Record<string, Record<string, ClassValue>>;
export type CVAConfigVariants<T extends CVAConfigSchema> = {
  [Variant in keyof T]?: StringToBoolean<keyof T[Variant]> | null | undefined;
};
export type Styled = NWStyled;
