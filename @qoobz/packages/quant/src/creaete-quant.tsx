import { cva, cx } from 'class-variance-authority';
import { ComponentProps, ComponentType } from 'react';
import { StyleSheet } from 'react-native';
import { twMerge as origTwMerge } from 'tailwind-merge';
import { create as twrncCreate, useDeviceContext } from 'twrnc';

import { createQuantProvider, useQuantStyle } from './quant-context';
import {
  QuantCVAConfig,
  QuantClassInput,
  QuantConfig,
  QuantOptions,
  QuantStyleFn,
  QuantVariantProps,
  QuantifiableComponent,
} from './types';
import { TWRNCStyle } from './v/types';

const QUANTIFY_MODE = process.env['Q_PLATFORM_TARGET'] === 'web' ? 'web' : 'native'; // TODO get from env

export function createQuant(config: QuantConfig) {
  const { options = {}, ...twConfig } = config;
  const tw = twrncCreate(twConfig as any);
  const styleMap = new WeakMap<TWRNCStyle, TWRNCStyle>();
  const finalOptions: QuantOptions = {
    twMerge: origTwMerge,
    ...options,
    mode: options?.mode || QUANTIFY_MODE || 'native',
  };

  const styleFn: QuantStyleFn = (style, ...inputs) => {
    if (finalOptions.mode === 'native') {
      if (!inputs.length) {
        return { style };
      }
      const twStyle = tw.style(...(inputs as any));
      let cached = styleMap.get(twStyle);
      if (!cached) {
        cached = StyleSheet.create({ style: twStyle }).style;
        styleMap.set(twStyle, cached);
      }

      return {
        style: style ? [cached, style] : cached,
      };
    } else {
      if (!inputs.length) {
        return { style };
      }

      return {
        style,
        className: finalOptions.twMerge(cx(...inputs)),
      };
    }
  };

  const quantValue = {
    style: styleFn,
    options: finalOptions,
  };

  return {
    quant: quantValue,
    QuantProvider: createQuantProvider(quantValue, finalOptions),
    useDeviceContext: () => useDeviceContext(tw),
  };
}

export function __quantify<TComponent extends QuantifiableComponent>(
  Component: TComponent,
  baseClassName?: QuantClassInput | QuantClassInput[],
) {
  const base = (Array.isArray(baseClassName) ? baseClassName : [baseClassName]).filter(Boolean);
  function QWrapped(
    props: ComponentProps<TComponent> & { className?: QuantClassInput | QuantClassInput[] },
  ) {
    const styleFn = useQuantStyle();
    const { className, style, ...rest } = props;
    const quantProps = styleFn(style, ...base, className);
    const styles = quantProps.className
      ? [{ $$css: true, x: quantProps.className }, quantProps.style]
      : quantProps.style;

    return <Component style={styles} {...(rest as any)} />;
  }
  QWrapped.displayName = `Quantified(${Component.displayName || Component.name} || 'Component')`;
  return QWrapped;
}

export function __qvariant<
  TTargetComponent extends ComponentType<{ style?: any; className?: string }>,
>() {
  return function qvantifier<
    TQConfig extends QuantCVAConfig<any>,
    TComponent extends ComponentType<
      ComponentProps<TTargetComponent> & QuantVariantProps<TQConfig> & { className?: string }
    >,
  >(Component: TComponent, cvaConfig: TQConfig) {
    const { className: baseClassName, ...config } = cvaConfig;
    const variantStyles = cva(baseClassName, config);

    function QWrapped(props: Omit<ComponentProps<TComponent>, 'class'>) {
      const cvaClassName = variantStyles(props as any);
      const { className, ...rest } = props;
      return <Component className={cvaClassName} {...(rest as any)} />;
    }
    QWrapped.displayName = `Qvantified(${Component.displayName || Component.name} || 'Component')`;
    return QWrapped;
  };
}

export function __qvantify<TProps extends { style?: any; className?: string }>() {
  return function qvantifier<
    TQConfig extends QuantCVAConfig<any>,
    TComponent extends ComponentType<TProps & QuantVariantProps<TQConfig> & { className?: string }>,
  >(Component: TComponent, cvaConfig: TQConfig) {
    const { className: baseClassName, ...config } = cvaConfig;
    const variantStyles = cva(baseClassName, config);

    function QWrapped(props: Omit<ComponentProps<TComponent>, 'class'>) {
      const styleFn = useQuantStyle();
      const cvaClassName = variantStyles(props as any);
      const { className, style, ...rest } = props;
      const quantProps = styleFn(style, cvaClassName);
      const styles = quantProps.className
        ? [{ $$css: true, x: quantProps.className }, quantProps.style]
        : quantProps.style;
      return <Component style={styles} {...(rest as any)} />;
    }
    QWrapped.displayName = `Qvantified(${Component.displayName || Component.name} || 'Component')`;
    return QWrapped;
  };
}

export function __styled<
  TComponent extends QuantifiableComponent,
  TQConfig extends QuantCVAConfig<any>,
>(
  Component: TComponent,
  baseClassName?: string | undefined,
  config?: Omit<TQConfig, '_' | 'className'>,
) {
  // const { className: baseClassName, ...config } = cvaConfig;
  const variantStyles =
    baseClassName || config
      ? cva(baseClassName, config)
      : (props: { className?: string }) => props.className;

  function QWrapped(
    props: Omit<ComponentProps<TComponent>, 'class'> & QuantVariantProps<TQConfig>,
  ) {
    const styleFn = useQuantStyle();
    const cvaClassName = variantStyles(props as any);
    const { className, style, ...rest } = props;
    const quantProps = styleFn(style, cvaClassName);
    const styles = quantProps.className
      ? [{ $$css: true, x: quantProps.className }, quantProps.style]
      : quantProps.style;
    return <Component style={styles} {...(rest as any)} />;
  }
  QWrapped.displayName = `Qvantified(${Component.displayName || Component.name} || 'Component')`;
  return QWrapped;
}

export function __$$<T extends QuantCVAConfig<any> | string>(x: T): T {
  return x;
}
