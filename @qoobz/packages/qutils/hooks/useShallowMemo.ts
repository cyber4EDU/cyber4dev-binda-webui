import { useRef } from 'react';

import { shallowEqual } from '../lang/object';

export function useShallowMemo<T, P extends any[]>(fn: (...args: P) => T, params: P, deps: any[]) {
  const ref = useRef<{ deps: any[]; params: P; value: T }>({ deps, params, value: fn(...params) });
  // check for dependency changes flatly
  if (ref.current.deps !== deps) {
    if (
      ref.current.deps.length !== deps.length ||
      !deps.every((dep, i) => dep === ref.current.deps[i])
    ) {
      ref.current = { deps, params, value: fn(...params) };
      return ref.current.value;
    }
  }
  // check for parameter changes shallowly
  if (ref.current.deps !== params) {
    if (
      ref.current.params.length !== params.length ||
      !params.every((param, i) => shallowEqual(param, ref.current.params[i]))
    ) {
      ref.current = { deps, params, value: fn(...params) };
    }
  }
  return ref.current.value;
}
