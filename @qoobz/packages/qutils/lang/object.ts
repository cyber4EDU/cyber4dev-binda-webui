function is(x: unknown, y: unknown): boolean {
  // eslint-disable-next-line no-self-compare
  return x === y || (x !== x && y !== y);
}

export function shallowEqual(a: unknown, b: unknown): boolean {
  if (is(a, b)) {
    return true;
  }

  if (typeof a !== 'object' || a === null || typeof b !== 'object' || b === null) {
    return false;
  }

  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);

  if (aKeys.length !== bKeys.length) {
    return false;
  }

  for (const key of aKeys) {
    // @ts-ignore
    if (!(key in b) || !is(a[key], b[key])) {
      return false;
    }
  }

  return true;
}
