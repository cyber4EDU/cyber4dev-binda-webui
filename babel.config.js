console.info('------------ Babel: root -------------');
module.exports = function (api) {
  api.cache.using(() => process.env.NODE_ENV);

  console.info(
    `>>> Babel: root [${api.version}] (${api.env()}) – ${api.caller((caller) => caller?.name)}`,
  );
  return {
    presets: ['@babel/preset-typescript', '@babel/preset-react'],
    plugins: ['tsconfig-paths-module-resolver'],
    babelrcRoots: ['.', 'packages/*'],
  };
};
