const { template } = require('./template');

module.exports = {
  typescript: true,
  native: 'true',
  icon: 24,
  template,
  ref: true,
  index: true,
  replaceAttrValues: {
    '#000': '{color}',
  },
};
