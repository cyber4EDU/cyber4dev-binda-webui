const comments = `
// Auto-generated file created by svgr-cli source svg-template.js
// Run pnpm generate-icon to update
// Do not edit
`;

function cleanJSX(jsx) {
  // console.info(jsx);
  if (jsx.openingElement) {
    const svg = jsx.openingElement;
    if (!svg.name.name === 'svg') {
      console.log(jsx);
      throw new Error('Expected svg');
    }
    svg.attributes = svg.attributes.filter((attr) => {
      if (attr.name.name === 'xmlns') return false;
      return true;
    });
  }
  return jsx;
}

/**
 * Clean the template from errors etc.
 * @param {string} template
 * @returns string
 */
function cleanTemplate(template) {
  return template;
}

/**
 * @param {import('@babel/types').ImportDeclaration[]} imports
 */
function cleanImportsV8(imports) {
  // does not work, as it adds the types anyway
  return imports.filter(
    (imp) => imp.source.value !== 'react-native-svg' || imp.importKind !== 'type',
  );
}

// eslint-disable-next-line no-unused-vars
function debug(input) {
  console.info('DEBUG', JSON.stringify(input, null, 2));
  return input;
}

const template = (vars, { tpl }) => {
  return cleanTemplate(tpl`
${comments}
${cleanImportsV8(vars.imports)};
${"import type { SvgProps } from 'react-native-svg';"}

function ${vars.componentName}(${vars.props}) { 
    const { color = '#000' } = props;
    return (
        ${cleanJSX(vars.jsx)}
    );
}
${vars.exports};
`);
};

module.exports = { template };
